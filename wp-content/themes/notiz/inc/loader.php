<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */
if( !defined("WPO_THEME_DIR") ){
    define( 'WPO_THEME_DIR', get_template_directory() );
    define( 'WPO_THEME_URI', get_template_directory_uri() );
}

define( 'WPO_FRAMEWORK_PATH', WPO_THEME_DIR . '/inc/core/' ); 
define( 'WPO_FRAMEWORK_LANGUAGE', WPO_FRAMEWORK_PATH.'language' );
define( 'WPO_FRAMEWORK_POSTTYPE', ABSPATH.'wp-content/plugins/wpothemer/posttypes/' );
define( 'WPO_FRAMEWORK_WIDGETS', WPO_FRAMEWORK_PATH.'widgets/' );

define( 'WPO_FRAMEWORK_TEMPLATES', WPO_THEME_DIR.'/templates/' ); 
define( 'WPO_FRAMEWORK_WOOCOMMERCE_WIDGETS', WPO_THEME_DIR.'/woocommerce/widgets/' );
define( 'WPO_FRAMEWORK_TEMPLATES_PAGEBUILDER', WPO_THEME_DIR.'/vc_templates/' );
define( 'WPO_FRAMEWORK_ADMIN_TEMPLATE_PATH', WPO_THEME_DIR . '/inc/core/admin/templates/' );
define( 'WPO_FRAMEWORK_PLUGINS', WPO_THEME_DIR.'/inc/plugins/' );
define( 'WPO_FRAMEWORK_XMLPATH', WPO_THEME_DIR.'/customize/' );
define( 'WPO_FRAMEWORK_CUSTOMZIME_STYLE', WPO_FRAMEWORK_XMLPATH.'assets/' );

// URI
define( 'WPO_FRAMEWORK_CUSTOMZIME_STYLE_URI', WPO_THEME_URI.'/css/customize/' );

define( 'WPO_FRAMEWORK_ADMIN_STYLE_URI', WPO_THEME_URI.'/inc/assets/' );
define( 'WPO_FRAMEWORK_ADMIN_IMAGE_URI', WPO_FRAMEWORK_ADMIN_STYLE_URI.'images/' );
define( 'WPO_FRAMEWORK_STYLE_URI', WPO_THEME_URI.'/inc/assets/' );  


require_once ( WPO_FRAMEWORK_PATH . 'functions/functions.php');


notiz_wpo_includes(  WPO_THEME_DIR . '/inc/plugins/*.php' );

notiz_wpo_includes(  WPO_FRAMEWORK_PATH . '/classes/*.php' );

if( is_admin() ) {
   /**
    * Admin Classess Core Frameworks Included
    */
    notiz_wpo_includes(  WPO_FRAMEWORK_PATH . '/classes/admin/*.php' );
 }