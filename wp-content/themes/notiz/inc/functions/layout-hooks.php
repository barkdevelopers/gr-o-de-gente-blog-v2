<?php 
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */	
	
	/**
	 * Functioning OF Template
	 */
	function notiz_wpo_header_style(){
	    $text_color = get_header_textcolor();
	    return ;
	}

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'wpo_custom_background_args', array(
	    'default-color' => 'f5f5f5',
	  ) ) );

	function notiz_wpo_custom_header_setup() {
		  add_theme_support( 'custom-header', apply_filters( 'wpo_custom_header_args', array(
		    'default-text-color'     => 'fff',
		    'width'                  => 1920,
		    'height'                 => 90,
		    'flex-height'            => true,
		    'wp-head-callback'       => 'notiz_wpo_header_style',
		    'admin-head-callback'    => 'wpo_admin_header_style',
		    'admin-preview-callback' => 'wpo_admin_header_image',
		  ) ) );
	}

	//add_action( 'after_setup_theme', 'notiz_wpo_custom_header_setup' );


	/**
	 * Add Custom Class To Body Tag
	 */
	add_filter('body_class', 'notiz_wpo_body_class');
	
	function notiz_wpo_body_class( $classes ){
		foreach ( $classes as $key => $value ) {
		
		if ( $value == 'boxed' || $value == 'default' ) 
			unset( $classes[$key] );
		}
		
		$classes[] = notiz_wpo_theme_options('configlayout');
		$classes[] = 'wpo-animate-scroll';

		return $classes;
	}

	/**
	 *
	 */
	function notiz_wpo_render_breadcrumbs_heading_tag( $tag ){
		if( ! is_single() ){
			return 'h3';
		}
		return $tag;
	}

	add_filter( 'notiz_wpo_render_breadcrumbs_heading_tag', 'notiz_wpo_render_breadcrumbs_heading_tag' );

	/**
	 * Hooks to render BreadCrumb
	 */
	function notiz_wpo_layout_breadcrumbs_render(){
		global $notizconfig;
		if( is_front_page() ){
			return ;
		}
?>
	    <div class="wpo-breadcrumbs">
	        <?php notiz_wpo_breadcrumb(); ?>
	    </div>
 	<?php 
	}

	add_action( 'notiz_wpo_layout_breadcrumbs_render', 'notiz_wpo_layout_breadcrumbs_render' );

	/**
	 * Hook To Renderr With Layout Having Configed Layout as left-right, or fullwidth 
	 * With Open Tags
	 */
	function notiz_wpo_layout_template_before(){
		global $notizconfig; 
	?>
		
		<section id="wpo-mainbody" class="wpo-mainbody default-template clearfix">
	  		<div class="container<?php if( isset($notizconfig['layout'])&&$notizconfig['layout']=='fullwidth') { ?>-fuild<?php } ?>">
	      	<div class="container-inner">
	        		<div class="row">
	          		<?php get_sidebar( 'left' );  ?>
			        <!-- MAIN CONTENT -->
			        <div id="wpo-content" class="<?php echo esc_attr( $notizconfig['main']['class'] ); ?>">
			            <div class="wpo-content">

	<?php }  
	add_action( 'notiz_wpo_layout_template_before', 'notiz_wpo_layout_template_before' );

	/**
	 * Hook To Renderr With Layout Having Configed Layout as left-right, or fullwidth 
	 * With Close Tags
	 */
	function notiz_wpo_layout_template_after(){
		global $notizconfig; 
	?>	
				         	</div>
			            </div>
			          <!-- //MAIN CONTENT -->
			          <?php get_sidebar( 'right' );  ?>
			         </div>
			   </div>
		   </div>
		</section>

	<?php }
	add_action( 'notiz_wpo_layout_template_after', 'notiz_wpo_layout_template_after' );

function notiz_wpo_get_link_social(){ ?>
	<ul class="icons">
		<?php

			$twitter_profile = get_the_author_meta( 'twitter_profile' );
			if ( $twitter_profile && $twitter_profile != '' ) {
				echo '<li class="twitter">
						<a href="' . esc_url($twitter_profile) . '">
							<i class="fa fa-twitter"></i>
						</a>
					</li>';
			}

			$facebook_profile = get_the_author_meta( 'facebook_profile' );
			if ( $facebook_profile && $facebook_profile != '' ) {
				echo '<li class="facebook">
						<a href="' . esc_url($facebook_profile) . '">
							<i class="fa fa-facebook"></i>
						</a>
					</li>';
			}
							
			$google_profile = get_the_author_meta( 'google_profile' );
			if ( $google_profile && $google_profile != '' ) {
				echo '<li class="google">
						<a href="' . esc_url($google_profile) . '" rel="author">
							<i class="fa fa-google-plus"></i>
						</a>
					</li>';
			}

			$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
			if ( $linkedin_profile && $linkedin_profile != '' ) {
		       echo '<li class="linkedin">
			       		<a href="' . esc_url($linkedin_profile) . '">
			       			<i class="fa fa-linkedin"></i>
						</a>
					</li>';
			}
							
			$pinterest_profile = get_the_author_meta( 'pinterest_profile' );
			if ( $pinterest_profile && $pinterest_profile != '' ) {
				echo '<li class="pinterest">
						<a href="' . esc_url($pinterest_profile) . '">
							<i class="fa fa-pinterest"></i>
						</a>
					</li>';
			}

			$instagram_profile = get_the_author_meta( 'instagram_profile' );
			if ( $instagram_profile && $instagram_profile != '' ) {
				echo '<li class="instagram">
						<a href="' . esc_url($instagram_profile) . '">
							<i class="fa fa-instagram"></i>
						</a>
					</li>';
			}
							
			
		?>
	</ul>
<?php }