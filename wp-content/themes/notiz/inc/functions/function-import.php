<?php
function notiz_fnc_import_remote_demos() { 
   return array(
      'notiz' => array( 'name' => 'notiz',  'source'=> 'http://wpsampledemo.com/notiz/notiz.zip' ),
   );
}

add_filter( 'pbrthemer_import_remote_demos', 'notiz_fnc_import_remote_demos' );



function notiz_fnc_import_theme() {
   return 'notiz';
}
add_filter( 'pbrthemer_import_theme', 'notiz_fnc_import_theme' );

function notiz_fnc_import_demos() {
   $folderes = glob( WPO_THEME_DIR.'/inc/import/*', GLOB_ONLYDIR ); 

   $output = array(); 

   foreach( $folderes as $folder ){
      $output[basename( $folder )] = basename( $folder );
   }
   
   return $output;
}
add_filter( 'pbrthemer_import_demos', 'notiz_fnc_import_demos' );

function notiz_fnc_import_types() {
   return array(
         'all' => 'All',
         'content' => 'Content',
         'widgets' => 'Widgets',
         'page_options' => 'Theme + Page Options',
         'menus' => 'Menus',
         'rev_slider' => 'Revolution Slider',
         'vc_templates' => 'VC Templates'
      );
}
add_filter( 'pbrthemer_import_types', 'notiz_fnc_import_types' );
