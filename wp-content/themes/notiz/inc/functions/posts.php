<?php 
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */ 


/********************************************************************************************************
 * function as shortcode to show related posts
 ********************************************************************************************************/

function notiz_wpo_get_format_icon_class($postformat){
    $icon = '';
    switch ($postformat) {
        case 'link':
            $icon = 'fa fa-link';
            break;

        case 'gallery':
            $icon = 'fa fa-th-large';
            break;

        case 'audio':
            $icon = 'fa fa-music';
            break;

        case 'video':
            $icon = 'fa fa-film';
            break;

        case 'chat':
            $icon = 'fa fa-weixin';
            break;

        case 'quote':
            $icon = 'fa fa-quote-left';
            break;

        default:
            $icon = 'fa fa-picture-o';
            break;

    }
    return $icon; 
}




if(!function_exists('notiz_wpo_related_post')){
    function notiz_wpo_related_post( $relate_count = 4, $posttype = 'post', $taxonomy = 'category', $style = '' ){
      
        $terms = get_the_terms( get_the_ID(), $taxonomy );
        $termids =array();
       
        if(!empty($terms) && !is_wp_error($terms)){
            foreach($terms as $term){
                if( is_object($term) ){
                   $termids[] = $term->term_id;
                }
            }
        }

        $args = array(
            'post_type' => $posttype,
            'posts_per_page' => $relate_count,
            'post__not_in' => array( get_the_ID() ),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'id',
                    'terms' => $termids,
                    'operator' => 'IN'
                )
            )
        );
        $template_name = 'related-'.$posttype.'.php';

        $relates = new WP_Query( $args );

        if(is_file(WPO_THEME_TEMPLATE_DIR.'elements/'.$template_name)) {
            include(WPO_THEME_TEMPLATE_DIR.'elements/'.$template_name);
        }
    }
}

/**
 * Render content by id 
 */
function notiz_wpo_render_post_content( $footer ){
   $post = get_post($footer);
   if($post){
    echo do_shortcode( $post->post_content ); 
  }
}


if(!function_exists('notiz_wpo_excerpt')){
    //Custom Excerpt Function
    function notiz_wpo_excerpt($limit,$afterlimit='[...]') {
        $excerpt = get_the_excerpt();
        if( $excerpt != ''){
           $excerpt = explode(' ', strip_tags( $excerpt ), $limit);
        }else{
            $excerpt = explode(' ', strip_tags(get_the_content( )), $limit);
        }
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).' '.$afterlimit;
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return strip_shortcodes( $excerpt );
    }
}

/**
 * Display an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index
 * views, or a div element when on single views.
 *
 * @since Twenty Fourteen 1.0
 */
function notiz_wpo_post_thumbnail($class='') {
  
  if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
    return;
  }

  if ( is_singular() ) :
  ?>
  <div class="post-thumbnail">
  <?php
    if ( ( ! is_active_sidebar( 'sidebar-2' ) || is_page_template( 'template-fullwidth.php' ) ) ) {
      the_post_thumbnail( 'fullwidth' );
    } else {
      the_post_thumbnail();
    }
  ?>
  </div>
  <?php else : ?>
  <a class="post-thumbnail <?php echo esc_attr($class);?>" href="<?php the_permalink(); ?>">
  <?php
    if ( ( ! is_active_sidebar( 'sidebar-2' ) || is_page_template( 'template-fullwidth.php' ) ) ) {
      the_post_thumbnail( 'fullwidth' );
    } else {
      the_post_thumbnail();
    }
  ?>
  </a>

  <?php endif; // End is_singular()
}




/**
 * Print HTML with meta information for the current post-date/time and author.
 *
 * @since Twenty Fourteen 1.0
 */
function notiz_wpo_posted_on() {
  if ( is_sticky() && is_home() && ! is_paged() ) {
    echo '<span class="featured-post">' . esc_html__( 'Sticky', 'notiz' ) . '</span>';
  }

  // Set up and print post meta information.
  // printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="" datetime="%2$s">%3$s</time></a></span><span class="meta-sep"> / </span><span class="byline"><span class="author vcard">'. esc_html__('By', 'notiz').' <a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>',
  //   esc_url( get_permalink() ),
  //   esc_attr( get_the_date( 'c' ) ),
  //   esc_html( get_the_date() ),
  //   esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
  //   get_the_author()
  // );
}
 



/**
 * Find out if blog has more than one category.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return boolean true if blog has more than 1 category
 */
function notiz_wpo_categorized_blog() {
  if ( false === ( $all_the_cool_cats = get_transient( 'wpo_category_count' ) ) ) {
    // Create an array of all the categories that are attached to posts
    $all_the_cool_cats = get_categories( array(
      'hide_empty' => 1,
    ) );

    // Count the number of categories that are attached to the posts
    $all_the_cool_cats = count( $all_the_cool_cats );

    set_transient( 'wpo_category_count', $all_the_cool_cats );
  }

  if ( 1 !== (int) $all_the_cool_cats ) {
    // This blog has more than 1 category so notiz_wpo_categorized_blog should return true
    return true;
  } else {
    // This blog has only 1 category so notiz_wpo_categorized_blog should return false
    return false;
  }
}


if(!function_exists('notiz_wpo_the_category')){
    function notiz_wpo_the_category($post_id=false,$separator='',$class=''){
        $categories = get_the_category($post_id);
        foreach ($categories as $key => $value) {
            $term_meta = get_option( 'taxonomy_'.$value->term_id ,array('skin'=>'skin-red') );
            if($class==''){
                $class = $term_meta['skin'];
            }else{
                $class .=' '.$term_meta['skin'];
            }
    ?>
        <a class="<?php echo esc_attr($class); ?>" href="<?php echo get_category_link( $value->term_id ); ?>">
            <?php echo esc_attr($value->name); ?>
        </a>
    <?php
            if($key<count($categories)-1){
                echo esc_html($separator);
            }
        }
    }
}

/**
 *
 */
function notiz_wpo_get_categoriesy( $category ) {
    $categories = get_categories( array( 'taxonomy' => $category ));

    $output = array( '' => esc_html__( 'All', 'notiz' ) );
    foreach( $categories as $cat ){
        if( is_object($cat) ) $output[$cat->slug] = $cat->name;
    }
    return $output;
}

if(function_exists('PostRatings')){
  add_action( 'wpo_rating', 'notiz_wpo_vote_count' );
  function notiz_wpo_vote_count(){
    $options = PostRatings()->getRating(get_the_ID());
    $classRating = "vote-default";
    if(isset($options['bayesian_rating']) && $options['bayesian_rating']>0 ){
      if(85<$options['bayesian_rating'] && $options['bayesian_rating'] <=100){
        $classRating = "vote-perfect";
      }
      if(75<$options['bayesian_rating'] && $options['bayesian_rating'] <=85){
        $classRating = "vote-good";
      }
      if(65<$options['bayesian_rating'] && $options['bayesian_rating'] <=75){
        $classRating = "vote-average";
      }
      if(55<$options['bayesian_rating'] && $options['bayesian_rating'] <=65){
        $classRating = "vote-bad";
      }
      if(0<$options['bayesian_rating'] && $options['bayesian_rating'] <=55){
        $classRating = "vote-poor";
      }
  ?>
  <?php
    $result_ratings = number_format((float)$options['bayesian_rating']/10,1,'.','');

  ?>
      <div class="entry-vote <?php echo esc_attr($classRating); ?>"><span class="entry-vote-inner"><?php echo trim($result_ratings) ?></span></div>
  <?php
    }
  }
}

if(!function_exists('notiz_wpo_get_post_views')){
    function notiz_wpo_get_post_views($postID){
        $count_key = 'wpo_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return 0;
        }
        return $count;
    }
}


if(!function_exists('notiz_wpo_embed')){
    function notiz_wpo_embed( $config ) {
        $postconfig = get_post_meta(get_the_ID(),$config['type'],true);
        $content='';
        if(isset($postconfig[$config['format']]))
            $content = wp_oembed_get($postconfig[$config['format']]);
        return $content;
    }
}



if(!function_exists('notiz_wpo_comment_form')){
    function notiz_wpo_comment_form($arg,$class='btn btn-outline btn-success radius-4x btn-md border-2'){
      ob_start();
      comment_form($arg);
      $form = ob_get_clean();
      echo str_replace('id="submit"','id="submit" class="btn '.$class.'"', $form);
    }
}

if(!function_exists('notiz_wpo_comment_reply_link')){
    function notiz_wpo_comment_reply_link($arg,$class='btn btn-default btn-xs'){
      ob_start();
      comment_reply_link($arg);
      $reply = ob_get_clean();
      echo str_replace('comment-reply-link','comment-reply-link '.$class, $reply);
    }
}


function notiz_wpo_modify_class_submit_form( $defaults){

    $defaults['class_submit'] =  $defaults['class_submit'].' btn btn-outline btn-success radius-4x btn-md border-2'; 
   return $defaults ; 
}

add_filter( 'comment_form_defaults', 'notiz_wpo_modify_class_submit_form' );

function notiz_wpo_get_breaking_post( $filter, $number ){

  switch ( $filter) {
    case 'mostview':
      $args = array(
        'posts_per_page'        => $number,
        'ignore_sticky_posts'   => 1,
        'orderby'               => 'meta_value_num',
        'meta_key'              => 'wpo_post_views_count',
        'order'                 => 'DESC'
      );
      break;
    
    case 'mostcomment':
      $args = array(
        'posts_per_page'        => $number,
        'ignore_sticky_posts'   => 1,
        'orderby'               => 'comment_count',
        'order'                 => 'DESC'
      );
      break;
    default:
      $args = array(
        'posts_per_page'        => $number,
        'ignore_sticky_posts'   => 1,
        'orderby'               => 'date',
        'order'                 => 'DESC'
      );
    break;
  }

  return new WP_Query($args);
}

//load ajax list/grid
add_action( 'wp_ajax_notiz_wpo_display_layout_listgrid', 'notiz_wpo_display_layout_listgrid' );
add_action( 'wp_ajax_nopriv_notiz_wpo_display_layout_listgrid', 'notiz_wpo_display_layout_listgrid' );
function notiz_wpo_display_layout_listgrid(){
  $args = $_POST['query'];

  $type = $_POST['type'];
  $colums = $_POST['colums'];
  $query = new WP_Query($args);

  $col          = floor(12/$colums);
  $smcol        = ($col > 4)? 6: $col;
  $class_column = 'col-lg-'.$col.' col-md-'.$col.' col-sm-'.$smcol;
  notiz_wpo_setCookieDisplay_Layout($type);
  $_count = 0;

    if($type=='grid'): 
      while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="<?php echo esc_attr($class_column); ?>">
          <?php get_template_part( 'templates/blog/blog', $type); ?>
      </div>
      <?php if($_count%$colums==$colums-1 || $_count==$query->post_count-1): ?>
      <div class="clearfix"></div>
      <?php endif; ?>
      <?php $_count++; ?>
      <?php endwhile; ?>
    <?php else: 
      while ( $query->have_posts() ) : $query->the_post();?>
        <div class="col-xs-12">
            <?php get_template_part( 'templates/blog/blog', $type); ?>
        </div>    
    <?php endwhile; ?>
  <?php endif;
  wp_reset_postdata();
  die();
}

function notiz_wpo_setCookieDisplay_Layout($value){
  setcookie('wpo_cookie_layout_post', $value , time()+3600*24*100,'/');
}

function notiz_wpo_add_to_author_profile( $contactmethods ) {
  
  $contactmethods['instagram_profile']    = 'profile Profile URL';
  $contactmethods['google_profile']       = 'Google Profile URL';
  $contactmethods['twitter_profile']      = 'Twitter Profile URL';
  $contactmethods['facebook_profile']     = 'Facebook Profile URL';
  $contactmethods['linkedin_profile']     = 'Linkedin Profile URL';
  $contactmethods['pinterest_profile']    = 'Pinterest Profile URL';
  
  return $contactmethods;
}
add_filter( 'user_contactmethods', 'notiz_wpo_add_to_author_profile', 10, 1);

function notiz_wpo_format_chat_content( $content ) {
    global $_post_format_chat_ids;

    /* If this is not a 'chat' post, return the content. */
    if ( !has_post_format( 'chat' ) )
        return $content;

    /* Set the global variable of speaker IDs to a new, empty array for this chat. */
    $_post_format_chat_ids = array();

    /* Allow the separator (separator for speaker/text) to be filtered. */
    $separator = apply_filters( 'my_post_format_chat_separator', ':' );

    /* Open the chat transcript div and give it a unique ID based on the post ID. */
    $chat_output = "\n\t\t\t" . '<div id="chat-transcript-' . esc_attr( get_the_ID() ) . '" class="chat-transcript">';

    /* Split the content to get individual chat rows. */
    $chat_rows = preg_split( "/(\r?\n)+|(<br\s*\/?>\s*)+/", $content );

    /* Loop through each row and format the output. */
    foreach ( $chat_rows as $chat_row ) {

        /* If a speaker is found, create a new chat row with speaker and text. */
        if ( strpos( $chat_row, $separator ) ) {

            /* Split the chat row into author/text. */
            $chat_row_split = explode( $separator, trim( $chat_row ), 2 );

            /* Get the chat author and strip tags. */
            $chat_author = strip_tags( trim( $chat_row_split[0] ) );

            /* Get the chat text. */
            $chat_text = trim( $chat_row_split[1] );

            /* Get the chat row ID (based on chat author) to give a specific class to each row for styling. */
            $speaker_id = notiz_wpo_format_chat_row_id( $chat_author );

            /* Open the chat row. */
            $chat_output .= "\n\t\t\t\t" . '<div class="chat-row ' . sanitize_html_class( "chat-speaker-{$speaker_id}" ) . '">';

            /* Add the chat row author. */
            $chat_output .= "\n\t\t\t\t\t" . '<div class="chat-author ' . sanitize_html_class( strtolower( "chat-author-{$chat_author}" ) ) . ' vcard"><cite class="fn">' . apply_filters( 'my_post_format_chat_author', $chat_author, $speaker_id ) . '</cite>' . $separator;

            /* Add the chat row text. */
            $chat_output .= "\n\t\t\t\t\t" . '<span class="chat-text">' . str_replace( array( "\r", "\n", "\t" ), '', apply_filters( 'my_post_format_chat_text', $chat_text, $chat_author, $speaker_id ) ) . '</span></div>';

            /* Close the chat row. */
            $chat_output .= "\n\t\t\t\t" . '</div><!-- .chat-row -->';
        }

        /**
         * If no author is found, assume this is a separate paragraph of text that belongs to the
         * previous speaker and label it as such, but let's still create a new row.
         */
        else {

            /* Make sure we have text. */
            if ( !empty( $chat_row ) ) {

                /* Open the chat row. */
                $chat_output .= "\n\t\t\t\t" . '<div class="chat-row ' . sanitize_html_class( "chat-speaker-{$speaker_id}" ) . '">';

                /* Don't add a chat row author.  The label for the previous row should suffice. */

                /* Add the chat row text. */
                $chat_output .= "\n\t\t\t\t\t" . '<div class="chat-text">' . str_replace( array( "\r", "\n", "\t" ), '', apply_filters( 'my_post_format_chat_text', $chat_row, $chat_author, $speaker_id ) ) . '</div>';

                /* Close the chat row. */
                $chat_output .= "\n\t\t\t</div><!-- .chat-row -->";
            }
        }
    }

    /* Close the chat transcript div. */
    $chat_output .= "\n\t\t\t</div><!-- .chat-transcript -->\n";

    /* Return the chat content and apply filters for developers. */
    return $chat_output;
}

function notiz_wpo_format_chat_row_id( $chat_author ) {
    global $_post_format_chat_ids;

    /* Let's sanitize the chat author to avoid craziness and differences like "John" and "john". */
    $chat_author = strtolower( strip_tags( $chat_author ) );

    /* Add the chat author to the array. */
    $_post_format_chat_ids[] = $chat_author;

    /* Make sure the array only holds unique values. */
    $_post_format_chat_ids = array_unique( $_post_format_chat_ids );

    /* Return the array key for the chat author and add "1" to avoid an ID of "0". */
    return absint( array_search( $chat_author, $_post_format_chat_ids ) ) + 1;
}