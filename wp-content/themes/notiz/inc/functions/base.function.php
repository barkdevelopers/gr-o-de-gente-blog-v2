<?php
/**
 * This files contain functions using for processing logic of themes not relating to any database.
 *
 * @version    $Id$
 * @package    wpbase
 * @author     Opal  Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */


/**
 * auto load files and extract paramters passing to this.
 */
function notiz_wpo_load_template( $template_name, $args = array() ) {
    $url = get_template_directory().'/';

    $located = $url . $template_name.'.php';

    if ( ! file_exists( $located ) ) {
        return;
    }
    if ( $args && is_array( $args ) ) {
        extract( $args );
    }
    include( $located );

}

/**
 * include files in template folder and extract parameters using for this.
 */
function notiz_wpo_get_template( $template_name, $args = array() ) {
    $url = get_template_directory().'/templates/';

    $located = $url . $template_name;

    if ( ! file_exists( $located ) ) {
        return;
    }
    if ( $args && is_array( $args ) ) {
        extract( $args );
    }
    include( $located );

}

/**
 * create a random key to use as primary key.
 */
if(!function_exists('notiz_wpo_makeid')){
    function notiz_wpo_makeid($length = 5){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}

/**
 * Get Theme Option Value.
 */
function notiz_wpo_theme_options($name, $default = false) {
  
    // get the meta from the database
    $options = ( get_option( 'wpo_theme_options' ) ) ? get_option( 'wpo_theme_options' ) : null;

    // d( $options );
   
    // return the option if it exists
    if ( isset( $options[$name] ) ) {
        return apply_filters( 'wpo_theme_options_$name', $options[ $name ] );
    }
    if( get_option( $name ) ){
        return get_option( $name );
    }
    // return default if nothing else
    return apply_filters( 'wpo_theme_options_$name', $default );
}

if(!function_exists('notiz_wpo_price')){
    function notiz_wpo_price($html){
      $tmp = '';
      if ( $html ) {
        $wpoEngine_price = preg_split("/<ins>/", $html);
        if(count($wpoEngine_price) > 1) { 
          $tmp .= '<div class="price old-price"> ';
          if(isset($wpoEngine_price[1])) $tmp .= '<ins>' . $wpoEngine_price[1]; 
          if(isset($wpoEngine_price[0])) $tmp .= $wpoEngine_price[0];
          $tmp .= '</div>';
        }else{ 
          $tmp = '<div class="price">'. $html .'</div>';
        }
      }
      return $tmp;
    }
}