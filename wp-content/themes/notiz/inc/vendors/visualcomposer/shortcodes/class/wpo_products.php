<?php  

class WPBakeryShortCode_Wpo_All_Products extends WPBakeryShortCode {

	public function getListQuery(){
		$list_query = array();
		$types = explode(',', $this->atts['show_tab']);
		foreach ($types as $type) {
			$list_query[$type] = $this->getTabTitle($type);
		}
		return $list_query;
	}

	public function getTabTitle($type){
		switch ($type) {
			case 'recent':
				return array('title'=> esc_html__('Latest Products','notiz'),'title_tab'=> esc_html__('Latest','notiz'));
			case 'featured_product':
				return array('title'=> esc_html__('Featured Products','notiz'),'title_tab'=> esc_html__('Featured','notiz'));
			case 'top_rate':
				return array('title'=> esc_html__('Top Rated Products','notiz'),'title_tab'=> esc_html__('Top Rated', 'notiz'));
			case 'best_selling':
				return array('title'=> esc_html__('BestSeller Products','notiz'),'title_tab'=> esc_html__('BestSeller','notiz'));
			case 'on_sale':
				return array('title'=> esc_html__('Special Products','notiz'),'title_tab'=> esc_html__('Special','notiz'));
		}
	}
}

class WPBakeryShortCode_Wpo_Products extends WPBakeryShortCode {

}

class WPBakeryShortCode_Wpo_Product_Deals extends WPBakeryShortCode {

}

class WPBakeryShortCode_Wpo_Productcategory extends WPBakeryShortCode {

}

class WPBakeryShortCode_Wpo_Category_Filter extends WPBakeryShortCode {

}

class WPBakeryShortCode_Wpo_Category_List extends WPBakeryShortCode {

}