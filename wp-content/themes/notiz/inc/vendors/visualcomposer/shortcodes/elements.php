<?php 
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

/**
 * Call To Action
 */	
 
		function notiz_wpo_get_iconstyles(){
			return  array(
				esc_html__( 'Default', 'notiz' ) => '',
				esc_html__( 'Outline Default', 'notiz' ) => 'icons-outline icons-default',
				esc_html__( 'Outline Primary', 'notiz' ) => 'icons-outline icons-primary',
				esc_html__( 'Outline Danger', 'notiz' ) => 'icons-outline icons-danger',
				esc_html__( 'Outline Success', 'notiz' ) => 'icons-outline icons-success',
				esc_html__( 'Outline Warning', 'notiz' ) => 'icons-outline icons-warning',
				esc_html__( 'Outline Info', 'notiz' ) => 'icons-outline icons-info',

				esc_html__( 'Inverse Default', 'notiz' )  => 'icons-inverse icons-default',
				esc_html__( 'Inverse Primary', 'notiz' )  => 'icons-inverse icons-primary',
				esc_html__( 'Inverse Danger', 'notiz' )   => 'icons-inverse icons-danger',
				esc_html__( 'Inverse Success', 'notiz' )  => 'icons-inverse icons-success',
				esc_html__( 'Inverse Warning', 'notiz' )  => 'icons-inverse icons-warning',
				esc_html__( 'Inverse Info', 'notiz' )	 => 'icons-inverse icons-info',

				esc_html__( 'Light', 'notiz' )	 => 'icons-light',
				esc_html__( 'Dark', 'notiz' )	 => 'icons-darker',

				esc_html__( 'Color Default', 'notiz' )	 => 'text-default nostyle',
				esc_html__( 'Color Primary', 'notiz' )	 => 'text-primary nostyle',
				esc_html__( 'Color Danger', 'notiz' )	 => 'text-danger nostyle',
				esc_html__( 'Color Success', 'notiz' )	 => 'text-success nostyle',
				esc_html__( 'Color Info', 'notiz' )	 => 'text-info',

			);
		}	 
		vc_update_shortcode_param( 'vc_cta_button2', array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Button Style Color', 'notiz' ),
			'param_name' => 'color',
			'prioryty' => 1,
			'value' => array(
				esc_html__( 'Default', 'notiz' ) => 'btn-default',
				esc_html__( 'Primary', 'notiz' ) => 'btn-primary',
				esc_html__( 'Danger', 'notiz' ) => 'btn-danger',
				esc_html__( 'Success', 'notiz' ) => 'btn-success',
				esc_html__( 'Info', 'notiz' ) => 'btn-info',
				esc_html__( 'Warning', 'notiz' ) => 'btn-warning',

				esc_html__( 'Outline Primary', 'notiz' ) => 'btn-outline btn-primary',
				esc_html__( 'Outline Danger', 'notiz' ) => 'btn-outline btn-danger',
				esc_html__( 'Outline Success', 'notiz' ) => 'btn-outline btn-success',
				esc_html__( 'Outline Info', 'notiz' ) => 'btn-outline btn-info',
				esc_html__( 'Outline Warning', 'notiz' ) => 'btn-outline btn-warning',

				esc_html__( 'Inverse Primary', 'notiz' ) => 'btn-inverse btn-primary',
				esc_html__( 'Inverse Danger', 'notiz' ) => 'btn-inverse btn-danger',
				esc_html__( 'Inverse Success', 'notiz' ) => 'btn-inverse btn-success',
				esc_html__( 'Inverse Info', 'notiz' ) => 'btn-inverse btn-info',
				esc_html__( 'Inverse  Warning', 'notiz' ) => 'btn-inverse btn-warning',
			)
		));

		vc_update_shortcode_param('vc_cta_button2', array(
		   'type' => 'dropdown',
		   'heading' => esc_html__( 'Heading Style', 'notiz' ),
		   'param_name' => 'heading_style',
		   'prioryty' => 2,
		   'value' => array(
		   	esc_html__('Default', 'notiz')	=> 'default',
		      esc_html__( 'Version 1', 'notiz' ) => 'v1',
		      esc_html__( 'Version 2', 'notiz' ) => 'v2',
		      esc_html__( 'Version 3', 'notiz' ) => 'v3',
				esc_html__( 'Version 4', 'notiz' ) => 'v4',
				esc_html__( 'Version 5', 'notiz' ) => 'v5',
				esc_html__( 'Version 6', 'notiz' ) => 'v6',
				esc_html__( 'Version 7', 'notiz' ) => 'v7',
				esc_html__( 'Version 8', 'notiz' ) => 'v8',
				esc_html__( 'Version 9', 'notiz' ) => 'v9',
				esc_html__( 'Version 10', 'notiz' ) => 'v10',
				esc_html__( 'Version 11', 'notiz' ) => 'v11',
				esc_html__( 'Version 12', 'notiz' ) => 'v12',
				esc_html__( 'Version 13', 'notiz' ) => 'v13',
				esc_html__( 'Version 14', 'notiz' ) => 'v14',
				esc_html__( 'Version 15', 'notiz' ) => 'v15',
		    )
		));

		vc_update_shortcode_param( 'vc_cta_button2', array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'CTA Style', 'notiz' ),
			'param_name' => 'style',
			'prioryty' => 1,
			'value' => array(
				esc_html__( 'Version 1', 'notiz' ) => '1',
				esc_html__( 'Version 2', 'notiz' ) => '2',
				esc_html__( 'Version 3', 'notiz' ) => '4',
				esc_html__( 'Version Light Style 1', 'notiz' ) => '1 light-style ',
				esc_html__( 'Version Light Style 2', 'notiz' ) => '3 light-style ',
				esc_html__( 'Version Light Style 4', 'notiz' ) => '4 light-style '
			
			)
		));

		vc_update_shortcode_param( 'vc_cta_button2', array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Button Radius Style', 'notiz' ),
			'param_name' => 'btn_style',
			'prioryty' => 1,
			'value' => array(
				esc_html__( 'None', 'notiz' ) => '',
				esc_html__( 'Radius 50%', 'notiz' ) => '',
			 	esc_html__( 'Radius 1x', 'notiz' ) => 'radius-1x',
			 	esc_html__( 'Radius 2x', 'notiz' ) => 'radius-2x',
			 	esc_html__( 'Radius 3x', 'notiz' ) => 'radius-3x',
			 	esc_html__( 'Radius 4x', 'notiz' ) => 'radius-4x',
				esc_html__( 'Radius 5x', 'notiz' ) => 'radius-5x',
				esc_html__( 'Radius 6x', 'notiz' ) => 'radius-6x',
			
			)
		));

		//Accordions
		vc_update_shortcode_param( 'vc_accordion', array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Style', 'notiz' ),
			'param_name' => 'style',
			'prioryty' => 1,
			'value' => array(
				esc_html__( 'Style 1', 'notiz' ) => 'style-1',
				esc_html__( 'Style 2', 'notiz' ) => 'style-2',
			)
		));

		//Accordions
		vc_update_shortcode_param( 'vc_toggle', array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Skin', 'notiz' ),
			'param_name' => 'skin',
			'prioryty' => 1,
			'value' => array(
				esc_html__( 'Skin 1', 'notiz' ) => '',
				esc_html__( 'Style 2', 'notiz' ) => 'style-2',
			)
		));

		//Tabs
		vc_update_shortcode_param( 'vc_tabs', array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Style', 'notiz' ),
			'param_name' => 'style',
			'prioryty' => 1,
			'value' => array(
				esc_html__( 'Default', 'notiz' ) => '',
				esc_html__( 'Style 1', 'notiz' ) => 'style-1',
				esc_html__( 'Style 2', 'notiz' ) => 'style-2',
				esc_html__( 'Style 3', 'notiz' ) => 'style-3',
				esc_html__( 'Style 4', 'notiz' ) => 'style-4',
				esc_html__( 'Style 5', 'notiz' ) => 'style-5'
			)
		));

		//Tab
		vc_update_shortcode_param( 'vc_tab', array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Icon', 'notiz' ),
			'param_name' => 'icon',
			'prioryty' => 1,
			'value' => ''
		));		

		vc_update_shortcode_param( 'vc_tab', array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Description', 'notiz' ),
			'param_name' => 'description',
			'prioryty' => 1,
			'value' => ''
		));

		//Add animation column

		$cssAnimation = array(
			esc_html__("No", 'notiz') => '',
			esc_html__("Top to bottom", 'notiz') => "top-to-bottom",
			esc_html__("Bottom to top", 'notiz') => "bottom-to-top",
			esc_html__("Left to right", 'notiz') => "left-to-right",
			esc_html__("Right to left", 'notiz') => "right-to-left",
			esc_html__("Appear from center", 'notiz') => "appear",
			'bounce' => 'bounce',
			'flash' => 'flash',
			'pulse' => 'pulse',
			'rubberBand' => 'rubberBand',
			'shake' => 'shake',
			'swing' => 'swing',
			'tada' => 'tada',
			'wobble' => 'wobble',
			'bounceIn' => 'bounceIn',
			'fadeIn' => 'fadeIn',
			'fadeInDown' => 'fadeInDown',
			'fadeInDownBig' => 'fadeInDownBig',
			'fadeInLeft' => 'fadeInLeft',
			'fadeInLeftBig' => 'fadeInLeftBig',
			'fadeInRight' => 'fadeInRight',
			'fadeInRightBig' => 'fadeInRightBig',
			'fadeInUp' => 'fadeInUp',
			'fadeInUpBig' => 'fadeInUpBig',
			'flip' => 'flip',
			'flipInX' => 'flipInX',
			'flipInY' => 'flipInY',
			'lightSpeedIn' => 'lightSpeedIn',
			'rotateInrotateIn' => 'rotateIn',
			'rotateInDownLeft' => 'rotateInDownLeft',
			'rotateInDownRight' => 'rotateInDownRight',
			'rotateInUpLeft' => 'rotateInUpLeft',
			'rotateInUpRight' => 'rotateInUpRight',
			'slideInDown' => 'slideInDown',
			'slideInLeft' => 'slideInLeft',
			'slideInRight' => 'slideInRight',
			'rollIn' => 'rollIn'
		);
		$add_css_animation = array(
			"type" => "dropdown",
			"heading" => esc_html__("CSS Animation", 'notiz'),
			"param_name" => "css_animation",
			"admin_label" => true,
			"value" => $cssAnimation,
			"description" => esc_html__("Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", 'notiz')
		);
		vc_add_param('vc_column',$add_css_animation);
		vc_add_param('vc_column_inner',$add_css_animation);
			

		vc_update_shortcode_param( 'vc_gmaps', array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display Style', 'notiz' ),
				'param_name' => 'display_style',
				 'value' => array(
				 		'Default' => '',
				 		'Popup' => 'popup'
				)
		));
		vc_update_shortcode_param( 'vc_gmaps', array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Image (use display style popup)', 'notiz' ),
				'param_name' => 'image',
				 'value' => array(
				 		'Default' => '',
				 		'Popup' => 'popup'
				) 		
		));