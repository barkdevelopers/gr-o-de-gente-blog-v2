<?php

	/*********************************************************************************************************************
	 *  Vertical menu
	 *********************************************************************************************************************/
	
    $option_menu = array( esc_html__('---Select Menu---','notiz')=>'');

    if( is_admin() ){
	    $menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );
	    foreach ($menus as $menu) {
	    	$option_menu[$menu->name]=$menu->term_id;
	    }
	}    
	vc_map( array(
	    "name" => esc_html__("WPO Vertical Menu",'notiz'),
	    "base" => "wpo_verticalmenu",
	    "class" => "",
	    "category" => esc_html__('Opal Elements', 'notiz'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => esc_html__("Title", 'notiz'),
				"param_name" => "title",
				"value" => 'Vertical Menu'
			),
	    	array(
				"type" => "dropdown",
				"heading" => esc_html__("Menu", 'notiz'),
				"param_name" => "menu",
				"value" => $option_menu,
				"admin_label" => true,
				"description" => esc_html__("Select menu.", 'notiz')
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Position", 'notiz'),
				"param_name" => "postion",
				"value" => array(
						'left'=>'left',
						'right'=>'right'
					),
				"admin_label" => true,
				"description" => esc_html__("Postion Menu Vertical.", 'notiz')
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Extra class name", 'notiz'),
				"param_name" => "el_class",
				"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
			)
	   	)
	));

	/*********************************************************************************************************************
	 * Pricing Table
	 *********************************************************************************************************************/
	vc_map( array(
	    "name" => esc_html__("WPO Pricing",'notiz'),
	    "base" => "wpo_pricing",
	    "description" => esc_html__('Make Plan for membership', 'notiz' ),
	    "class" => "",
	    "category" => esc_html__('Opal Elements', 'notiz'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => esc_html__("Title", 'notiz'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Price", 'notiz'),
				"param_name" => "price",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Currency", 'notiz'),
				"param_name" => "currency",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Period", 'notiz'),
				"param_name" => "period",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Subtitle", 'notiz'),
				"param_name" => "subtitle",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Is Featured", 'notiz'),
				"param_name" => "featured",
				'value' 	=> array(  esc_html__('No', 'notiz') => 0,  esc_html__('Yes', 'notiz') => 1 ),
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Skin", 'notiz'),
				"param_name" => "skin",
				'value' 	=> array(  esc_html__('Skin 1', 'notiz') => 'v1',  esc_html__('Skin 2', 'notiz') => 'v2', esc_html__('Skin 3', 'notiz') => 'v3' ),
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Box Style", 'notiz'),
				"param_name" => "style",
				'value' 	=> array( 'boxed' => esc_html__('Boxed', 'notiz')),
			),

			array(
				"type" => "textarea_html",
				"heading" => esc_html__("Content", 'notiz'),
				"param_name" => "content",
				"value" => '',
				'description'	=> esc_html__('Allow  put html tags', 'notiz')
			),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Link Title", 'notiz'),
				"param_name" => "linktitle",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Link", 'notiz'),
				"param_name" => "link",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Extra class name", 'notiz'),
				"param_name" => "el_class",
				"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
			)
	   	)
	));


	/*********************************************************************************************************************
	 *  Mega Posts
	 *********************************************************************************************************************/

	function notiz_wpo_parramMegaLayout($settings,$value){
		$dependency = vc_generate_dependencies_attributes($settings);
		ob_start();
		?>
			<div class="layout_images">
				<?php foreach ($settings['layout_images'] as $key => $image) {
					echo '<img src="'.$image.'" data-layout="'.$key.'" class="'.$key.' '.(($key==$value)?'active':'').'">';
				} ?>
			</div>
			<input 	type="hidden"
					name="<?php echo esc_attr($settings['param_name']); ?>"
					class="layout_image_field wpb_vc_param_value wpb-textinput <?php echo esc_attr($settings['param_name']).' '.esc_attr($settings['type']).'_field'; ?>"
					value="<?php echo esc_attr($value); ?>" <?php echo trim($dependency); ?>>
		<?php
		return ob_get_clean();
	}
	 
	/* Heading Text Block
	---------------------------------------------------------- */
	vc_map( array(
		'name'        => esc_html__( 'WPO Widget Heading','notiz'),
		'base'        => 'wpo_title_heading',
		"class"       => "",
		"category"    => esc_html__('Opal Elements', 'notiz'),
		'description' => esc_html__( 'Create title for one Widget', 'notiz' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'value'       => esc_html__( 'Title', 'notiz' ),
				'description' => esc_html__( 'Enter heading title.', 'notiz' ),
				"admin_label" => true
			),
			array(
			    'type' => 'colorpicker',
			    'heading' => esc_html__( 'Title Color', 'notiz' ),
			    'param_name' => 'font_color',
			    'description' => esc_html__( 'Select font color', 'notiz' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Title font size', 'notiz' ),
				'param_name' => 'size',
				'value' => array(
					esc_html__( 'Large', 'notiz' ) => 'font-size-lg',
					esc_html__( 'Medium', 'notiz' ) => 'font-size-md',
					esc_html__( 'Small', 'notiz' ) => 'font-size-sm',
					esc_html__( 'Extra small', 'notiz' ) => 'font-size-xs'
				),
				'description' => esc_html__( 'Select title font size.', 'notiz' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Title Align', 'notiz' ),
				'param_name' => 'title_align',
				'value' => array(
					esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
					esc_html__( 'Align left', 'notiz' ) => 'separator_align_left',
					esc_html__( 'Align right', 'notiz' ) => "separator_align_right"
				),
				'description' => esc_html__( 'Select title align.', 'notiz' )
			),
			array(
				"type" => "textarea",
				'heading' => esc_html__( 'Description', 'notiz' ),
				"param_name" => "descript",
				"value" => '',
				'description' => esc_html__( 'Enter description for title.', 'notiz' )
		    ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		),
	));
	

	/*********************************************************************************************************************
	*  Reassuarence
	*********************************************************************************************************************/
	vc_map( array(
	    "name" => esc_html__("WPO Reassuarence",'notiz'),
	    "base" => "wpo_reassuarence",
	    "class" => "",
	    "description"=> esc_html__('Counting number with your term', 'notiz'),
	    "category" => esc_html__('Opal Elements', 'notiz'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => esc_html__("Title", 'notiz'),
				"param_name" => "title",
				"value" => '',
				"admin_label" => true
			),

		 

		 	array(
				"type" => "textfield",
				"heading" => esc_html__("FontAwsome Icon", 'notiz'),
				"param_name" => "icon",
				"value" => '',
				'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'notiz' )
								. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank"> '
							. esc_html__( 'here to see the list', 'notiz' ) . '</a>' . esc_html__( 'and use class icons-lg, icons-md, icons-sm to change its size', 'notiz' )
			),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Icon Color", 'notiz'),
				"param_name" => "color",
				"value" => 'black'
			),


			array(
				"type" => "attach_image",
				"description" => esc_html__("If you upload an image, icon will not show.", 'notiz'),
				"param_name" => "image",
				"value" => '',
				'heading'	=> esc_html__('Image', 'notiz' )
			),

		 	array(
				"type" => "textarea",
				"heading" => esc_html__("Short Information", 'notiz'),
				"param_name" => "description",
				"value" => '',
				'description'	=> esc_html__('Allow  put html tags', 'notiz')
			),


		 	array(
				"type" => "textarea_html",
				"heading" => esc_html__("Detail Information", 'notiz'),
				"param_name" => "information",
				"value" => '',
				'description'	=> esc_html__('Allow  put html tags', 'notiz')
			),


			array(
				"type" => "dropdown",
				"heading" => esc_html__("Style", 'notiz'),
				"param_name" => "style",
				'value' 	=> array( 'circle' => esc_html__('circle', 'notiz'), 'vertical' => esc_html__('vertical', 'notiz') , 'horizontal' => esc_html__('horizontal', 'notiz') ),
			),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Extra class name", 'notiz'),
				"param_name" => "el_class",
				"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
			)
	   	)
	));
	


	/*********************************************************************************************************************
	 *  Facebook Like Box
	 *********************************************************************************************************************/
	vc_map( array(
		'name'        => esc_html__( 'WPO Facebook Like Box','notiz'),
		'base'        => 'wpo_facebook_like_box',
		"class"       => "",
		"category"    => esc_html__('Opal Elements', 'notiz'),
		'description' => esc_html__( 'Create title for one block', 'notiz' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget', 'notiz' ),
				'param_name' => 'title',
				'value'       => esc_html__( 'Find us on Facebook', 'notiz' ),
				'description' => esc_html__( 'Enter heading title.', 'notiz' ),
				"admin_label" => true
			),
			 
		 
			array(
				"type" => "textfield",
				"heading" => esc_html__("Facebook Page URL", 'notiz'),
				"param_name" => "page_url",
				"value" => "#"
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Width", 'notiz'),
				"param_name" => "width",
				"value" => 268
			),		
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Color Scheme', 'notiz' ),
				'param_name' => 'color_scheme',
				'value' => array(
					esc_html__( 'Light', 'notiz' ) => 'light',
					esc_html__( 'Dark', 'notiz' ) => 'dark'
				),
				'description' => esc_html__( 'Select Color Scheme.', 'notiz' )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show faces", 'notiz'),
                "param_name" => "show_faces",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show stream", 'notiz'),
                "param_name" => "show_stream",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show facebook header", 'notiz'),
                "param_name" => "show_header",
                "value" => array(
                    'Yes, please' => true
                )
			),	
			array(
				"type" => "textfield",
				"heading" => esc_html__("Extra class name", 'notiz'),
				"param_name" => "el_class",
				"value" => ''
			),									
		),
	));

	/* WPO Social Links
	---------------------------------------------------------- */
	vc_map( array(
		'name'        => esc_html__( 'WPO Social Links','notiz'),
		'base'        => 'wpo_social_links',
		"class"       => "",
		"category"    => esc_html__('Opal Elements', 'notiz'),
		'description' => esc_html__( 'Show social link for site', 'notiz' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'notiz' ),
				'param_name' => 'title',
				'value'       => esc_html__( 'Find us on social networks', 'notiz' ),
				'description' => esc_html__( 'Enter heading title.', 'notiz' ),
				"admin_label" => true
			),
			//facebook
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Facebook", 'notiz'),
                "param_name" => "show_facebook",
                "value" => array(
                    esc_html__('Yes, please', 'notiz') => true
                ),
                'std' => true
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Link Facebook", 'notiz'),
				"param_name" => "link_facebook",
				"value" => "",
				'std' => 'https://www.facebook.com/opalwordpress',
				'description' => esc_html__('Facebook page url. Example: https://www.facebook.com/opalwordpress', 'notiz')
			),

			//twitter
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Twitter", 'notiz'),
                "param_name" => "show_twitter",
                "value" => array(
                    esc_html__('Yes, please', 'notiz') => true
                ),
                'std' => true
			),		
			array(
				"type" => "textfield",
				"heading" => esc_html__("Link Twitter", 'notiz'),
				"param_name" => "link_twitter",
				"value" => "",
				'std' => 'https://twitter.com/opalwordpress',
				'description'	=> esc_html__('Insert the Twitter link. Example: https://twitter.com/opalwordpress', 'notiz')
			),

			//Youtube
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Youtube", 'notiz'),
                "param_name" => "show_youtube",
                "value" => array(
                    esc_html__('Yes, please', 'notiz') => true
                ),
                'std' => true
			),					
			array(
				"type" => "textfield",
				"heading" => esc_html__("Link Youtube ", 'notiz'),
				"param_name" => "link_youtube",
				"value" => "",
				'description' => esc_html__('Insert the YouTube link. Example: https://www.youtube.com/user/WPOpalTheme', 'notiz'),
				'std' => 'https://www.youtube.com/user/WPOpalTheme'
			),

			//Pinterest
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Pinterest", 'notiz'),
                "param_name" => "show_pinterest",
                "value" => array(
                    esc_html__('Yes, please', 'notiz') => true
                ),
                'std' => true
			),	
			array(
				"type" => "textfield",
				"heading" => esc_html__("Link Pinterest ", 'notiz'),
				"param_name" => "link_pinterest",
				"value" => "",
				'std' => 'https://www.pinterest.com/opalwordpress/',
				'description' => esc_html__('Insert the Pinterest link. Example: https://www.youtube.com/user/WPOpalTheme', 'notiz'),
			),

			//Google plus
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Google plus", 'notiz'),
                "param_name" => "show_google_plus",
                "value" => array(
                    esc_html__('Yes, please', 'notiz') => true
                ),
                'std' => true
			),
			
			array(
				"type" => "textfield",
				"heading" => esc_html__("Link Google plus", 'notiz'),
				"param_name" => "link_google_plus",
				"value" => "#",
				'std' => 'https://plus.google.com/+WPOpal',
				'description' => esc_html__('Insert the Pinterest link. Example: https://plus.google.com/+WPOpal', 'notiz'),
			),
			
			// LinkedIn
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show LinkedIn", 'notiz'),
                "param_name" => "show_linkedIn",
                "value" => array(
                    esc_html__('Yes, please', 'notiz') => true
                ),
                'std' => true
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Link LinkedIn", 'notiz'),
				"param_name" => "link_linkedIn",
				"value" => "#",
				'description' => esc_html__('Insert the Pinterest link. Example: https://www.linkedin.com/pub/opal-wordpress/67/a25/565', 'notiz'),
				'std' => 'https://www.linkedin.com/pub/opal-wordpress/67/a25/565'
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Style", 'notiz'),
				"param_name" => "style",
				'value' 	=> array( 
					esc_html__('Style v1', 'notiz') => 'style-1', 
					esc_html__('Style v2', 'notiz') => 'style-2'
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			),
			
		),
	));
	
	
	//Element Coming soon
	vc_map( array(
		'name'        => esc_html__( 'WPO Coming soon','notiz'),
		'base'        => 'wpo_coming_soon',
		"class"       => "",
		"style" 	  => "",
		"category"    => esc_html__('Opal Elements', 'notiz'),
		'description' => esc_html__( 'Create Element Coming soon', 'notiz' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'notiz' ),
				'param_name' => 'title',
				'value'       => esc_html__( '', 'notiz' ),
				'description' => esc_html__( 'Enter heading title.', 'notiz' ),
				"admin_label" => true
			),
			array(
			    'type' => 'wpo_datepicker',
			    'heading' => esc_html__( 'Date coming soon', 'notiz' ),
			    'param_name' => 'date_comingsoon',
			    'description' => esc_html__( 'Enter Date Coming soon', 'notiz' )
			),
			array(
				"type" => "textarea",
				'heading' => esc_html__( 'Description', 'notiz' ),
				"param_name" => "description",
				"value" => '',
				'description' => esc_html__( 'Enter description for title.', 'notiz' )
		    ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Style", 'notiz'),
				"param_name" => "style",
				'value' 	=> array( 
					'style 1' => esc_html__('countdown-v1', 'notiz'), 
					'style 2' => esc_html__('countdown-v2', 'notiz') ),
			)
		),
	));
	
		


	/*********************************************************************************************************************
	 *  Video Box
	*********************************************************************************************************************/
	vc_map( array(
	    "name" => esc_html__("Video box",'notiz'),
	    "base" => "wpo_video_box",
	    'icon' => 'icon-wpb-news-15',
	    'description'=>'Show Video box',
	    "class" => "",
	    "category" => esc_html__('Opal Elements', 'notiz'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => esc_html__("Title", 'notiz'),
				"param_name" => "title",
				"value" => '',
				"admin_label" => true
			),
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
	    	array(
				"type" => "textfield",
				"heading" => esc_html__("Data Url", 'notiz'),
				"param_name" => "url",
				"value" => '',
				"admin_label" => true,
				"description" => "example: //player.vimeo.com/video/88558878",
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__("Title video", 'notiz'),
				"param_name" => "title_video",
				"value" => '',
			),
			array(
				"type" => "textarea_html",
				"heading" => esc_html__("Description video", 'notiz'),
				"param_name" => "content",
				"value" => '',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Data Width', 'notiz' ),
				'param_name' => 'width',
				'value' => '1920'
		    ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Data Height', 'notiz' ),
				'param_name' => 'height',
				'value' => '1080'
		    ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'value' => ''
		    )
	   	)
	));

	//Element Last twitter
	vc_map( array(
		'name'        => esc_html__( 'WPO Last Twitter','notiz'),
		'base'        => 'wpo_last_twitter',
		"class"       => "",
		"style" 	  => "",
		"category"    => esc_html__('Opal Elements', 'notiz'),
		'description' => esc_html__( 'Create Element Last Twitter', 'notiz' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'notiz' ),
				'param_name' => 'title',
				'value'       => esc_html__( '', 'notiz' ),
				'std' => esc_html__('Last Twitter', 'notiz'),
				'description' => esc_html__( 'Enter heading title.', 'notiz' ),
			),
			array(
			    'type' => 'textfield',
			    'heading' => esc_html__( 'Twitter Username', 'notiz' ),
			    'param_name' => 'username',
			    'std' => 'Opal Wordpress',
			    'description' => esc_html__( 'Enter Username Twitter', 'notiz' ),
			),
			array(
				"type" => "textfield",
				'heading' => esc_html__( 'Widget ID Twitter', 'notiz' ),
				"param_name" => "widget_id",
				"value" => '',
				'std' => '615459328879759360',
				'description' => esc_html__( 'Please view link to get Widget ID: <a href="https://en.support.wordpress.com/widgets/twitter-timeline-widget/">get Widget ID</a>', 'notiz' )
		    ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Twitter status limit', 'notiz' ),
				'param_name' => 'status_limit',
				'value' => array(
					'1' => 1,
					'2' => 2,
					'3' => 3,
					'4' => 4,
					'5' => 5
				),
				'std' => 1
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show background", 'notiz'),
                "param_name" => "show_background",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Replies", 'notiz'),
                "param_name" => "show_replies",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Header", 'notiz'),
                "param_name" => "show_header",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Footer ", 'notiz'),
                "param_name" => "show_footer",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => esc_html__("Show Border", 'notiz'),
                "param_name" => "show_border",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			),
		),
	));

	

		/**********************************************************************************************************************
			 * Buttons
			 **********************************************************************************************************************/
			vc_map( array(
		        "name" => esc_html__("WPO Button",'notiz'),
		        "base" => "wpo_button",
		        'description'=> esc_html__('Create Button', 'notiz'),
		        "class" => "",
		        "category" => esc_html__('Opal Elements', 'notiz'),
		        "params" => array(
		            array(
		                "type" => "textfield",
		                "heading" => esc_html__("Button Text", 'notiz'),
		                "param_name" => "btn_title",
		                "admin_label" => true,
		                "value" => 'Button',
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__("Button Size", 'notiz'),
		                "param_name" => "btn_size",
		                "std" => "btn-lg",
		                'value'     => array(                                 
		                                'Button Mini Small' => esc_html__('btn-xs', 'notiz'),
		                                'Button Small' => esc_html__('btn-sm', 'notiz'),
		                                'Button Medium' => esc_html__('btn-md', 'notiz'),                                
		                                'Button Large' => esc_html__('btn-lg', 'notiz'),
		                            ),
		            ),
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Button Style', 'notiz' ),
		                'param_name' => 'btn_color',
		                "std" => "btn-outline-light",
		                'value' => array(
		                    esc_html__( 'Default', 'notiz' ) => 'btn-default',
		                    esc_html__( 'Primary', 'notiz' ) => 'btn-primary',
		                    esc_html__( 'Danger', 'notiz' ) => 'btn-danger',
		                    esc_html__( 'Success', 'notiz' ) => 'btn-success',
		                    esc_html__( 'Info', 'notiz' ) => 'btn-info',
		                    esc_html__( 'Warning', 'notiz' ) => 'btn-warning',
		                    esc_html__( 'Outline Light', 'notiz' ) => 'btn-outline-light',
		                    esc_html__( 'Outline Primary', 'notiz' ) => 'btn-outline btn-primary',
		                    esc_html__( 'Outline Danger', 'notiz' ) => 'btn-outline btn-danger',
		                    esc_html__( 'Outline Success', 'notiz' ) => 'btn-outline btn-success',
		                    esc_html__( 'Outline Info', 'notiz' ) => 'btn-outline btn-info',
		                    esc_html__( 'Outline Warning', 'notiz' ) => 'btn-outline btn-warning',
		                    esc_html__( 'Inverse', 'notiz' ) => 'btn-inverse',
		                    esc_html__( 'Inverse Primary', 'notiz' ) => 'btn-inverse btn-primary',
		                    esc_html__( 'Inverse Danger', 'notiz' ) => 'btn-inverse btn-danger',
		                    esc_html__( 'Inverse Success', 'notiz' ) => 'btn-inverse btn-success',
		                    esc_html__( 'Inverse Info', 'notiz' ) => 'btn-inverse btn-info',
		                    esc_html__( 'Inverse  Warning', 'notiz' ) => 'btn-inverse btn-warning',
		                ),
		            ),
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Button border size', 'notiz' ),
		                'param_name' => 'btn_border_size',
		                "std" => "border-0",
		                'value' => array(
		                	esc_html__( 'None', 'notiz' ) => 'border-0',
		                    esc_html__( '1px', 'notiz' ) => 'border-1',
		                    esc_html__( '2px', 'notiz' ) => 'border-2',                    
		                )
		            ),             
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Button Radius', 'notiz' ),
		                'param_name' => 'btn_radius',
		                "std" => "radius-0x",
		                'value' => array(
		                    esc_html__( 'None', 'notiz' ) => 'radius-0x',
		                    esc_html__( 'Radius 50%', 'notiz' ) => 'radius-x',
		                    esc_html__( 'Radius 1x', 'notiz' ) => 'radius-1x',
		                    esc_html__( 'Radius 2x', 'notiz' ) => 'radius-2x',
		                    esc_html__( 'Radius 3x', 'notiz' ) => 'radius-3x',
		                    esc_html__( 'Radius 4x', 'notiz' ) => 'radius-4x',
		                    esc_html__( 'Radius 5x', 'notiz' ) => 'radius-5x',
		                    esc_html__( 'Radius 6x', 'notiz' ) => 'radius-6x',    
		                )
		            ),
		            array(
		                'type'       => 'textfield',
		                'heading'    => esc_html__( 'Margin', 'notiz' ),
		                'param_name' => 'margin',
		                'value'      => '0px',
		                'description' => esc_html__('Enter margin for button (px)', 'notiz')
		            ),
		            array(
		                'type'       => 'textfield',
		                'heading'    => esc_html__( 'Width', 'notiz' ),
		                'param_name' => 'width',
		                'value'      => 'auto',
		                'description' => esc_html__('Enter width for button (px)', 'notiz')
		            ),
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Buton Align', 'notiz' ),
		                'param_name' => 'alignment',
		                "std" => "separator_align_left",
		                'value' => array(
		                    esc_html__( 'None', 'notiz' )   => 'none',
		                    esc_html__( 'left', 'notiz' ) => 'separator_align_left',
		                    esc_html__( 'center', 'notiz' ) => 'separator_align_center',
		                    esc_html__( 'right', 'notiz' ) => 'separator_align_right'
		                )
		            ),
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Icon library', 'notiz' ),
		                'value' => array(
		                    esc_html__( 'Font Awesome', 'notiz' ) => 'fontawesome',
		                    esc_html__( 'Open Iconic', 'notiz' ) => 'openiconic',                    
		                ),
		                'param_name' => 'icon_type',
		                'description' => esc_html__( 'Select icon library.', 'notiz' ),
		                'group'         => esc_html__( 'Icons', 'notiz' )
		            ),
		            array(
		                'type' => 'iconpicker',
		                'heading' => esc_html__( 'Icon', 'notiz' ),
		                'param_name' => 'icon_fontawesome',
		                'value' => '',
		                'settings' => array(
		                    'emptyIcon' => false, 
		                    'iconsPerPage' => 200, 
		                ),
		                'dependency' => array(
		                    'element' => 'icon_type',
		                    'value' => 'fontawesome',
		                ),
		                'description' => esc_html__( 'Select icon from library.', 'notiz' ),
		                'group'         => esc_html__( 'Icons', 'notiz' )
		            ),
		            array(
		                'type' => 'iconpicker',
		                'heading' => esc_html__( 'Icon', 'notiz' ),
		                'param_name' => 'icon_openiconic',
		                'settings' => array(
		                    'emptyIcon' => false, 
		                    'type' => 'openiconic',
		                    'iconsPerPage' => 200, 
		                ),
		                'dependency' => array(
		                    'element' => 'icon_type',
		                    'value' => 'openiconic',
		                ),
		                'description' => esc_html__( 'Select icon from library.', 'notiz' ),
		                'group'         => esc_html__( 'Icons', 'notiz' )
		            ),            
		            array(
		                'type' => 'colorpicker',
		                'heading' => esc_html__( 'Button Icon Color', 'notiz' ),
		                'param_name' => 'icons_color',
		                'description' => esc_html__( 'Select icon color', 'notiz' ),
		                'group'         => esc_html__( 'Icons', 'notiz' )
		            ),
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Icon Size', 'notiz' ),
		                'param_name' => 'icons_size',
		                'prioryty' => 1,
		                'std' => 'fa-3',
		                'value' => array(
		                    esc_html__( 'Mini', 'notiz' ) => 'fa-1',
		                    esc_html__( 'Small', 'notiz' ) => 'fa-2',
		                    esc_html__( 'Normal', 'notiz' ) => 'fa-3',
		                    esc_html__( 'Large', 'notiz' ) => 'fa-4',     
		                ),
		                'group'         => esc_html__( 'Icons', 'notiz' )
		            ),
		            array(
		                  'type' => 'dropdown',
		                  'heading' => esc_html__( 'Icon Position', 'notiz' ),
		                  'param_name' => 'icons_position',
		                  'prioryty' => 1,
		                  'std' => 'pull-left',
		                  'value' => array(
		                      esc_html__( 'None', 'notiz' ) => 'none',
		                      esc_html__( 'Left', 'notiz' ) => 'pull-left',
		                      esc_html__( 'Right', 'notiz' ) => 'pull-right',                      
		                  ),
		                  'group'         => esc_html__( 'Icons', 'notiz' )
		              ),
		            array(
		                'type' => 'checkbox',
		                'heading' => esc_html__( 'Enable Icon ?', 'notiz' ),
		                'param_name' => 'is_icons',
		                'description' => esc_html__( 'Enable icon .', 'notiz' ),
		                'value' => array( esc_html__( 'Yes', 'notiz' ) => 'yes' ),
		                'group'         => esc_html__( 'Icons', 'notiz' )
		            ),
		            array(
						'type' => 'vc_link',
						'heading' => esc_html__( 'Button link', 'notiz' ),
						'param_name' => 'btn_link',
						'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'notiz' )
					),
		            array(
		                "type" => "textfield",
		                "heading" => esc_html__("Extra class name", 'notiz'),
		                "param_name" => "el_class",
		                "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
		            )
		        )
		    ));

			/**
			 *
			 */

			vc_map( array(
				'name'        => esc_html__( 'WPO Block Heading','notiz'),
				'base'        => 'wpo_block_heading',
				"class"       => "",
				"category"    => esc_html__('Opal Elements', 'notiz'),
				'description' => esc_html__( 'Create Block Heading with info + icon', 'notiz' ),
				"params"      => array(
					
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Block Heading Title', 'notiz' ),
						'param_name' => 'title',
						'value'       => esc_html__( '', 'notiz' ),
						'description' => esc_html__( 'Enter heading title.', 'notiz' ),
						"admin_label" => true
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Sub Heading Title', 'notiz' ),
						'param_name' => 'subheading',
						'value'       => esc_html__( '', 'notiz' ),
						'description' => esc_html__( 'Enter Sub heading title.', 'notiz' ),
						"admin_label" => true
					),
				 	array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Title Align', 'notiz' ),
						'param_name' => 'title_align',
						'value' => array(
							esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
							esc_html__( 'Align left', 'notiz' ) => 'separator_align_left',
							esc_html__( 'Align right', 'notiz' ) => "separator_align_right"
						),
						'description' => esc_html__( 'Select title align.', 'notiz' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Title Alignment', 'notiz' ),
						'param_name' => 'alignment',
						'value' => array(
							esc_html__( 'Align left', 'notiz' ) => 'separator_align_left',
							esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
							esc_html__( 'Align right', 'notiz' ) => 'separator_align_right'
						)
					),
				 	 array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Heading Style', 'notiz' ),
		                'param_name' => 'heading_style',
		                'value' => array(
		                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
		                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
		                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
							esc_html__( 'Version 4', 'notiz' ) => 'v4',
							esc_html__( 'Version 5', 'notiz' ) => 'v5',
							esc_html__( 'Version 6', 'notiz' ) => 'v6',
							esc_html__( 'Version 7', 'notiz' ) => 'v7',
							esc_html__( 'Version 8', 'notiz' ) => 'v8',
							esc_html__( 'Version 9', 'notiz' ) => 'v9',
							esc_html__( 'Version 10', 'notiz' ) => 'v10',
							esc_html__( 'Version 11', 'notiz' ) => 'v11',
							esc_html__( 'Version 12', 'notiz' ) => 'v12',
							esc_html__( 'Version 13', 'notiz' ) => 'v13',
		                )
		            ),
					array(
						"type" => "textarea",
						'heading' => esc_html__( 'Description', 'notiz' ),
						"param_name" => "descript",
						"value" => '',
						'description' => esc_html__( 'Enter description for title.', 'notiz' )
				    ),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'notiz' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
					)
				),
			));
			

			/**
			 *
			 */
				/*********************************************************************************************************************
			 *  Our Service
			 *********************************************************************************************************************/
			vc_map( array(
			    "name" => esc_html__("Opal Featured Box",'notiz'),
			    "base" => "wpo_featuredbox",
			    "description"=> esc_html__('Decreale Service Info', 'notiz'),
			    "class" => "",
			    "category" => esc_html__('Opal Elements', 'notiz'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'notiz'),
						"param_name" => "title",
						"value" => '',
							"admin_label" => true
					),

			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Sub Title", 'notiz'),
						"param_name" => "subtitle",
						"value" => '',
							"admin_label" => false
					),
					
			    	array(
						"type" => "colorpicker",
						"heading" => esc_html__("Font Text Color", 'notiz'),
						"param_name" => "color",
						"value" => '',
							"admin_label" => false
					),

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'notiz'),
						"param_name" => "style",
						'admin_label' => true,
						'value' 	=> array(
							esc_html__('Icon Left', 'notiz') => 'icon-box-left', 
							esc_html__('Icon Right', 'notiz') => 'icon-box-right', 
							esc_html__('Icon Center', 'notiz') => 'icon-box-center', 
							esc_html__('Icon Top - Text Left', 'notiz') => 'icon-box-top text-left', 
							esc_html__('Icon Top - Text Right', 'notiz') => 'icon-box-top text-right', 
							esc_html__('Icon Right - Text Left', 'notiz') => 'icon-right-text-left',
							esc_html__('Background Box Hover', 'notiz') => 'background-box-hover space-padding-0'
						),
					),	
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Background Block', 'notiz' ),
						'param_name' => 'background',
						'value' => array(
							esc_html__( 'None', 'notiz' ) => '',
							esc_html__( 'Success', 'notiz' ) => 'bg-success',
							esc_html__( 'Info', 'notiz' ) => 'bg-info',
							esc_html__( 'Danger', 'notiz' ) => 'bg-danger',
							esc_html__( 'Warning', 'notiz' ) => 'bg-warning',
							esc_html__( 'Light', 'notiz' ) => 'bg-default',
						)
					),

				 	array(
						"type" => "textfield",
						"heading" => esc_html__("FontAwsome Icon", 'notiz'),
						"param_name" => "icon",
						"value" => '',
						'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'notiz' )
										. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
										. esc_html__( 'here to see the list', 'notiz' ) . '</a>' . esc_html__( 'and use class icons-lg, icons-md, icons-sm to change its size', 'notiz' )
					),
				 	array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Icon Style', 'notiz' ),
						'param_name' => 'iconstyle',
						"admin_label" => true,
						'value' => notiz_wpo_get_iconstyles()
					),
					array(
						"type" => "attach_image",
						"heading" => esc_html__("Photo", 'notiz'),
						"param_name" => "photo",
						"value" => '',
						'description'	=> ''
					),

					array(
						"type" => "textarea_html",
						"heading" => esc_html__("Information", 'notiz'),
						"param_name" => "content",
						"value" => '',
						'description'	=> esc_html__('Allow  put html tags', 'notiz' )
					),

					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'notiz'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
					)
			   	)
			));
			

			/*********************************************************************************************************************
			 *  WPO Counter
			 *********************************************************************************************************************/
			vc_map( array(
			    "name" => esc_html__("WPO Counter",'notiz'),
			    "base" => "wpo_counter",
			    "class" => "",
			    "description"=> esc_html__('Counting number with your term', 'notiz'),
			    "category" => esc_html__('Opal Elements', 'notiz'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'notiz'),
						"param_name" => "title",
						"value" => '',
							"admin_label" => true
					),

					array(
						"type" => "colorpicker",
						"heading" => esc_html__("Text Color", 'notiz'),
						"param_name" => "color",
						"value" => '',
						'description'	=> ''
					),
					array(
						"type" => "textarea",
						"heading" => esc_html__("Description", 'notiz'),
						"param_name" => "description",
						"value" => '',
							"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number", 'notiz'),
						"param_name" => "number",
						"value" => ''
					),

				 	array(
						"type" => "textfield",
						"heading" => esc_html__("FontAwsome Icon", 'notiz'),
						"param_name" => "icon",
						"value" => 'fa-pencil radius-x',
						'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'notiz' )
										. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
										. esc_html__( 'here to see the list', 'notiz' ) . '</a>' . esc_html__( 'and use class icons-lg, icons-md, icons-sm to change its size', 'notiz' )
					),


					array(
						"type" => "attach_image",
						"description" => esc_html__("If you upload an image, icon will not show.", 'notiz'),
						"param_name" => "image",
						"value" => '',
						'heading'	=> esc_html__('Image', 'notiz' )
					),

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'notiz'),
						"param_name" => "style",
						'value' 	=> array( 
							esc_html__('Default', 'notiz') =>  '',
							esc_html__('Style 1 light style', 'notiz') =>'style-1 light-style',
							esc_html__('Style 2 light style', 'notiz') =>'style-2-light' ,
						)	
					),

					array(	
						"type" => "dropdown",
						"heading" => esc_html__("Text Color", 'notiz'),
						"param_name" => "text_color",
						'value' 	=> array( esc_html__('None', 'notiz') =>  'text-default', esc_html__('Primary', 'notiz') =>'text-primary' , esc_html__('Info', 'notiz') => 'text-info',  esc_html__('Danger', 'notiz') => 'text-danger',  esc_html__('Warning', 'notiz') => 'text-warning'  ),
					),
				
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'notiz'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
					)
			   	)
			));

			/*********************************************************************************************************************
			 *  Info Box
			 *********************************************************************************************************************/
			vc_map( array(
			    "name" => esc_html__("WPO Info Box",'notiz'),
			    "base" => "wpo_inforbox",
			    "class" => "",
			    "description"=> esc_html__( 'Show header, text in special style', 'notiz'),
			    "category" => esc_html__('Opal Elements', 'notiz'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'notiz'),
						"param_name" => "title",
						"value" => '',
							"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Sub Title", 'notiz'),
						"param_name" => "sub_title",
						"value" => '',
							"admin_label" => true
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Title Align', 'notiz' ),
						'param_name' => 'title_align',
						'value' => array(
							esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
							esc_html__( 'Align left', 'notiz' ) => 'separator_align_left',
							esc_html__( 'Align right', 'notiz' ) => "separator_align_right"
						),
						'description' => esc_html__( 'Select title align.', 'notiz' )
					), 
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Title font size', 'notiz' ),
						'param_name' => 'size',
						'value'      => array(
							esc_html__( 'Large', 'notiz' )       => 'font-size-lg',
							esc_html__( 'Medium', 'notiz' )      => 'font-size-md',
							esc_html__( 'Small', 'notiz' )       => 'font-size-sm',
							esc_html__( 'Extra small', 'notiz' ) => 'font-size-xs'
						)
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Info Box Content Style', 'notiz' ),
						'param_name' => 'inforbox_style',
						'value'      => array(
						esc_html__( 'Light', 'notiz' )   => '',
						esc_html__( 'Dark', 'notiz' ) => 'inforbox-dark',
						)
					),

					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Info Box Align Mode', 'notiz' ),
						'param_name' => 'inforbox_alight',
						'value'      => array(
						esc_html__( 'Left Style', 'notiz' )   => '',
						esc_html__( 'Right Style', 'notiz' ) => 'inforbox-align-right',
						)
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style display', 'notiz' ),
						'param_name' => 'style_display',
						'value'      => array(
						esc_html__( 'Default', 'notiz' )   => '',
						esc_html__( 'Style 1', 'notiz' ) => 'style-1',
						)
					),
					array(
						"type" => "textarea",
						"heading" => esc_html__("information", 'notiz'),
						"param_name" => "information",
						"value" => '',
						'description'	=> esc_html__('Allow  put html tags', 'notiz')
					),
					array(
						"type" => "attach_image",
						"heading" => esc_html__("Backgroup Image", 'notiz'),
						"param_name" => "imagebg",
						"value" => '',
						'description'	=> ''
					),
					array(
						"type" => "colorpicker",
						"heading" => esc_html__("Background Color", 'notiz'),
						"param_name" => "colorbg",
						"value" => '',
						'description'	=> ''
					),

					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'notiz'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
					)
			   	)
			));

			vc_map( array(
			    "name" => esc_html__("WPO Info Box 2",'notiz'),
			    "base" => "wpo_inforbox2",
			    "class" => "",
			    "description"=> esc_html__( 'Show Info In special Block', 'notiz'),
			    "category" => esc_html__('Opal Elements', 'notiz'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'notiz'),
						"param_name" => "title",
						"value" => '',
							"admin_label" => true
					),
					 
					array(
						"type" => "textarea",
						"heading" => esc_html__("information", 'notiz'),
						"param_name" => "information",
						"value" => '',
						'description'	=> esc_html__('Allow  put html tags', 'notiz')
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("FontAwsome Icon", 'notiz'),
						"param_name" => "icon",
						"value" => '',
						'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'notiz' )
										. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
										. esc_html__( 'here to see the list', 'notiz' ) . '</a>' . esc_html__( 'and use class icons-lg, icons-md, icons-sm to change its size', 'notiz' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Backgroup Style', 'notiz' ),
						'param_name' => 'bgstyle',
						'prioryty' => 1,
						"description" => esc_html__( 'Set background style or customize at below', 'notiz'),
						'value' => array(
							esc_html__( 'Default', 'notiz' ) => 'bg-nostyle',
							esc_html__( 'Primary', 'notiz' ) => 'bg-primary text-white',
							esc_html__( 'Danger', 'notiz' ) => 'bg-danger text-white',
							esc_html__( 'Success', 'notiz' ) => 'bg-success text-white',
							esc_html__( 'Info', 'notiz' ) => 'bg-info text-white',
							esc_html__( 'Warning', 'notiz' ) => 'bg-warning text-white'
						)
					),
				 

					array(
						"type" => "attach_image",
						"heading" => esc_html__("Backgroup Image", 'notiz'),
						"param_name" => "imagebg",
						"value" => '',
						'description'	=> ''
					),

					array(
						"type" => "colorpicker",
						"heading" => esc_html__("Background Color", 'notiz'),
						"param_name" => "colorbg",
						"value" => '',
						'description'	=> ''
					),

					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'notiz'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
					)
			   	)
			));
			
		 
			/*********************************************************************************************************************
			 *  Banner
			 *********************************************************************************************************************/
			vc_map( array(
			    "name" => esc_html__("WPO Banner",'notiz'),
			    "base" => "wpo_banner",
			    "class" => "",
			    "description"=> esc_html__( 'Show Banner in special style', 'notiz'),
			    "category" => esc_html__('Opal Elements', 'notiz'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'notiz'),
						"param_name" => "title",
						"value" => '',
							"admin_label" => true
					),
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Sub title", 'notiz'),
						"param_name" => "subheading",
						"value" => '',
							"admin_label" => true
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Content Position', 'notiz' ),
						'param_name' => 'content_position',
						'value'      => array(
						esc_html__( 'Left', 'notiz' )   => 'content_position_left',
						esc_html__( 'Center', 'notiz' ) => 'content_position_center',
						esc_html__( 'Right', 'notiz' )  => 'content_position_right'
						)
					),
				 
					array(
						"type" => "textarea",
						"heading" => esc_html__("information", 'notiz'),
						"param_name" => "information",
						"value" => '',
						'description'	=> esc_html__('Allow  put html tags', 'notiz')
					),
					array(
						"type" => "attach_image",
						"heading" => esc_html__("Backgroup Image", 'notiz'),
						"param_name" => "imagebg",
						"value" => '',
						'description'	=> ''
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Link Button", 'notiz'),
						"param_name" => "link",
						"value" => '',
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Text Button", 'notiz'),
						"param_name" => "link_text",
						"value" => '',
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Button Style Color', 'notiz' ),
						'param_name' => 'color',
						'prioryty' => 1,
						'value' => array(
							esc_html__( 'Default', 'notiz' ) => 'btn-default',
							esc_html__( 'Primary', 'notiz' ) => 'btn-primary',
							esc_html__( 'Danger', 'notiz' ) => 'btn-danger',
							esc_html__( 'Success', 'notiz' ) => 'btn-success',
							esc_html__( 'Info', 'notiz' ) => 'btn-info',
							esc_html__( 'Warning', 'notiz' ) => 'btn-warning',

							esc_html__( 'Outline Primary', 'notiz' ) => 'btn-outline btn-primary',
							esc_html__( 'Outline Danger', 'notiz' ) => 'btn-outline btn-danger',
							esc_html__( 'Outline Success', 'notiz' ) => 'btn-outline btn-success',
							esc_html__( 'Outline Info', 'notiz' ) => 'btn-outline btn-info',
							esc_html__( 'Outline Warning', 'notiz' ) => 'btn-outline btn-warning',
							esc_html__( 'Outline Light', 'notiz' ) => 'btn-outline-light',
							
							esc_html__( 'Inverse Primary', 'notiz' ) => 'btn-inverse btn-primary',
							esc_html__( 'Inverse Danger', 'notiz' ) => 'btn-inverse btn-danger',
							esc_html__( 'Inverse Success', 'notiz' ) => 'btn-inverse btn-success',
							esc_html__( 'Inverse Info', 'notiz' ) => 'btn-inverse btn-info',
							esc_html__( 'Inverse  Warning', 'notiz' ) => 'btn-inverse btn-warning',
						)
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Button Radius Style', 'notiz' ),
						'param_name' => 'btn_style',
						'prioryty' => 1,
						'value' => array(
							esc_html__( 'None', 'notiz' ) => '',
							esc_html__( 'Radius 50%', 'notiz' ) => 'radius-x',
						 	esc_html__( 'Radius 1x', 'notiz' ) => 'radius-1x',
						 	esc_html__( 'Radius 2x', 'notiz' ) => 'radius-2x',
						 	esc_html__( 'Radius 3x', 'notiz' ) => 'radius-3x',
						 	esc_html__( 'Radius 4x', 'notiz' ) => 'radius-4x',
							esc_html__( 'Radius 5x', 'notiz' ) => 'radius-5x',
							esc_html__( 'Radius 6x', 'notiz' ) => 'radius-6x',
						
						)
					),

					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Hover Effect', 'notiz' ),
						'param_name' => 'hover_effect',
						'prioryty' => 1,
						'value' => array(
							esc_html__( 'Always Show Content', 'notiz' ) => 'no-effect',
							esc_html__( 'Light and Hidden Content', 'notiz' ) => 'light-hide-effect',
						)
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'notiz'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
					)
			   )
			));