<?php  
	$newssupported = true;


if( $newssupported ) {
	/**********************************************************************************
	 * Front Page Posts
	 **********************************************************************************/


	/// Front Page 1
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 1', 'notiz' ),
		'base' => 'wpo_frontpageposts',
		'icon' => 'icon-wpb-news-1',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	// Front page 2
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 2', 'notiz' ),
		'base' => 'wpo_frontpageposts2',
		'icon' => 'icon-wpb-news-8',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	// Front page 3
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 3', 'notiz' ),
		'base' => 'wpo_frontpageposts3',
		'icon' => 'icon-wpb-news-3',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

		 

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	// Front page 4
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 4', 'notiz' ),
		'base' => 'wpo_frontpageposts4',
		'icon' => 'icon-wpb-news-4',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
		 

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),
			 

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Front page 5
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 5', 'notiz' ),
		'base' => 'wpo_frontpageposts5',
		'icon' => 'icon-wpb-news-5',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post no thumbnail', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Grid Columns", 'notiz'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Front page 6
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 6', 'notiz' ),
		'base' => 'wpo_frontpageposts6',
		'icon' => 'icon-wpb-news-6',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Columns count", 'notiz'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Front page 7
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 7', 'notiz' ),
		'base' => 'wpo_frontpageposts7',
		'icon' => 'icon-wpb-news-7',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
		 

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	
	// Front page 12
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 12', 'notiz' ),
		'base' => 'wpo_frontpageposts12',
		'icon' => 'icon-wpb-news-12',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	// Frontpage 13
	vc_map( array(
		'name' => esc_html__( '(News) FontPage 13', 'notiz' ),
		'base' => 'wpo_frontpageposts13',
		'icon' => 'icon-wpb-news-13',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Categories Tab Hovering to show post', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );	

	// Frontpage 14
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 14', 'notiz' ),
		'base' => 'wpo_frontpageposts14',
		'icon' => 'icon-wpb-news-1',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),
 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	
	// Frontpage Categories
	vc_map( array(
		'name' => esc_html__( '(News) Categories Post', 'notiz' ),
		'base' => 'wpo_categoriespost',
		'icon' => 'icon-wpb-news-3',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

		 

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );	

	// Front page 9
	vc_map( array(
		'name' => esc_html__( '(News) FrontPage 9', 'notiz' ),
		'base' => 'wpo_frontpageposts9',
		'icon' => 'icon-wpb-news-9',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Grid Columns", 'notiz'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Timeline Posts
	vc_map( array(
		'name' => esc_html__( '(Blog) TimeLine Post', 'notiz' ),
		'base' => 'wpo_timelinepost',
		'icon' => 'icon-wpb-news-10',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),
 

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Enable Pagination', 'notiz' ),
				'param_name' => 'pagination',
				'value' => array( 'No'=>'0', 'Yes'=>'1'),
				'std' => '0',
				'admin_label' => true,
				'description' => esc_html__( 'Select style display.', 'notiz' )
			)
		)
	) );

	// Tab Posts
	vc_map( array(
		'name' => esc_html__( '(News) Categories Tab Post', 'notiz' ),
		'base' => 'wpo_categorytabpost',
		'icon' => 'icon-wpb-application-icon-large',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),

		 

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
	

	$layout_image = array(
		esc_html__('Grid', 'notiz')             => 'grid-1',
		esc_html__('List', 'notiz')             => 'list-1',
		esc_html__('List not image', 'notiz')   => 'list-2',
	);

	// Grid List
	vc_map( array(
		'name' => esc_html__( '(News) Grid List', 'notiz' ),
		'base' => 'wpo_gridlist',
		'icon' => 'icon-wpb-news-2',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Post having news,managzine style', 'notiz' ),
	 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
		 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Style", 'notiz' ),
				"param_name" => "style",
				"value" => array( esc_html__('Default Style', 'notiz' ) => ''  ,  esc_html__('Style 1', 'notiz' ) => 'style1' ,  esc_html__('Style 2', 'notiz' ) => 'style2' ),
				"std" => ''
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Enable Button List Grid', 'notiz' ),
				'param_name' => 'listgrid',
				"value" => array(
                    'Yes, It is Ok' => true
                ),
				'std' => true,
				'description' => esc_html__( 'Enable or Disable button list grid.', 'notiz' )
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Enable Load more', 'notiz' ),
				'param_name' => 'loadmore',
				"value" => array(
                    'Yes, It is Ok' => true
                ),
				'std' => true,
				'description' => esc_html__( 'Enable or disable button load more.', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Grid Posts
	vc_map( array(
		'name' => esc_html__( '(News) Grid Posts', 'notiz' ),
		'base' => 'wpo_gridposts',
		'icon' => 'icon-wpb-news-2',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Post having news,managzine style', 'notiz' ),
	 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
		 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Layout Type", 'notiz'),
				"param_name" => "layout",
				"layout_images" => $layout_image,
				"value" => $layout_image,
				"admin_label" => true,
				"description" => esc_html__("Select Skin layout.", 'notiz')
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Grid Columns", 'notiz'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 6),
				"std" => 3
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );


	
	/**********************************************************************************
	 * Mega Blogs
	 **********************************************************************************/

	/// Blog Front Page
	vc_map( array(
		'name' => esc_html__( '(Blog) FrontPage', 'notiz' ),
		'base' => 'wpo_frontpageblog',
		'icon' => 'icon-wpb-news-1',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),
 			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Number Main Posts", 'notiz'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );


	vc_map( array(
		'name' => esc_html__('(Blog) Grids ', 'notiz' ),
		'base' => 'wpo_megablogs',
		'icon' => 'icon-wpb-news-2',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

		 
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),
		 
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Description', 'notiz' ),
				'param_name' => 'descript',
				"value" => ''
			),

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 10 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Layout", 'notiz' ),
				"param_name" => "layout",
				"value" => array( esc_html__('Default Style', 'notiz' ) => 'blog' , esc_html__('Default Style 2', 'notiz' ) => 'blog-v2'  ,  esc_html__('Special Style 1', 'notiz' ) => 'special-1',  esc_html__('Special Style 2', 'notiz' ) => 'special-2',  esc_html__('Special Style 3', 'notiz' ) => 'special-3' ),
				"std" => 3,
				'admin_label'=> true
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Grid Columns", 'notiz'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 6),
				"std" => 3
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );


// FrontPage 8
	vc_map( array(
		'name' => esc_html__('(News) FrontPage 8 ', 'notiz' ),
		'base' => 'wpo_frontpageposts8',
		'icon' => 'icon-wpb-news-2',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having grid styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

		 
			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),

			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 10 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Layout", 'notiz' ),
				"param_name" => "layout",
				"value" => array( esc_html__('Default Style', 'notiz' ) => 'blog' , esc_html__('Default Style 2', 'notiz' ) => 'blog-v2'  ,  esc_html__('Special Style 1', 'notiz' ) => 'special-1',  esc_html__('Special Style 2', 'notiz' ) => 'special-2',  esc_html__('Special Style 3', 'notiz' ) => 'special-3' ),
				"std" => 3,
				'admin_label'=> true
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Grid Columns", 'notiz'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 6),
				"std" => 3
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Enable Button List Grid', 'notiz' ),
				'param_name' => 'listgrid',
				"value" => array(
                    'Yes, It is Ok' => true
                ),
				'std' => true,
				'description' => esc_html__( 'Enable or Disable button list grid.', 'notiz' )
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Enable Load more', 'notiz' ),
				'param_name' => 'loadmore',
				"value" => array(
                    'Yes, It is Ok' => true
                ),
				'std' => true,
				'description' => esc_html__( 'Enable or disable button load more.', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );
 

	/**********************************************************************************
	 * Slideshow Post Widget Gets
	 **********************************************************************************/
		vc_map( array(
			'name' => esc_html__( '(News) Slideshow Post', 'notiz' ),
			'base' => 'wpo_slideshowpost',
			'icon' => 'icon-wpb-news-slideshow',
			"category" => esc_html__('Opal News', 'notiz'),
			'description' => esc_html__( 'Play Posts In slideshow', 'notiz' ),
			 
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Widget title', 'notiz' ),
					'param_name' => 'title',
					'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
					"admin_label" => true
				),

				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Title Alignment', 'notiz' ),
					'param_name' => 'alignment',
					'value' => array(
						esc_html__( 'Align left', 'notiz' ) => 'separator_align_left',
						esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
						esc_html__( 'Align right', 'notiz' ) => 'separator_align_right'
					)
				),

				array(
					'type' => 'textarea',
					'heading' => esc_html__( 'Heading Description', 'notiz' ),
					'param_name' => 'descript',
					"value" => ''
				),

				array(
					'type' => 'loop',
					'heading' => esc_html__( 'Grids content', 'notiz' ),
					'param_name' => 'loop',
					'settings' => array(
						'size' => array( 'hidden' => false, 'value' => 10 ),
						'order_by' => array( 'value' => 'date' ),
					),
					'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
				),

				array(
					"type" => "dropdown",
					"heading" => esc_html__("Layout", 'notiz' ),
					"param_name" => "layout",
					"value" => array( esc_html__('Default Style', 'notiz' ) => 'blog'  ,  esc_html__('Special Style 1', 'notiz' ) => 'style1' ,  esc_html__('Special Style 2', 'notiz' ) => 'style2' ),
					"std" => 3
				),

				array(
					"type" => "dropdown",
					"heading" => esc_html__("Grid Columns", 'notiz'),
					"param_name" => "grid_columns",
					"value" => array( 1 , 2 , 3 , 4 , 6),
					"std" => 3
				),


				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
					'param_name' => 'grid_thumb_size',
					'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'notiz' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
				)
			)
		) );
	
	// List Posts
	vc_map( array(
		'name' => esc_html__( '(News) List Post', 'notiz' ),
		'base' => 'wpo_listpost',
		'icon' => 'icon-wpb-news-10',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Post having blog styles', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
					esc_html__( 'Version 9', 'notiz' ) => 'v9',
                )
            ),
			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Thumbnail size', 'notiz' ),
				'param_name' => 'grid_thumb_size',
				'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Top Author
	vc_map( array(
		'name' => esc_html__( '(News) Top Author', 'notiz' ),
		'base' => 'wpo_top_author',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Element top author', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => esc_html__( 'Title Alignment', 'notiz' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				esc_html__( 'Align left', 'notiz' )   => 'separator_align_left',
				esc_html__( 'Align center', 'notiz' ) => 'separator_align_center',
				esc_html__( 'Align right', 'notiz' )  => 'separator_align_right'
				)
			),

			array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
                'param_name' => 'widget_title_style',
                'value' => array(
                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
					esc_html__( 'Version 4', 'notiz' ) => 'v4',
					esc_html__( 'Version 5', 'notiz' ) => 'v5',
					esc_html__( 'Version 6', 'notiz' ) => 'v6',
					esc_html__( 'Version 7', 'notiz' ) => 'v7',
					esc_html__( 'Version 8', 'notiz' ) => 'v8',
                )
            ),

			 array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Title font size', 'notiz' ),
				'param_name' => 'size',
				'value' => array(
					esc_html__( 'Large', 'notiz' ) => 'font-size-lg',
					esc_html__( 'Medium', 'notiz' ) => 'font-size-md',
					esc_html__( 'Small', 'notiz' ) => 'font-size-sm',
					esc_html__( 'Extra small', 'notiz' ) => 'font-size-xs'
				),
				'description' => esc_html__( 'Select title font size.', 'notiz' )
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select style', 'notiz' ),
				'param_name' => 'author_style',
				'value' => array(
                    esc_html__( 'Style Version 1', 'notiz' ) => 'style1',
                    esc_html__( 'Style Version 2', 'notiz' ) => 'style2',
                ),
				"std" => 'style1',
				'description' => esc_html__( 'Select Authors style. ', 'notiz' )
			),
			 
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Number author', 'notiz' ),
				'param_name' => 'number',
				'value' => 4,
				'description' => esc_html__( 'Number author to show.', 'notiz' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Columns', 'notiz' ),
				'param_name' => 'columns',
				"value" => array( 1 , 2 , 3 , 4 , 5, 6),
				"std" => 1,
				'description' => esc_html__( 'Select column . ', 'notiz' )
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Rows', 'notiz' ),
				'param_name' => 'rows',
				"value" => array( 1 , 2 , 3 , 4),
				"std" => 4,
				'description' => esc_html__( 'Select row . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	// Tabs categories
	vc_map( array(
		'name' => esc_html__( '(News) Tab Categories Parent', 'notiz' ),
		'base' => 'wpo_tab_categories_parent',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create Element tab categories parent', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title Category', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Category parent', 'notiz' ),
				'param_name' => 'category_id',
				"value" => notiz_get_categories_parent('category'),
				'description' => esc_html__( 'Select Category parent to show tab Children. ', 'notiz' )
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select style', 'notiz' ),
				'param_name' => 'post_style',
				'value' => array(
                    esc_html__( 'Style Version 1', 'notiz' ) => 'style1',
                    esc_html__( 'Style Version 2', 'notiz' ) => 'style2',
                    esc_html__( 'Style Version 3', 'notiz' ) => 'style3',
					esc_html__( 'Style Version 4', 'notiz' ) => 'style4',
					esc_html__( 'Style Version 5', 'notiz' ) => 'style5',
					esc_html__( 'Style Version 6', 'notiz' ) => 'style6',
                ),
				"std" => 'style1',
				'description' => esc_html__( 'Select style for Category tab . ', 'notiz' )
			),
			array(
				"type" => "attach_image",
				"heading" => esc_html__("Photo", 'notiz'),
				"param_name" => "photo",
				"value" => '',
				'description'	=> 'Image use for style 2 and style 6'
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	//Trending tabs
	vc_map( array(
			'name' => esc_html__( '(News) Tab Trending', 'notiz' ),
			'base' => 'wpo_tab_trending',
			"category" => esc_html__('Opal News', 'notiz'),
			'description' => esc_html__( 'Create Element tab Trending post', 'notiz' ),
			 
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Title', 'notiz' ),
					'param_name' => 'title',
					'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
					"admin_label" => true
				),
				array(
	                'type' => 'colorpicker',
	                'heading' => esc_html__( 'Title Color', 'notiz' ),
	                'param_name' => 'title_color',
	                'description' => esc_html__( 'Select font color', 'notiz' )
	            ),
	            array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Number post to show', 'notiz' ),
					'param_name' => 'number',
					'value' => 10,
					'description' => esc_html__( 'Number author to show.', 'notiz' )
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Rows', 'notiz' ),
					'param_name' => 'rows',
					"value" => array( 1 , 2 , 3 , 4, 5),
					"std" => 4,
					'description' => esc_html__( 'Select row . ', 'notiz' )
				),
				// array(
				// 	'type' => 'dropdown',
				// 	'heading' => esc_html__( 'Select style', 'notiz' ),
				// 	'param_name' => 'trending_style',
				// 	'value' => array(
	   //                  esc_html__( 'Style Version 1', 'notiz' ) => 'style1',
	   //                  esc_html__( 'Style Version 2', 'notiz' ) => 'style2',
	   //              ),
				// 	"std" => 'style1',
				// 	'description' => esc_html__( 'Select style for Category tab . ', 'notiz' )
				// ),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'notiz' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
				)
			)
		) );

	/// Blog Slider
	vc_map( array(
		'name' => esc_html__( '(Blog) Slide Show', 'notiz' ),
		'base' => 'wpo_blog_slideshow',
		'icon' => 'icon-wpb-news-1',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create blog slide show', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),
 			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Grids content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

	/// Blog grid Slider
	vc_map( array(
		'name' => esc_html__( '(Blog) Grid Slide Show', 'notiz' ),
		'base' => 'wpo_bloggrid_slideshow',
		'icon' => 'icon-wpb-news-1',
		"category" => esc_html__('Opal News', 'notiz'),
		'description' => esc_html__( 'Create blog grid slide show', 'notiz' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Widget title', 'notiz' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'notiz' ),
				"admin_label" => true
			),
 			 
			array(
				'type' => 'loop',
				'heading' => esc_html__( 'Blog content', 'notiz' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false),
					'order_by' => array( 'value' => 'date' ),
					'post_type' => array( 'value' => 'post' ),
				),
				'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'notiz' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select slider', 'notiz' ),
				'param_name' => 'slider',
				"value" => array( 1 , 2 , 3 , 4),
				"std" => 2,
				'description' => esc_html__( 'Select slider . ', 'notiz' )
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'notiz' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'notiz' )
			)
		)
	) );

}
