<?php
if(WPO_PLGTHEMER_ACTIVED){
	if( notiz_wpo_get_posttype_enable('video')) {

		 vc_map( array(
		    "name" => esc_html__("Opal Video Grid V1",'notiz'),
		    "base" => "wpo_video_grid_v1",
		    'icon' => 'icon-wpb-news-15',
		    'description'=>'Show Gallery with gallery post type',
		    "class" => "",
		    "category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Number gallery', 'notiz' ),
					'param_name' => 'number',
					'value' => '9'
			    ),
			    array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Grid Columns', 'notiz' ),
					'param_name' => 'grid_columns',
					'value' => '2'
			    ),
		   	)
		));
	}

	/*********************************************************************************************************************
	 *  Portfolio
	 *********************************************************************************************************************/
	if( notiz_wpo_get_posttype_enable('portfolio')) {
		vc_map( array(
		    "name" => esc_html__("WPO Portfolio",'notiz'),
		    "base" => "wpo_portfolio",
		    'icon' => 'icon-wpb-news-15',
		    'description'=>'Portfolio',
		    "class" => "",
		    "category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	 array(
	                "type" => "checkbox",
	                "heading" => esc_html__("Item No Padding", 'notiz'),
	                "param_name" => "nopadding",
	                "value" => array(
	                    'Yes, It is Ok' => true
	                ),
	                'std' => true
				),	
		    	 
			 
				array(
					"type" => "textarea",
					'heading' => esc_html__( 'Description', 'notiz' ),
					"param_name" => "descript",
					"value" => ''
			    ),

				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Display Masonry', 'notiz' ),
					'param_name' => 'masonry',
					'value' => array(
						esc_html__( 'No', 'notiz' ) => '0',
						esc_html__( 'Yes', 'notiz' ) => '1',
					)
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Number of portfolio to show", 'notiz'),
					"param_name" => "number",
					"value" => '6'
				),

				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Columns count', 'notiz' ),
					'param_name' => 'columns_count',
					'value' => array( 6, 4, 3, 2, 1 ),
					'std' => 3,
					'admin_label' => true,
					'description' => esc_html__( 'Select columns count.', 'notiz' )
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Enable Pagination', 'notiz' ),
					'param_name' => 'pagination',
					'value' => array( 'No'=>'0', 'Yes'=>'1'),
					'std' => 'style-1',
					'admin_label' => true,
					'description' => esc_html__( 'Select style display.', 'notiz' )
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
				)
		   	)
		));
	}

	/*********************************************************************************************************************
	 *  Gallery grid
	 *********************************************************************************************************************/
	if( notiz_wpo_get_posttype_enable('gallery')) {
		vc_map( array(
		    "name" => esc_html__("WPO Gallery Grid",'notiz'),
		    "base" => "wpo_gallery_grid",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Gallery Grid',
		    "class" => "",
		    "category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	 
				 
				array(
					"type" => "textfield",
					'heading' => esc_html__( 'Number gallery', 'notiz' ),
					"param_name" => "number",
					"value" => '6'
			    ),
			    array(
					"type" => "dropdown",
					'heading' => esc_html__( 'Columns', 'notiz' ),
					"param_name" => "column",
					"value" => array('2'=>'2', '3'=>'3', '4'=>'4')
			    ),
			    array(
					"type" => "dropdown",
					'heading' => esc_html__( 'Remove Padding', 'notiz' ),
					"param_name" => "padding",
					"value" => array(esc_html__('No', 'notiz') => '', esc_html__('Yes', 'notiz') => '1')
			    ),
			    array(
					"type" => "textfield",
					'heading' => esc_html__( 'Extra class name', 'notiz' ),
					"param_name" => "class",
					"value" => ''
			    )
		   	)
		));
	
		/*********************************************************************************************************************
		 *  Gallery portfolio
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("WPO Gallery Filter",'notiz'),
		    "base" => "wpo_gallery_filter",
		    'icon' => 'icon-wpb-news-15',
		    'description'=>'Show Gallery with gallery post type',
		    "class" => "",
		    "category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	 
			 
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Number gallery', 'notiz' ),
					'param_name' => 'number',
					'value' => '9'
			    ),
			    array(
					"type" => "dropdown",
					'heading' => esc_html__( 'Columns', 'notiz' ),
					'param_name' => 'column',
					"value" => array('2' => '2', '3' => '3', '4' => '4'),
			    ),
			    array(
			    	'type' => 'dropdown',
			    	'heading' => esc_html__('Show Pagination', 'notiz'),
			    	'param_name' => 'pagination',
			    	'value' => array(esc_html__('Yes', 'notiz') => '1', esc_html__('No', 'notiz') => '0' )
			    )
		   	)
		));
	}

	/*********************************************************************************************************************
	 *  Brands Carousel
	 *********************************************************************************************************************/
 
	if( notiz_wpo_get_posttype_enable('brand')) {
		vc_map( array(
		    "name" => esc_html__("WPO Brands Carousel",'notiz'),
		    "base" => "wpo_brands",
		    'description'=>'Show Brand Logos, Manufacture Logos',
		    "class" => "",
		    "category" => esc_html__('Opal Woocommece','notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),

				array(
					"type" => "textarea",
					"heading" => esc_html__('Description', 'notiz'),
					"param_name" => "descript",
					"value" => ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Number of brands to show", 'notiz'),
					"param_name" => "number",
					"value" => '6'
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Icon", 'notiz'),
					"param_name" => "icon"
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
				)
		   	)
		));
	}

		/**********************************************************************************************************************
			 * Testimonials
			 **********************************************************************************************************************/
		//if( notiz_wpo_get_posttype_enable('testimonials')) {
			vc_map( array(
			    "name" => esc_html__("WPO Testimonials",'notiz'),
			    "base" => "wpo_testimonials",
			    'description'=> esc_html__('Play Testimonials In Carousel', 'notiz'),
			    "class" => "",
			    "category" => esc_html__('Opal Elements', 'notiz'),
			    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => '',
						"admin_label" => true
				),

		    	array(
	                'type' => 'dropdown',
	                'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
	                'param_name' => 'widget_title_style',
	                'value' => array(
	                    esc_html__( 'Version 1', 'notiz' ) => 'v1',
	                    esc_html__( 'Version 2', 'notiz' ) => 'v2',
	                    esc_html__( 'Version 3', 'notiz' ) => 'v3',
						esc_html__( 'Version 4', 'notiz' ) => 'v4',
						esc_html__( 'Version 5', 'notiz' ) => 'v5',
						esc_html__( 'Version 6', 'notiz' ) => 'v6',
						esc_html__( 'Version 7', 'notiz' ) => 'v7',
						esc_html__( 'Version 8', 'notiz' ) => 'v8',
	                )
	            ),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Backgroup Image", 'notiz'),
					"param_name" => "imagebg",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "colorpicker",
					"heading" => esc_html__("Background Color", 'notiz'),
					"param_name" => "colorbg",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
				)
	   		)
		));
	//}

	if( notiz_wpo_get_posttype_enable('team')) {
		vc_map( array(
		    "name" => esc_html__("(Team) Grid Carousel",'notiz'),
		    "base" => "wpo_teamcarousel",
		    "class" => "",
		    "description" => esc_html__('Show data from Posttype Team in Carousel and Grid Style', 'notiz'),
		    "category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),

			 	array(
					"type" => "textfield",
					"heading" => esc_html__("Number Of Items", 'notiz'),
					"param_name" => "number",
					"value" => '4',
						"admin_label" => false
				),
			 	
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Show In Columns", 'notiz'),
					"param_name" => "show",
					'description'	=> esc_html__('Display Team in N Columns Of Carousel Style','notiz'),
					'value' 	=> array(  esc_html__('1 Column', 'notiz') => '1',  esc_html__('2 Columns', 'notiz') => '2',  esc_html__('3 Columns', 'notiz') => '3' , esc_html__('4 Columns', 'notiz')  => '4' , esc_html__('5 Columns', 'notiz') => '5')
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
				)
		   	)
		));

		/******************************
		 * Our Team
		 ******************************/
		vc_map( array(
		    "name" => esc_html__("(Team) Grid Style",'notiz'),
		    "base" => "wpo_team",
		    "class" => "",
		    "description" => 'Show Personal Profile Info',
		    "category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'notiz'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Job", 'notiz'),
					"param_name" => "job",
					"value" => 'CEO',
					'description'	=>  ''
				),

				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'notiz'),
					"param_name" => "information",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'notiz')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Phone", 'notiz'),
					"param_name" => "phone",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Google Plus", 'notiz'),
					"param_name" => "google",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Facebook", 'notiz'),
					"param_name" => "facebook",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Twitter", 'notiz'),
					"param_name" => "twitter",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Pinterest", 'notiz'),
					"param_name" => "pinterest",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Linked In", 'notiz'),
					"param_name" => "linkedin",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'notiz'),
					"param_name" => "style",
					'value' 	=> array(  esc_html__('circle', 'notiz') => 'circle',  esc_html__('vertical', 'notiz') => 'vertical',  esc_html__('horizontal', 'notiz') => 'horizontal' , esc_html__('special', 'notiz')  => 'special' , esc_html__('hover', 'notiz') => 'team-hover')
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
				)
		   	)
		));
	
		/******************************
		 * Our Team
		 ******************************/
		vc_map( array(
			"name" => esc_html__("(Team) List Style",'notiz'),
			"base" => "wpo_team_list",
			"class" => "",
			"description" => esc_html__('Show Info In List Style', 'notiz'),
			"category" => esc_html__('Opal Elements', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'notiz'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'notiz'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Phone", 'notiz'),
					"param_name" => "phone",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'notiz'),
					"param_name" => "information",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'notiz')
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("blockquote", 'notiz'),
					"param_name" => "blockquote",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Email", 'notiz'),
					"param_name" => "email",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Facebook", 'notiz'),
					"param_name" => "facebook",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Twitter", 'notiz'),
					"param_name" => "twitter",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Linked In", 'notiz'),
					"param_name" => "linkedin",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'notiz'),
					"param_name" => "style",
					'value' 	=> array( 'circle' => esc_html__('circle', 'notiz'), 'vertical' => esc_html__('vertical', 'notiz') , 'horizontal' => esc_html__('horizontal', 'notiz') ),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
				)

		   	)
		));
	}
}