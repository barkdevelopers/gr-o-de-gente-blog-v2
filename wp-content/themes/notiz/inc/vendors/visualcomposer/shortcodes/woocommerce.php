<?php
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
if( WPO_WOOCOMMERCE_ACTIVED ) {
		$params 	= array();
	 	$cats 		= array();
		$product_columns_deal = array(1, 2, 3, 4);
		$value 		= array();

		/**
		 * wpo_productcategory
		 */
		

		$product_layout  = array('Grid'=>'grid','List'=>'list','Carousel'=>'carousel');
		$product_type    = array('Best Selling'=>'best_selling','Featured Products'=>'featured_product','Top Rate'=>'top_rate','Recent Products'=>'recent_product','On Sale'=>'on_sale','Recent Review' => 'recent_review' );
		$product_columns = array(6,4, 3, 2, 1);
		$show_tab 		 = array(
			                array('recent', esc_html__('Latest Products', 'notiz')),
			                array( 'featured_product', esc_html__('Featured Products', 'notiz' )),
			                array('best_selling', esc_html__('BestSeller Products', 'notiz' )),
			                array('top_rate', esc_html__('TopRated Products', 'notiz' )),
			                array('on_sale', esc_html__('Special Products', 'notiz' ))
			            );


		
		if( is_admin() ){
			global $wpdb;
			$sql     = "SELECT a.name,a.slug,a.term_id FROM $wpdb->terms a JOIN  $wpdb->term_taxonomy b ON (a.term_id= b.term_id ) where b.count>0 and b.taxonomy = 'product_cat'";
			$results = $wpdb->get_results($sql);
		
			foreach ($results as $vl) {
				$value[$vl->name] = $vl->slug;
			}
			$query 		= "SELECT a.name,a.slug,a.term_id FROM $wpdb->terms a JOIN  $wpdb->term_taxonomy b ON (a.term_id= b.term_id ) where b.count>0 and b.taxonomy = 'product_cat' and b.parent = 0";
			$categories = $wpdb->get_results($query);
			
			foreach ($categories as $category) {
				$cats[$category->name] = $category->term_id;
			}
		}


	    vc_map( array(
	        "name" => esc_html__("WPO Product Deals",'notiz'),
	        "base" => "wpo_product_deals",
	        "class" => "",
	        "category" => esc_html__('Opal Woocommece','notiz'),
	        "params" => array(
	            array(
	                "type" => "textfield",
	                "class" => "",
	                "heading" => esc_html__('Title', 'notiz'),
	                "param_name" => "title",
	                "admin_label" => true
	            ),
	            array(
	                "type" => "textfield",
	                "heading" => esc_html__("Extra class name", 'notiz'),
	                "param_name" => "el_class",
	                "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'notiz')
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__("Columns count",'notiz'),
	                "param_name" => "columns_count",
	                "value" => $product_columns_deal,
	                "admin_label" => true,
	                "description" => esc_html__("Select columns count.",'notiz')
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__("Layout",'notiz'),
	                "param_name" => "layout",
	                "value" => array(esc_html__('Carousel', 'notiz') => 'carousel', esc_html__('Grid', 'notiz') =>'grid' ),
	                "admin_label" => true,
	                "description" => esc_html__("Select columns count.",'notiz')
	            )
	        )
	    ));
	

	

		vc_map( array(
		    "name" => esc_html__("WPO Product Category",'notiz'),
		    "base" => "wpo_productcategory",
		    "class" => "",
		    "category" =>esc_html__("Opal Woocommece",'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_html__('Title', 'notiz'),
					"param_name" => "title",
					"value" =>''
				),
		    	array(
					"type" => "dropdown",
					"class" => "",
					"heading" => esc_html__('Category', 'notiz'),
					"param_name" => "category",
					"value" =>$value,
					"admin_label" => true
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style",'notiz'),
					"param_name" => "style",
					"value" => $product_layout
				),
				array(
					"type"        => "attach_image",
					"description" => esc_html__("Upload an image for categories", 'notiz'),
					"param_name"  => "image_cat",
					"value"       => '',
					'heading'     => esc_html__('Image', 'notiz' )
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Number of products to show",'notiz'),
					"param_name" => "number",
					"value" => '4'
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Columns count",'notiz'),
					"param_name" => "columns_count",
					"value" => $product_columns,
					"admin_label" => true,
					"description" => esc_html__("Select columns count.",'notiz')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Icon",'notiz'),
					"param_name" => "icon"
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name",'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'notiz')
				)
		   	)
		));
	
		/**
		* wpo_category_filter
		*/

		vc_map( array(
				"name"     => esc_html__("WPO Product Categories Filter",'notiz'),
				"base"     => "wpo_category_filter",
				"class"    => "",
				"category" => esc_html__('Opal Woocommece', 'notiz'),
				"params"   => array(

				array(
					"type" => "dropdown",
					"class" => "",
					"heading" => esc_html__('Category', 'notiz'),
					"param_name" => "term_id",
					"value" =>$cats,
					"admin_label" => true
				),

				array(
					"type"        => "attach_image",
					"description" => esc_html__("Upload an image for categories (190px x 190px)", 'notiz'),
					"param_name"  => "image_cat",
					"value"       => '',
					'heading'     => esc_html__('Image', 'notiz' )
				),

				array(
					"type"       => "textfield",
					"heading"    => esc_html__("Number of categories to show",'notiz'),
					"param_name" => "number",
					"value"      => '5'
				),

				array(
					"type"        => "textfield",
					"heading"     => esc_html__("Extra class name",'notiz'),
					"param_name"  => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'notiz')
				)
		   	)
		));


		/**
		 * wpo_products
		 */
		vc_map( array(
		    "name" => esc_html__("WPO Products",'notiz'),
		    "base" => "wpo_products",
		    "class" => "",
		    "category" => esc_html__('Opal Woocommece', 'notiz'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title",'notiz'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => ''
				),
				
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Widget Title Style', 'notiz' ),
					'param_name' => 'widget_title_style',
					'value' => array(
						esc_html__( 'Version 1', 'notiz' ) => 'v1',
						esc_html__( 'Version 2', 'notiz' ) => 'v2',
						esc_html__( 'Version 3', 'notiz' ) => 'v3',
						esc_html__( 'Version 4', 'notiz' ) => 'v4',
						esc_html__( 'Version 5', 'notiz' ) => 'v5',
						esc_html__( 'Version 6', 'notiz' ) => 'v6',
						esc_html__( 'Version 7', 'notiz' ) => 'v7',
						esc_html__( 'Version 8', 'notiz' ) => 'v8',
					)
				),
				 
		    	array(
					"type" => "dropdown",
					"heading" => esc_html__("Type",'notiz'),
					"param_name" => "type",
					"value" => $product_type,
					"admin_label" => true,
					"description" => esc_html__("Select columns count.",'notiz')
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style",'notiz'),
					"param_name" => "style",
					"value" => $product_layout
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Widget Product Style', 'notiz' ),
					'param_name' => 'widget_product_style',
					'value' => array(
						esc_html__( 'Style 1', 'notiz' ) => 'style1',
						esc_html__( 'Style 2', 'notiz' ) => 'style2',
					)
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Columns count",'notiz'),
					"param_name" => "columns_count",
					"value" => $product_columns,
					"admin_label" => true,
					"description" => esc_html__("Select columns count.",'notiz')
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Rows count",'notiz'),
					"param_name" => "rows_count",
					"value" => $product_columns_deal,
					"std" => 1,
					"description" => esc_html__("Select rows count.",'notiz')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Number of products to show",'notiz'),
					"param_name" => "number",
					"value" => '6'
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name",'notiz'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'notiz')
				)
		   	)
		));
	
		

		 

		vc_map( array(
			"name"     => esc_html__("WPO Product Categories List",'notiz'),
			"base"     => "wpo_category_list",
			"class"    => "",
			"category" => esc_html__('Opal Woocommece', 'notiz'),
			"params"   => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__('Title', 'notiz'),
				"param_name" => "title",
				"value"      => '',
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Show post counts', 'notiz' ),
				'param_name' => 'show_count',
				'description' => esc_html__( 'Enables show count total product of category.', 'notiz' ),
				'value' => array( esc_html__( 'Yes, please', 'notiz' ) => 'yes' )
			),
			array(
				"type"       => "checkbox",
				"heading"    => esc_html__("show children of the current category",'notiz'),
				"param_name" => "show_children",
				'description' => esc_html__( 'Enables show children of the current category.', 'notiz' ),
				'value' => array( esc_html__( 'Yes, please', 'notiz' ) => 'yes' )
			),
			array(
				"type"       => "checkbox",
				"heading"    => esc_html__("Show dropdown children of the current category ",'notiz'),
				"param_name" => "show_dropdown",
				'description' => esc_html__( 'Enables show dropdown children of the current category.', 'notiz' ),
				'value' => array( esc_html__( 'Yes, please', 'notiz' ) => 'yes' )
			),

			array(
				"type"        => "textfield",
				"heading"     => esc_html__("Extra class name",'notiz'),
				"param_name"  => "el_class",
				"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'notiz')
			)
	   	)
	));
	
	$params =  array(
    	array(
			"type" => "textfield",
			"heading" => esc_html__("Title",'notiz'),
			"param_name" => "title",
			"admin_label" => true,
			"value" => ''
		),
		  
		array(
            "type" => "sorted_list",
            "heading" => esc_html__("Show Tab", 'notiz'),
            "param_name" => "show_tab",
            "description" => esc_html__("Control teasers look. Enable blocks and place them in desired order.", 'notiz'),
            "value" => "recent,featured_product,best_selling",
            "options" => $show_tab
        ),
        array(
			"type" => "dropdown",
			"heading" => esc_html__("Style",'notiz'),
			"param_name" => "style",
			"value" => $product_layout
		),
		array(
			"type" => "textfield",
			"heading" => esc_html__("Number of products to show",'notiz'),
			"param_name" => "number",
			"value" => '4'
		),
		array(
			"type" => "dropdown",
			"heading" => esc_html__("Columns count",'notiz'),
			"param_name" => "columns_count",
			"value" => $product_columns,
			"admin_label" => true,
			"description" => esc_html__("Select columns count.",'notiz')
		),
		array(
			"type" => "textfield",
			"heading" => esc_html__("Extra class name",'notiz'),
			"param_name" => "el_class",
			"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'notiz')
		)
   	);
 

	/**
	 * wpo_all_products
	 */
	vc_map( array(
	    "name" => esc_html__("WPO Products Tabs",'notiz'),
	    "base" => "wpo_all_products",
	    "class" => "",
	    "category" => esc_html__('Opal Woocommece', 'notiz'),
	    "params" => $params
	));
}