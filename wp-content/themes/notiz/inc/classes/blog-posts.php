<?php
class Notiz_WPO_Blogposts{
	
	// Ajax Display Layout
	public function __construct(){

		add_action( 'wp_ajax_wpo_load_more_posts', array($this,'WPO_LoadMore_Posts') );
		add_action( 'wp_ajax_nopriv_wpo_load_more_posts', array($this,'WPO_LoadMore_Posts') );
	}

	public function WPO_LoadMore_Posts(){
		$args = $_POST['query'];


		$type       	= $_POST['type'];
		$style       	= $_POST['style'];
		$grid_columns 	= $_POST['column'];
		$args['paged']   	= $_POST['paged'];
		$loop       	= new WP_Query($args);

		$result       = $posts = array();
		$_count= 0;

		if($loop->have_posts()){
			ob_start();
			
			while($loop->have_posts()):  $loop->the_post();
                get_template_part( 'templates/post/single', $style );
                 if($_count%2==1){
                    echo '<div class="clearfix"></div>';
                }
                $_count++;
            endwhile;
			wp_reset_postdata();
            
            $posts = ob_get_clean();
		}

		if($_POST['paged'] >= $loop->max_num_pages)
			$result['check'] = false;
		else
			$result['check'] = true;

		$result['posts'] = $posts;
		print_r(json_encode($result));
		exit();
	}
}
new Notiz_WPO_Blogposts();