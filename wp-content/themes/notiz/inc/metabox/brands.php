
<p class="wpo_section ">
	<?php 
		$brand_link = array(
            'id'    => 'brand_link',
            'title' => esc_html__('Link brand', 'notiz'),
            'des'   => esc_html__('(Enter link band)', 'notiz')
        );
        $mb->addTextElement( $brand_link );
	?>
</p>