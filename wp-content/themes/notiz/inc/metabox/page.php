<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */
global $wp_registered_sidebars;
$meta_tabs = array(
        array(
            'id'    => 'wpo-config',
            'icon'  => 'fa-wrench',
            'title' => esc_html__('General', 'notiz')
        ),
        array(
            'id'    => 'option',
            'icon'  => 'fa-cogs',
            'title' => esc_html__('Template Option', 'notiz')
        )
    );

?>
<div id="wpo-post" class="wpo-metabox">
<!-- Nav tabs -->
    <?php $mb->getTabsConfig($meta_tabs); ?>

    <!--Genaral config -->
    <div class="wpo-meta-content active" id="wpo-config">
	<?php
		$layout = array('id' => 'page_layout', 'title' => esc_html__('Page Layout','notiz') );
		$mb->selectLayout($layout);
	?>
	<div style="clear:both;"></div>
		<!--Select left sidebar-->
		<p class="wpo_section left-sidebar for-leftmain for-leftmainright target-page-layout">
		    <?php $mb->the_field('left_sidebar'); ?>
    <?php 
    	$left_sidebars = array('id'=> 'left_sidebar','title'=> esc_html__('Left Sidebar','notiz'),'data'=>$wp_registered_sidebars,'default'=>'sidebar-left');
        $mb->getSelectElement($left_sidebars);
    ?>
		</p>
		<!--Select right sidebar-->
		<p class="wpo_section right-sidebar for-mainright for-leftmainright target-page-layout">
    <?php 
        $right_sidebars = array('id'=> 'right_sidebar','title'=> 'Right Sidebar','data'=>$wp_registered_sidebars,'default'=>'sidebar-right');
        $mb->getSelectElement($right_sidebars); 
    ?>
		</p>

		<p class="wpo_section right-sidebar for-mainright for-leftmainright for-leftmain target-page-layout">
		    <?php 

		    	$columns = array();
		    	$columns[] = array(
		    		'id' => '', 'name' => esc_html__('Default','notiz'),
		    	);
		    	$columns[] = array(
		    		'id' => '2', 'name' => esc_html__('2 Columns','notiz'),
		    	);
		    	$columns[] = array(
		    		'id' => '3', 'name' => esc_html__('3 Columns','notiz'),
		    	);
		    	$columns[] = array(
		    		'id' => '4', 'name' => esc_html__('4 Columns','notiz'),
		    	);

		        $right_sidebars = array('id'=> 'sidebar_column','title'=> esc_html__('Sidebar Columns', 'notiz'),'data'=>$columns,'default'=>'');
		        $mb->getSelectElement($right_sidebars); 
		    ?>
		    <i><?php esc_html_e( 'Coloque o número de colunas do bootstrap para o sidebar.', 'notiz' );?></i>
		</p>


		<!--Select Layout-->
	    <p class="wpo_section">
			<?php
				$data_layout = array(
					array( 'id' => 'global', 'name' => 'Use Global'),
					array( 'id' => 'fullwidth', 'name' => 'Full width'),
					array( 'id' => 'boxed', 'name' => 'Boxed')
				);
				$layout = array(
		    		'id' => 'layout',
		    		'title' => 'Layout style',
		    		'data' => $data_layout,
		    		'default' => 'global'
				);
		    	$mb->getSelectElement( $layout );
			?><i><?php esc_html_e( 'Se a página estiver com full with', 'notiz' );?></i>
		</p>

		<!--Select skins-->

		<!--Select header skin-->
		<p class="wpo_section ">
    	<?php
	    	$header = notiz_wpo_cst_headerlayouts();
	    	$data = array(array( 'id' => 'global', 'name' => 'Use Global'));
	    	foreach ($header as $key => $value) {
	    		$data[] = array('id'=> $key,'name' => $value);
	    	}
	    	$header_skin = array(
	    		'id' => 'header_skin',
	    		'title' => 'Header Skin',
	    		'data' => $data,
	    		'default' => 'global'
			);
	    	$mb->getSelectElement( $header_skin );
    	?>
	    </p>

       <p class="wpo_section">
       <?php 
         $footer = notiz_wpo_get_footerbuilder_profiles();
         $data = array(array( 'id' => 'global', 'name' => 'Use Global'));
         foreach ($footer as $key => $value) {
            $data[] = array('id'=> $key,'name' => $value);
         }
         $footer_skin = array(
            'id' => 'footer_skin',
            'title' => 'Footer Skin',
            'data' => $data,
            'default' => 'global'
         );
         $mb->getSelectElement( $footer_skin );
       ?>
       </p>
	 		<!--show title config -->
 		<p class="wpo_section">
        <?php
            $_show_title = array('id'=>'showtitle', 'title'=>esc_html__('Show Title', 'notiz') );
            $mb->getCheckboxElement($_show_title); 
        ?>
        </p>

		  <!--Show Breadcrumb config -->
        <p class="wpo_section show_breadcrumb">
        <?php
            $_show_breadcrumb = array('id'=>'breadcrumb', 'title'=>esc_html__('Show Breadcrumb', 'notiz') );
            $mb->getCheckboxElement($_show_breadcrumb); 
        ?>
        </p>
		
	</div>
	<div class="wpo-meta-content" id="option">
		<!--Blog config -->
		<p class="wpo_section wpo-check wpo-template-blog-template">
		<?php
			$number = array(
                'id'    => 'blog_number',
                'title' => 'Number post',
                'des'   => '(Number post per page)'
            );
			$mb->addNumberElement( $number );
		?>
		</p>
		<p class="wpo_section wpo-check wpo-template-blog-template" data-group="style_blog" data-id="default:list:masonry:grid">
		<?php
			$path = trailingslashit( get_template_directory() ).'/templates/blog/blog-*.php';
			$file_name = 'blog-';
			$styles = notiz_wpo_get_styles($path, $file_name);
			foreach( $styles as $key=>$val){
				$data_styles[] = array('id'=> $key,'name' => $val);
			}
			$_styles = array(
		    		'id' => 'blog_style',
		    		'title' => 'Blog style',
		    		'data' => $data_styles,
		    		'default' => ''
				);
	    	$mb->getSelectElement( $_styles );
		?>
		</p>

		<p class="wpo_section wpo-check wpo-template-blog-template style_blog masonry grid">
		<?php 
			$column = array(
	    		'id' => 'blog_columns',
	    		'title' => 'Posts Show Columns',
	    		'data' => array(
	    			array('id' => 2, 'name' => '2 columns'),
	    			array('id' => 3, 'name' => '3 columns'),
	    			array('id' => 4, 'name' => '4 columns'),
    			),
	    		'default' => '2'
			);
	    	$mb->getSelectElement( $column );
	    	
    	?>
		</p>
		<p class="wpo_section wpo-check wpo-template-blog-template style_blog grid list">
		<?php 
			$is_show = array(
	    		'id' => 'show_listgrid',
	    		'title' => 'Show button list/grid',
	    		'data' => array(
	    			array('id' => 1, 'name' => 'Yes'),
	    			array('id' => 0, 'name' => 'No'),
    			),
	    		'default' => 1
			);
	    	$mb->getSelectElement( $is_show );
	    	
    	?>
		</p>

		<!--Portfolio config -->
		<p class="wpo_section wpo-check wpo-template-template-portfolio">
		<?php
			$data_number = array(
                'id'    => 'portfolio_number',
                'title' => 'Number portfolio per page',
                'des'   => ''
            );
			$mb->addNumberElement( $data_number );
		?>
		</p>
		<p class="wpo_section wpo-check wpo-template-template-portfolio" data-group="style_portfolio" data-id="default:masonry">
		<?php
			$portfolio_path = trailingslashit( get_template_directory() ).'/templates/portfolio/portfolio-*.php';
			$portfolio_file = 'portfolio-';
			$portfolio_styles = notiz_wpo_get_styles($portfolio_path, $portfolio_file);
			$portfolio_data = array();
			foreach( $portfolio_styles as $_key=>$_val){
				$portfolio_data[] = array('id'=> $_key,'name' => $_val);
			}
			$styles_portfolio = array(
		    		'id' => 'portfolio_style',
		    		'title' => 'Portfolio style',
		    		'data' => $portfolio_data,
		    		'default' => ''
				);
	    	$mb->getSelectElement( $styles_portfolio );
		?>
		</p>

		<p class="wpo_section wpo-check wpo-template-template-portfolio">
		<?php 
			$_column = array(
	    		'id' => 'portfolio_columns',
	    		'title' => 'Portfolio Show Columns',
	    		'data' => array(
	    			array('id' => 2, 'name' => '2 columns'),
	    			array('id' => 3, 'name' => '3 columns'),
	    			array('id' => 4, 'name' => '4 columns'),
    			),
	    		'default' => '2'
			);
	    	$mb->getSelectElement( $_column );
    	?>
		</p>

	</div>
</div>