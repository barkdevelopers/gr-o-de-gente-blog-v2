<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
$quote_content = array('type' => 'wpo_postconfig', 'format' =>'quote_content');
$quote_author = array('type' => 'wpo_postconfig', 'format' =>'quote_author');
?>
<div class="entry-thumb">
	<blockquote class="blockquote blockquote-success blockquote-left space-20">
      <i class="fa fa-quote-left blockquote-icon bg-success radius-x"></i>
   <div class="blockquote-in">
		<p><?php if(!is_single()) echo '<a class="quick-link" href="'.get_the_permalink().'">' ?><?php echo notiz_wpo_getcontent( $quote_content); ?><?php if(!is_single()) echo '</a>'; ?></p>
		<small><?php echo notiz_wpo_getcontent( $quote_author); ?></small>
	</div>
	</blockquote>
</div>

