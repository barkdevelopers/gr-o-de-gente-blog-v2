<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
$postformat = get_post_format();

$icon = notiz_wpo_get_format_icon_class($postformat);
$class = 'post-default post-masonry';
if(is_single()) $class .= ' post-single'; else $class .= ' not-post-single';

wp_enqueue_script( 'wpo_isotope_js', WPO_THEME_URI.'/js/isotope.pkgd.min.js', array( 'jquery' ) );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
        <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>          
        <?php endif; ?>
        <div class="post-container clearfix">
            <div class="blog-post-detail">
                <figure class="entry-thumb">
                    <?php notiz_wpo_post_thumbnail('entry-image zoom-2'); ?>
                    <!-- entry-image -->
                </figure>
                <div class="entry-data">
                    <div class="entry-meta-top">
                        <div class="entry-created bg-darker space-padding-top-10">
                            <span class="month"><?php echo get_the_date( 'M' ); ?></span>
                            <span class="date"><?php echo get_the_date('d'); ?></span>
                        </div>
                        <?php if(!empty($icon)){ ?>
                            <div class="icon-post color-white text-center">
                                <span class="space-padding-top-10 text-white <?php echo esc_attr($icon); ?>"></span>
                            </div>    
                        <?php } ?>
                    </div>
                    <div class="entry-category">
                        <?php $cats = get_the_category(); ?>
                            <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                    <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                <?php } // endif ?>
                            <?php } // endif ?>
                    </div> 
                    <h3 class="entry-title">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>

                    <!-- <div class="entry-meta">
                        <span class="author-link"><?php esc_html_e('Por ', 'notiz') ?><?php the_author_posts_link(); ?></span>
                        <span class="meta-sep space-padding-lr-5"> . </span>
                        <span class="comment-count">
                            <?php comments_popup_link(esc_html__(' nenhum comentário', 'notiz'), esc_html__(' 1 Comentário', 'notiz'), esc_html__(' % Comentários', 'notiz')); ?>
                        </span>
                    </div> -->

                    <p class="entry-content">
                        <?php echo notiz_wpo_excerpt(32); ?>
                    </p>

                    <!-- <div class="entry-link">
                        <a href="<?php the_permalink(); ?>" class="btn btn-outline"><?php esc_html_e( 'Leia mais','notiz' ); ?></a>
                    </div> -->
                </div>
            </div>
        </div>
    </article>