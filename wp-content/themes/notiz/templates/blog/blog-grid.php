<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
$postformat = get_post_format();
$icon = notiz_wpo_get_format_icon_class($postformat);
if(is_single()) $class = ' post-single'; else $class = ' not-post-single';
?>
<?php
  $post_category = "";
  $categories = get_the_category();
  $separator = ' | ';
  $output = '';
  if($categories){
    foreach($categories as $category) {
      $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( "View all posts in %s", 'notiz' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
    }
  $post_category = trim($output, $separator);
  }      
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
        <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
        <?php endif; ?>
        <div class="post-container">
            <div class="blog-post-detail blog-post-grid">
                <figure class="entry-thumb">
                    <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                        <?php the_post_thumbnail('postthumb-grid');?>
                    </a>
                </figure>
                <div class="entry-data">
                    <div class="entry-meta-2">
                        <span class="category">
                            <?php echo trim($post_category); ?>
                        </span>
                        
                        <?php if(!empty($icon)){ ?>
                            <span class="post-icon">
                                <i class="<?php echo esc_attr($icon); ?>"></i>
                            </span>
                        <?php } ?>
                        
                        <!-- post-icon -->
                    </div>
                    <!-- entry-meta-2 -->
                    <h3 class="entry-title">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    
                    <p class="entry-description">
                        <?php echo notiz_wpo_excerpt(10); ?>
                    </p>

                    <div class="entry-meta">
                        <!-- <span class="entry-date"><?php //echo get_the_date(); ?></span> -->
                        <!-- <span class="meta-sep"> / </span> -->
                        <span class="comment-count">
                            <?php comments_popup_link(esc_html__(' nenhum comentário', 'notiz'), esc_html__(' 1 comentário', 'notiz'), esc_html__(' % Comentários', 'notiz')); ?>
                        </span>
                        <span class="meta-sep"> / </span>
                        <span class="author-link"><?php the_author_posts_link(); ?></span>
                        <?php if(is_tag()): ?>
                            <span class="meta-sep"> / </span>
                            <span class="tag-link"><?php the_tags('Tags: ',', '); ?></span>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
    </article>
