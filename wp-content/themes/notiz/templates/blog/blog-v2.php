<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
$postformat = get_post_format();
global $post;
$icon = notiz_wpo_get_format_icon_class($postformat);
$class = '';
if(is_single()) $class .= ' post-single'; else $class .= ' not-post-single blog-v1';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
        <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
          
        <?php endif; ?>
        <div class="post-container clearfix">
            <div class="blog-post-detail">
                <figure class="entry-thumb">
                   
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="entry-image">
                      <?php the_post_thumbnail('thumbnails-post');?>
                    </a>
 
                    <div class="entry-meta-top">
                        <?php if(!empty($icon)){ ?>
                            <div class="icon-post bg-theme color-white text-center">
                                <span class="text-white <?php echo esc_attr($icon); ?>"></span>
                            </div>    
                        <?php } ?>
                        
                         <?php if(is_sticky()){ ?>
                           <div class="hidden icon-post icon-sticky bg-red color-white text-center">
                                <span class="text-white fa fa-thumb-tack"></span>
                            </div> 
                        <?php } ?>
                    </div>   
                    <div class="author-avatar">  <?php echo get_avatar( get_the_author_meta( 'email' ), 64 ) ?> </div> 
                    <div class="entry-category"><?php echo notiz_wpo_the_category( $post->ID );?> </div>
                </figure>
                <div class="entry-data">
                    <h3 class="entry-title">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>

                    <div class="entry-meta">
                         <?php notiz_wpo_posted_on(); ?>
                        <span class="meta-sep space-padding-lr-5"> . </span>
                        <span class="comment-count">
                            <?php comments_popup_link(esc_html__(' nenhum comentário', 'notiz'), esc_html__(' 1 comentário', 'notiz'), esc_html__(' % Comentários', 'notiz')); ?>
                        </span>
                    </div>
 
                </div>
            </div>
        </div>
    </article>