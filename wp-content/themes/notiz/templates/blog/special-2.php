<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
?>

<?php
    $postformat = get_post_format();
    $icon = notiz_wpo_get_format_icon_class($postformat);
    $post_category = "";
    $categories = get_the_category();
    $separator = ' | ';
    $output = '';
    if($categories){
        foreach($categories as $category) {
          $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( "View all posts in %s", 'notiz' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
        }
        $post_category = trim($output, $separator);
    }      
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('nice-style v2'); ?> >
    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
    <?php endif; ?>
    <div class="post-container">
        <div class="blog-post-detail blog-post-grid">
            <figure class="entry-thumb">
                <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                    <?php notiz_wpo_post_thumbnail(); ?>
                </a>
                <!-- entry-image -->
            </figure>
            <div class="entry-detail">
                <div class="entry-data">
                    <div class="left col-xs-3 no-padding">
                        <div class="entry-meta">
                            <!-- <span class="entry-date">
                                <span class="date"><?php // echo get_the_date('d'); ?></span>
                                <span class="month"><?php // echo get_the_date('M'); ?></span>
                            </span> -->
                             <?php if(!empty($icon)){ ?>
                            <span class="post-icon">
                                <i class="<?php echo esc_attr($icon); ?>"></i>
                            </span>
                        <?php } ?>
                            <!-- post-icon -->                       
                        </div>
                    </div>
                    <div class="right col-xs-9 no-padding">    
                        <div class="entry-category">
                            <?php $cats = get_the_category(); ?>
                                <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                    <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                        <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                    <?php } // endif ?>
                                <?php } // endif ?>
                        </div>
                        <h3 class="entry-title">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="description">
                <?php the_excerpt(); ?>
                </div>
            </div>
            <!-- entry-detail -->
            <div class="entry-content-footer clearfix">
                <div class="pull-left">
                    <div class="author-avatar">
                        <?php echo get_avatar( get_the_author_meta( 'email' ), 64 ) ?>
                    </div>
                    <span class="author-link"><?php the_author_posts_link(); ?></span>
                </div>

                <div class="pull-right">
                    <span class="comment-count">
                        <i class="fa fa-comments-o"></i>
                        <?php comments_popup_link(esc_html__(' 0', 'notiz'), esc_html__(' 1', 'notiz'), esc_html__(' %', 'notiz')); ?>
                    </span>
                </div>
            </div>
            <!-- /entry-content-footer -->
        </div>
    </div>
</article>
