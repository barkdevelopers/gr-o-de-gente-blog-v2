<div class="posts-inner">
    <div class="row">
    <?php
        $i = 0;
        while($loop->have_posts()): $loop->the_post();
     ?>
        <?php if( $i == 0 ): ?>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <?php get_template_part( 'templates/post/_single' ) ?>
            </div>
        <?php elseif( $i >=1 && $i <=2 ): ?>
            <?php if($i==1): ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
            <?php endif; ?>
            <?php $style = ($i==1)? 'v5': 'v2';?>
                <?php get_template_part( 'templates/post/_single', $style ) ?>
            <?php if($i==2 || $i==$loop->post_count-1): ?>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <?php if($i==3): ?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="media-wrapper">
                        <?php endif; ?>
                            <?php get_template_part( 'templates/post/_single', 'v3' ) ?>
                        <?php if($i==7 || $i == $loop->post_count-1): ?>
                    </div>            
                </div>
            <?php endif; ?> 
        <?php endif;?>
        <?php  $i++; ?>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
    
    </div>
</div>