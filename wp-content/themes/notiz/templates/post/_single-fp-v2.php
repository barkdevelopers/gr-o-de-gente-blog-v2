<?php $thumbsize = isset($thumbsize)? $thumbsize : 'full';?>
<article class="post space-20">
<?php if ( has_post_thumbnail() ){ ?>
    <figure class="entry-thumb">
        <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
            <?php the_post_thumbnail( $thumbsize );?>
        </a>
    </figure>
    <div class="entry-content">
        <div class="entry-meta">
            <div class="entry-category">
                <?php $cats = get_the_category(); ?>
                <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                    <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                        <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                    <?php } // endif ?>
                <?php } // endif ?>
            </div>
        </div>
        <?php if (get_the_title()) { ?>
            <h4 class="entry-title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h4>
        <?php } ?>
    </div>   
<?php } ?>
</article>