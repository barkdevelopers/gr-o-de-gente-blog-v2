<?php
  $post_category = "";
  $categories = get_the_category();
  $separator = ' | ';
  $output = '';
  if($categories){
    foreach($categories as $category) {
      $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( "View all posts in %s", 'notiz' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
    }
  $post_category = trim($output, $separator);
  }      
?>
<article class="post <?php echo get_post_format(); ?>">
    <div class="post-container">
        <div class="row">
        <?php if ( has_post_thumbnail() ): ?>
            <div class="col-md-5 col-sm-5">
                <div class="post-thumb">
                    <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                        <?php the_post_thumbnail();?>
                    </a>
                    <!-- entry-image -->
                    <?php do_action('wpo_rating') ?>
                </div>                
            </div>
        <?php endif; ?>
            <div class="entry-content col-md-7 col-sm-7">
                <div class="entry-meta-2">
                    <span class="category">
                        <?php $cats = get_the_category(); ?>
                            <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                    <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                <?php } // endif ?>
                            <?php } // endif ?>
                    </span>
                </div>
                <?php if (get_the_title()): ?>
                    <h4 class="entry-title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h4>
                <?php endif; ?>


                <?php if (! has_excerpt()) {
                        echo "";
                    } else {
                        ?>
                            <p class="entry-description"><?php echo notiz_wpo_excerpt(20,'...'); ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</article>
