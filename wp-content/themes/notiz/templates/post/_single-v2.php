<article class="post">
    <h4 class="entry-title">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h4>
    <div class="entry-content-inner clearfix">
        <div class="entry-meta">
            <!-- <span class="entry-date"><?php // the_time( 'M d, Y' ); ?></span> -->
        </div>
        <!-- end:entry-meta -->
    </div>
</article>