<?php $thumbsize = isset($thumbsize)? $thumbsize : 'thumbnails-post';?>
<?php
  $post_category = "";
  $categories = get_the_category();
  $separator = ' | ';
  $output = '';
  if($categories){
    foreach($categories as $category) {
      $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( "View all posts in %s", 'notiz' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
    }
  $post_category = trim($output, $separator);
  }

    $postformat = get_post_format();
    $icon = notiz_wpo_get_format_icon_class($postformat);      
?>
<article class="post <?php echo get_post_format(); ?>">
    <?php
    if ( has_post_thumbnail() ) {
        ?>
            <figure class="entry-thumb">
                <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                    <?php the_post_thumbnail( $thumbsize );?>
                </a>
                <!-- vote    -->
                <?php do_action('wpo_rating') ?>
                <?php if(!empty($icon)){ ?>
                    <span class="post-icon">
                        <i class="<?php echo esc_attr($icon); ?>"></i>
                    </span>
                <?php } ?>
                <!-- post-icon -->
            </figure>
            <!-- entry-thumb -->
        <?php
    }
    ?>
    <div class="entry-content">
        <?php
            if (get_the_title()) {
            ?>
                <h4 class="entry-title">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h4>
            <?php
        }
        ?>


        <?php
            if (! has_excerpt()) {
                echo "";
            } else {
                ?>
                    <p class="entry-description"><?php echo notiz_wpo_excerpt(22,'...'); ?></p>
                <?php
            }
        ?>

        <div class="entry-content-inner clearfix">
            <div class="entry-meta">
                <!-- <span class="entry-date"><?php // the_time( 'M d, Y' ); ?></span> -->
            </div>
            <!-- end:entry-meta -->
        </div>
        <!-- end:entry-content-inner -->
    </div>
    <!-- end:entry-content -->
    <div class="entry-content-footer clearfix">
        <div class="pull-left">
            <div class="author-avatar">
                <?php echo get_avatar( get_the_author_meta( 'email' ), 64 ) ?>
            </div>
            <span class="author-link"><?php the_author_posts_link(); ?></span>
        </div>

        <div class="pull-right">
            <span class="comment-count">
                <i class="fa fa-comments-o"></i>
                <?php comments_popup_link(esc_html__(' 0', 'notiz'), esc_html__(' 1', 'notiz'), esc_html__(' %', 'notiz')); ?>
            </span>
        </div>
    </div>
    <!-- /entry-content-footer -->
</article>
<!-- post -->