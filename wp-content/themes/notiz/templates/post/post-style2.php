<div class="posts-inner">
    <div class="row">
    <?php
        $i = 0;
        $img = wp_get_attachment_image_src($photo,'full');
        while($loop->have_posts()): $loop->the_post();

     ?>
        <?php if( $i <= 1 ): ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <?php get_template_part( 'templates/post/_single-v5' ) ?>
            </div>
        <?php else: ?>
            <?php if( $i==2): ?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="media-wrapper">
                        <?php endif; ?>
                            <?php get_template_part( 'templates/post/_single-v3' ) ?>
                        <?php if($i==5 || $i == $loop->post_count-1): ?>
                    </div>            
                </div>
            <?php endif; ?> 
        <?php endif;?>
        <?php  $i++; ?>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
    
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="banner-wrapper">
                <?php if(isset($img[0]) && $img[0]): ?>
                    <img src="<?php echo esc_url_raw($img[0]);?>" title="<?php echo esc_attr($ocategory->name);?>" alt="<?php echo esc_attr($ocategory->name);?>"/>
                <?php endif; ?>
            </div>            
        </div>
    </div>
</div>