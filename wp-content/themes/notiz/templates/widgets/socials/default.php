<div class="social-box clearfix">
    <div class="pull-left">
        <?php
            if ( $title ) {
                echo ($before_title)  . trim( $title ) . $after_title;
            }
        ?>
    </div>
    <ul class="socials-link pull-right clearfix">
        <?php foreach( $socials as $key=>$social):
            if( isset($social['status']) && !empty($social['page_url']) ): ?>
                <li>
                    <a href="<?php echo esc_url($social['page_url']);?>" class="<?php echo esc_attr($key); ?>">
                        <i class="fa fa-<?php echo esc_attr($key); ?>">&nbsp;</i>
                    </a>
                </li>
        <?php
            endif;
        endforeach;
        ?>
    </ul>
</div>