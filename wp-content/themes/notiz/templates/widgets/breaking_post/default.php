<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     Opal  Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
// Display the widget title
?>

<div class="breaking-news <?php echo esc_attr($class); ?>">
	<div class="pull-left">
		<?php
			if ( $title ) {
			    echo ($before_title)  . trim( $title ) . $after_title;
			}
		?>
	</div>
<?php
	$posts = notiz_wpo_get_breaking_post( $filter, $number);
	if($posts->have_posts()):
?>
	<div class="post-widget media-post-layout widget-content ">
		<div class="breaking-news-inner">
			<div class="widget-content text-center breaking-owl-carousel owl-carousel-play" data-ride="owlcarousel">
				<div class="owl-carousel" data-slide="<?php echo esc_attr($post_per_page); ?>" data-pagination="false" data-navigation="true" data-autoplay="true">
					<?php while ( $posts->have_posts() ): $posts->the_post(); ?>
						<div class="item">
							<div class="media-body">
								<h6 class="entry-title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h6>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
				<!-- control button -->
				<?php   if( $posts->post_count > $post_per_page ) { ?>
				<div class="carousel-controls">
					<a href="#productcarouse" data-slide="prev" class="left carousel-control carousel-xs radius-x">
						<i class="fa fa-chevron-left"></i>
					</a>
					<a href="#productcarouse" data-slide="next" class="right carousel-control carousel-xs radius-x">
						<i class="fa fa-chevron-right"></i>
					</a>
				</div>
				<?php } ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
</div>