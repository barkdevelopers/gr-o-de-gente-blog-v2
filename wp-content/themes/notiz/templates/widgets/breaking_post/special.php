<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     Opal  Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
// Display the widget title
?>

<div class="breaking-news <?php echo esc_attr($class); ?>">
<?php
	$posts = notiz_wpo_get_breaking_post( $filter, $number);
	if($posts->have_posts()):
?>
	<div class="post-widget media-post-layout widget-content special">
		<div class="widget-content">
			<div class="row">
				<?php while ( $posts->have_posts() ): $posts->the_post(); ?>
					<div class="item col-md-4 col-sm-4 col-xs-12">
						<div class="media-body">
							<!-- <span class="entry-date">
                                <span class="date"><?php // echo get_the_date('d'); ?></span>
                                <span class="month"><?php // echo get_the_date('M'); ?></span>
                            </span> -->
							<h6 class="entry-title">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h6>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
<?php endif; ?>
</div>