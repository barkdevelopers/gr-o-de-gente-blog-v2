 
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="post-container">
			<div class="post-thumb">
				<div class="video-responsive">
					<?php notiz_wpo_embed(); ?>
				</div>
			</div>
		 		 <div class="post-container">

                    <header class="page-heading">
                        <div class="author-avatar">
                                 <?php echo get_avatar( get_the_author_meta( 'email' ), 64 ) ?>
                            </div>

                            <div class="entry-meta">
                                <?php notiz_wpo_posted_on(); ?>
                                <span class="meta-sep"> / </span>
                                <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
                                <span class="comments-link"><?php comments_popup_link( esc_html__( 'Deixe um comentário', 'notiz' ), esc_html__( '1 Comentário', 'notiz' ), esc_html__( '% Comentários', 'notiz' ) ); ?></span>
                               
                                <?php endif; ?>

                                <?php edit_post_link( esc_html__( 'Editar', 'notiz' ), '<span class="edit-link">', '</span>' ); ?>
                            </div><!-- .entry-meta -->


                       <?php if( notiz_wpo_theme_options('blog_show-title', true) ){ ?>   
                            <div class="entry-name">
                                <h1 class="entry-title"> <?php the_title(); ?> </h1>
                            </div>    
                        <?php } ?>
                     </header>
                    <div class="entry-content">
                        <?php
                            the_content( esc_html__( 'Continue reading <span class="meta-nav">&rarr;</span>', 'notiz' ) );
                            wp_link_pages( array(
                                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Páginas:', 'notiz' ) . '</span>',
                                'after'       => '</div>',
                                'link_before' => '<span>',
                                'link_after'  => '</span>',
                            ) );
                        ?>
                    </div> 
                
             </div>
     
		</div><!--  End .post-container -->
	</article>
