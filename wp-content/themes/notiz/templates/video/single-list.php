<?php $thumbsize = isset($thumbsize)? $thumbsize : 'thumbnails-post';?>
<?php
  $post_category = "";
  $categories = get_the_category();
  $separator = ' | ';
  $output = '';
  if($categories){
    foreach($categories as $category) {
      $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( "View all posts in %s", 'notiz' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
    }
  $post_category = trim($output, $separator);
  }      
?>
<article class="post">
    <?php if ( has_post_thumbnail() ) { ?>
        <figure class="entry-thumb">
            <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                <?php the_post_thumbnail( $thumbsize );?>
            </a>
        </figure>
    <?php } ?>
    <div class="entry-data">
     
        <h4 class="entry-title">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h4>
        <div class="entry-content clearfix">
            <div class="entry-meta">
                <div class="entry-category hidden">
                    <?php the_category(); ?>
                </div>
                <ul class="entry-comment list-inline hidden">
                    <li class="comment-count">
                        <?php comments_popup_link(esc_html__(' 0 ', 'notiz'), esc_html__(' 1 ', 'notiz'), esc_html__(' % ', 'notiz')); ?>
                    </li>
                </ul>

                 <div class="entry-create">
                    <span class="author"><?php echo esc_html_e('Por ', 'notiz'); the_author_posts_link(); ?></span>
                    <!-- <span class="entry-date"><?php // the_time( 'M d, Y' ); ?></span> -->
                </div>
            </div>
            
            <?php if ( has_excerpt()) { ?>
            <p class="entry-description"><?php echo notiz_wpo_excerpt(35,'...'); ?></p>
            <?php } ?>
        </div>
    </div>
</article>