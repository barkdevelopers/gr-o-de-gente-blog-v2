<?php
  $post_category = "";
  $categories = get_the_category();
  $separator = ' | ';
  $output = '';
  if($categories){
    foreach($categories as $category) {
      $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( esc_html__( "View all posts in %s", 'notiz' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
    }
  $post_category = trim($output, $separator);
  }      
?>
<?php

	if( $relates->have_posts() ): ?>
		<h4 class="related-post-title">
            <span><?php esc_html_e( 'Posts Relacionados', 'notiz' ); ?></span>
        </h4>

        <div class="related-posts-content">
            <div class="row">
    		<?php
                $class_column = floor( 12/$relate_count );
        		while ( $relates->have_posts() ) : $relates->the_post();
                    ?>
        			<div class="col-sm-<?php echo esc_attr($class_column); ?> col-md-<?php echo esc_attr($class_column); ?> col-lg-<?php echo esc_attr($class_column); ?>">
                        <div class="element-item">
                            <div class="entry-thumb">
                                <?php if ( has_post_thumbnail()) : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="entry-image zoom-2">
                                        <?php the_post_thumbnail('full'); ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                            
                            <div class="entry-content">
                                <div class="entry-meta-2">
                                    <span class="category">
                                        <?php $cats = get_the_category(); ?>
                                            <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                                <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                                    <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                                <?php } // endif ?>
                                            <?php } // endif ?>
                                    </span>
                                    <span class="post-icon">
                                        <?php 
                                            if ( 'video' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'audio' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'gallery' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'aside' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'chat' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'image' == get_post_format() ) {
                                                echo '<i class="fa"></i>'; 
                                            } elseif ( 'link' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'quote' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } elseif ( 'status' == get_post_format() ) {
                                                echo '<i class="fa"></i>';
                                            } else {
                                                echo '<i class="fa"></i>';
                                            } 
                                        ?>
                                    </span>
                                    <!-- post-icon -->
                                </div>
                                <h3 class="entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                                <div class="entry-content-inner clearfix">
                                    <div class="entry-meta">
                                        <div class="entry-create">                    
                                            <!-- <span class="entry-date"><?php // the_time( 'M d, Y' ); ?></span> -->
                                            <!-- <span>/</span> -->
                                            <span class="comment-count">
                                                <i class="fa fa-comments-o"></i>
                                                <?php comments_popup_link(esc_html__(' nenhum comentário', 'notiz'), esc_html__(' 1 Comentário', 'notiz'), esc_html__(' % Comentários', 'notiz')); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <?php
                endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <?php
    endif;
?>