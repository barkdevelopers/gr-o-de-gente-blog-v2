<?php
/**
* $Desc
*
* @version    $Id$
* @package    wpbase
* @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
* @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
* @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
*
* @website  http://www.wpopal.com
* @support  http://www.wpopal.com/support/forum.html
*/
?>

<div class="author-info">

	<div class="author-about-container media">
		<div class="avatar-img pull-left">
			<?php echo get_avatar( get_the_author_meta( 'user_email' ),72 ); ?>
		</div>
		<!-- .author-avatar -->
		<div class="description media-body">
			<div class="row">
				<div class="col-md-9 col-sm-9 col-xs-12">
					<h4 class="author-title">
						<?php esc_html_e( 'Postado por ', 'notiz' ); ?>
						<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
							<?php echo get_the_author(); ?>
						</a>
					</h4>
					<?php the_author_meta( 'description' ); ?>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="social-author pull-right"><?php notiz_wpo_get_link_social(); ?></div>
				</div>
			</div>
			
		</div>
		
	</div>
</div>