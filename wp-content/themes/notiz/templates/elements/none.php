<section class="cat-top  page-found clearfix">
	<header class="header-title">
		<h6 class="page-title"><?php esc_html_e( 'Nada encontrado', 'notiz' ); ?></h6>
	</header><!-- /header -->
</section>
<article class="wrapper">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

		<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'notiz' ), $allowed_html_array ), admin_url( 'post-new.php' ) ); ?></p>
	
	<?php elseif ( is_search() ) : ?>

	<p><?php esc_html_e( 'Desculpe, mas nada foi encontrado com os termos pesquisados.', 'notiz' ); ?></p>
	<?php get_search_form(); ?>

	<?php else : ?>

	<p><?php esc_html_e( 'Parece que não conseguimos achar oque estava procurando, Talvez a busca ajuda.', 'notiz' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</article>
