<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php locate_template('templates/preloader.php', true); ?>

     <section class="wpo-page row-offcanvas row-offcanvas-left"> <?php locate_template('templates/mobile/topbar.php', true);?>
    <?php
        $meta_template = get_post_meta(get_the_ID(),'wpo_template',true);
    ?>

    <!-- START Wrapper -->
    <section class="wpo-wrapper <?php echo isset($meta_template['el_class']) ? esc_attr( $meta_template['el_class'] ) : '' ; ?>">
        <!-- Top bar -->
        <section id="wpo-topbar" class="wpo-topbar topbar-v2">
    
            <div class="container">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                        <ul>
                        <?php if( !is_user_logged_in() ){ ?>
                            <li><?php do_action('wpo-login-button'); ?></li>
                            <li><a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>" title="<?php esc_html_e('Registrar','notiz'); ?>"><?php esc_html_e('Registrar','notiz'); ?></a></li>
                        <?php }else{ ?>
                            <?php $current_user = wp_get_current_user(); ?>
                            <li><?php esc_html_e('Bem Vindo ','notiz'); ?><?php echo trim($current_user->display_name); ?> !</li>
                            <li><a href="<?php echo wp_logout_url(); ?>"><?php esc_html_e('Deslogar', 'notiz'); ?></a></li>
                        <?php } ?>
                        </ul>
                        
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                        <?php 
                            if(is_active_sidebar( 'top-header' )){ 
                                dynamic_sidebar( 'top-header' );
                            }
                        ?>
                    </div>
                    
                </div>
                <!-- row -->
                
            </div>
            <!-- container -->

        </section>
        <!-- // Topbar -->
        <!-- HEADER -->
        <header id="wpo-header" class="wpo-header wpo-header-v2">
            <div class="container-inner header-wrap">
                <div class="container header-wrapper-inner">
                    <div class="row">
                        <!-- LOGO -->
                        <div class="logo-in-theme col-lg-2 col-md-2 col-sm-12 col-xs-12 space-top-35">
                            <?php if( notiz_wpo_theme_options('logo') ) { ?>
                            <div class="logo">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <img src="<?php echo esc_url_raw( notiz_wpo_theme_options('logo') ); ?>" alt="<?php bloginfo( 'name' ); ?>">
                                </a>
                            </div>
                            <?php } else { ?>
                                <div class="logo logo-theme">
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                         <img src="<?php echo get_template_directory_uri() . '/images/logo4.png'; ?>" alt="<?php bloginfo( 'name' ); ?>" />
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- end:LOGO -->
                        <!-- Main Menu -->
                        <div class="wpo-mainmenu-wrap col-lg-7 col-md-7 col-sm-12 col-xs-12">
                            <div class="mainmenu-content-wapper">
                                <div class="mainmenu-content text-right">
                                    <nav id="wpo-mainnav" data-style='light' data-duration="<?php echo notiz_wpo_theme_options('megamenu-duration',400); ?>" class="padding-large position-static  wpo-megamenu <?php echo notiz_wpo_theme_options('magemenu-animation','slide'); ?> animate navbar navbar-mega" role="navigation">

                                         <?php
                                            $args = array(  'theme_location' => 'mainmenu',
                                                            'container_class' => 'collapse navbar-collapse navbar-ex1-collapse space-padding-0',
                                                            'menu_class' => 'nav navbar-nav megamenu',
                                                            'fallback_cb' => '',
                                                            'menu_id' => 'account',
                                                            'walker' => class_exists("Wpo_Megamenu") ? new Wpo_Megamenu() : new Wpo_bootstrap_navwalker );
                                            wp_nav_menu($args);
                                        ?>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- end:Main menu -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <div class="box-quick-action hidden-xs hidden-sm">
                                <div class="search_form hidden-input">
                                    <?php get_search_form(); ?>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 hidden-sm hidden-xs">
                            <?php if( WPO_WOOCOMMERCE_ACTIVED ) { ?>
                                <?php if( notiz_wpo_theme_options('woo-show-minicart', true) ) : ?>
                                    <div class="cartwrapper">
                                        <?php get_template_part( 'woocommerce/cart/mini-cart-button-v2' ); ?>       
                                    </div>
                                <?php endif; ?>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- end:row -->
                </div>
                <!-- end:container -->
            </div>

        </header>
        <!-- //HEADER -->