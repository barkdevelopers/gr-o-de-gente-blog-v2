<?php
/*
*Template Name: Blog
*
*/
global $notizconfig;
// Get Page Config
$notizconfig = $wpoEngine->getPageConfig();

?>

<?php get_header( $wpoEngine->getHeaderLayout() );  ?>

 <?php 
 	if( isset($notizconfig['breadcrumb']) && $notizconfig['breadcrumb'] ){
    do_action( 'notiz_wpo_layout_breadcrumbs_render' ); 
  } 
?>  
    
 <?php do_action( 'notiz_wpo_layout_template_before' ) ; ?>
 <?php if($notizconfig['showtitle']){ ?>
      <header class="header-title">
        <div class="container">
          <h1 class="page-title"><?php the_title(); ?></h1>
        </div>  
      </header><!-- /header -->
    <?php } ?>

     <div class="post-area post-list blog-page blog-page-<?php echo (esc_attr($notizconfig['blog_style']) ?  esc_attr($notizconfig['blog_style']) : 'default'); ?> <?php echo ($notizconfig['blog_style']=='masonry')? 'blog-masonry ': ''; ?>" id="container">
         <?php get_template_part('contents-post');?>
     </div>

 <?php do_action( 'notiz_wpo_layout_template_after' ) ; ?>

<?php get_footer(); ?>