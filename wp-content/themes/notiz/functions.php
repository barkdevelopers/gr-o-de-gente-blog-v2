<?php
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */


@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

define( 'WPO_THEME_DIR', get_template_directory() );
define( 'WPO_THEME_TEMPLATE_DIR',  WPO_THEME_DIR . '/templates/' );
define( 'WPO_THEME_TEMPLATE_DIR_PAGEBUILDER', WPO_THEME_DIR.'/vc_templates/' );

define( 'WPO_THEME_INC_DIR', WPO_THEME_DIR.'/inc/' );
define( 'WPO_THEME_CSS_DIR', WPO_THEME_DIR.'/css/' );

define( 'WPO_THEME_URI', get_template_directory_uri() );

define( 'WPO_THEME_VERSION', '1.0' );


define( 'WPO_FRAMEWORK_CUSTOMZIME_STYLE_URI', WPO_THEME_URI.'/css/customize/' );
define( 'WPO_FRAMEWORK_ADMIN_STYLE_URI', WPO_THEME_URI.'/inc/assets/' );
define( 'WPO_FRAMEWORK_ADMIN_IMAGE_URI', WPO_FRAMEWORK_ADMIN_STYLE_URI.'images/' );
define( 'WPO_FRAMEWORK_STYLE_URI', WPO_THEME_URI.'/inc/assets/' );  

/**
 * Define constants storing status enable or disable  installed plugins.
 */
define( 'WPO_WOOCOMMERCE_ACTIVED', 	   in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'WPO_VISUAL_COMPOSER_ACTIVED', in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ); 
define( 'WPO_PLGTHEMER_ACTIVED', 	   in_array( 'wpo-notizframework/wpo-notizframework.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );

/* 
 * Localization
 */ 
$lang = WPO_THEME_DIR . '/languages' ;
load_theme_textdomain( 'notiz', $lang );

/**
 * batch including all files in a path.
 *
 * @param String $path : PATH_DIR/*.php or PATH_DIR with $ifiles not empty
 */
function notiz_wpo_includes( $path, $ifiles=array() ){

    if( !empty($ifiles) ){
         foreach( $ifiles as $key => $file ){
            $file  = $path.'/'.$file; 
            if(is_file($file)){
                require($file);
            }
         }   
    }else {
        $files = glob($path);
        foreach ($files as $key => $file) {
            if(is_file($file)){
                require($file);
            }
        }
    }
}

if ( ! isset( $content_width ) ) {
    $content_width = 600;
}

remove_theme_support( 'custom-header' );


function notiz_wpo_get_posttype_enable( $post_type){
    $posttypes = array();
    $opts = get_option( 'wpo_themer_posttype' );
    if( !empty($opts) ){
      foreach( $opts as $opt => $key ){
          $posttypes[] = str_replace( 'enable_', '', $opt );
      }  
    }
    return in_array( $post_type, $posttypes );
}

/*
 * Include list of files from Opal Framework.
 */ 
notiz_wpo_includes(  WPO_THEME_DIR . '/inc/plugins/*.php' );

 
notiz_wpo_includes(  WPO_THEME_DIR . '/inc/classes/*.php' );

if( is_admin() ) {
   /**
    * Admin Classess Core Frameworks Included
    */ 
    notiz_wpo_includes(  WPO_THEME_DIR . '/inc/classes/admin/*.php' );
 }


/// include list of functions to process logics of worpdress not support 3rd-plugins.
notiz_wpo_includes(  WPO_THEME_INC_DIR . '/functions/*.php' );

/// WooCommerce specified functions
if( WPO_VISUAL_COMPOSER_ACTIVED  ) {
    notiz_wpo_includes(  WPO_THEME_INC_DIR . '/vendors/visualcomposer/*.php' );
}

/// WooCommerce specified functions
if( WPO_WOOCOMMERCE_ACTIVED  ) {
    notiz_wpo_includes(  WPO_THEME_INC_DIR . '/vendors/woocommerce/*.php' );
}


/**
 * Theme Customizer
 */ 
notiz_wpo_includes(  WPO_THEME_INC_DIR . '/customizer/*.php' );
 
$wpoEngine = new Notiz_WPO_TemplateFront();
$protocol = is_ssl() ? 'https:' : 'http:';
$wpoEngine->init();