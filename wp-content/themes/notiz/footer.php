<?php
/**
* $Desc
*
* @version    $Id$
* @package    wpbase
* @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
* @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
* @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
*
* @website  http://www.wpopal.com
* @support  http://www.wpopal.com/support/forum.html
*/
global $footer_builder, $notizconfig;
$footer = notiz_wpo_theme_options('footer-style','default');
if('page' == get_post_type()){
	if($notizconfig['footer_skin'] && $notizconfig['footer_skin']!='global'){
		$footer = $notizconfig['footer_skin'];
	}
}

?>
	<?php if(is_active_sidebar('social')) dynamic_sidebar('social'); ?>

	<?php if( $footer !='default' ){
     //   echo do_shortcode($footer_builder['footer']);
        ?>
        	<footer id="wpo-footer" class="wpo-footer wpo-footer-builder <?php echo esc_attr( get_post($footer)->post_name ); ?>">
        		<div class="container<?php if( isset($notizconfig['layout'])&&$notizconfig['layout']=='fullwidth') { ?>-fuild<?php } ?>">
	            <?php 
	           	 	notiz_wpo_render_post_content( $footer );
	            ?>
	         </div>   
          	</footer>
	<?php }else{ ?>

	<footer id="wpo-footer" class="wpo-footer">
		<div class="container">
			<section class="container-inner">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<div class="inner wow fadeInUp">
							<?php dynamic_sidebar('footer-1'); ?>
						</div>
						<?php endif; ?>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="inner wow fadeInUp">
							<?php dynamic_sidebar('footer-2'); ?>
						</div>
						<?php endif; ?>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="inner wow fadeInUp">
							<?php dynamic_sidebar('footer-3'); ?>
						</div>
						<?php endif; ?>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
						<div class="inner wow fadeInUp">
							<?php dynamic_sidebar('footer-4'); ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</section>
		</div>
	</footer>

	<div class="wpo-copyright">
		<div class="container333">
			<div class="copyright">
				<address>
					<?php 
						
						$copyright =  notiz_wpo_theme_options('copyright_text', 'Copyright &#169; 2015 - <a>Notiz</a> - All rights reserved.<br>Powered by Wordpress.');
						if(!empty($copyright)){
							echo ($copyright);
						}else{
							echo wp_kses( __('Copyright &#169; 2015 - <a href="#">Notiz</a> - All rights reserved.<br>Powered by Wordpress.','notiz') ); 
						}
					?>
				</address>

			</div>
		</div>
	</div>	
	<?php } ?>
</section>
<!-- END Wrapper -->
<?php get_sidebar( 'offcanvas-left' );  ?>
</section>
<?php wp_footer(); ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery(".banner-wrapper").each(function() {
			if( jQuery(this).find("img").length ){
				var srcImage = jQuery(this).find("img").attr("src");

				if( typeof srcImage != "undefined"){
					if( srcImage.indexOf("blog270PromoRoupinhasII") > -1 ) {
						var html = "";
						html += "<a href='http://bit.ly/1Ys27sF' title='Roupinhas Descoladas' target='_blank'>";
							html += jQuery(this).html();
						html += "</a>";

						jQuery(this).html(html);
					}

					if( srcImage.indexOf("blog270DiaRei") > -1 ) {
						var html = "";
						html += "<a href='http://bit.ly/1Ys2mEj' title='Marinho Premium' target='_blank'>";
							html += jQuery(this).html();
						html += "</a>";

						jQuery(this).html(html);
					}
				}
			}
		});
	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41479635-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>