<?php 
  /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */    
    global $wp_query, $notizconfig;

if( !empty($wp_query)){
    if(is_category()){

        $style = notiz_wpo_theme_options('archive-style');
        $columns_count = (int)notiz_wpo_theme_options('archive-column', 4);
        $col = floor(12/$columns_count);
        $smcol = ($col > 4)? 6: $col;
        $class_column='col-lg-'.$col.' col-md-'.$col.' col-sm-'.$smcol;
        $post_per_page = get_option('posts_per_page');
        $show_listgrid = notiz_wpo_theme_options('show-listgrid');
    }elseif( is_page()) {

        $options = array(
            'blog_number' => 10,
            'blog_style' => '',
            'blog_columns' => 2,
            'show_listgrid' => true,
        );

        //$notizconfig     = apply_filter( 'wpo_posts_options', $notizconfig );
        $notizconfig     = array_merge( $options, $notizconfig );       
        $style          = $notizconfig['blog_style'] ;
        $post_per_page  = $notizconfig['blog_number'];
        $columns_count = $notizconfig['blog_columns'];

        $col = floor(12/$columns_count);

        $show_listgrid = $notizconfig['show_listgrid'];

        $smcol = ($col > 4)? 6: $col;
        $class_column='col-lg-'.$col.' col-md-'.$col.' col-sm-'.$smcol;
        
        if(is_front_page())
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        else
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        $args = array(
            'post_type' => 'post',
            'paged' => $paged,
            'posts_per_page'=> $post_per_page
        );

        $wp_query = new WP_Query($args);
    }else{
        $style = '';
        $post_per_page = get_option('posts_per_page');
        $columns_count = 1;
        $columns_count = (int)notiz_wpo_theme_options('archive-column', 4);
    }

}else{
    get_template_part( 'templates/elements/none' );
}

  $stringquery = (json_encode($wp_query->query_vars));
  if (isset($_COOKIE['wpo_cookie_layout_post'])) {
    $layout = $_COOKIE['wpo_cookie_layout_post'];
  }else{
    $layout = $style;
  }
  $_count = 0;

?>
<?php if ( have_posts() ) : ?>
  <?php if($style=='grid' || $style=='list' && $show_listgrid): ?>
    <div class="button-gridlist" id="wpo-listgrid-posts">
      <div class="display-post input-group">
          <span><?php esc_html_e('Página', 'notiz');?></span>
          <div class="class-button">
            <a style="position:relative;" class="btn-list <?php echo (($layout=='list')? 'active':''); ?>" data-colums='1' data-query='<?php echo esc_attr($stringquery); ?>' data-type="list"  href="javascript:void(0);">
              <i class="fa fa-list"></i>
            </a>
            <a style="position:relative;" class="btn-large <?php echo (($layout=='grid')? 'active':''); ?>" data-colums='<?php echo esc_attr($columns_count);?>' data-query='<?php echo esc_attr($stringquery); ?>' data-type="grid"  href="javascript:void(0);">
              <i class="fa fa-th"></i>
            </a>
          </div>
      </div>
  </div>
<?php endif; ?>

<div class="post-listgrid row <?php echo ($style=='masonry')? 'blog-masonry isotope-masonry': ''; ?> posts-<?php echo ($style ? $style : 'default') ?>">
  <?php if($style=='masonry'): ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="masonry-item <?php echo esc_attr($class_column); ?>">
          <?php get_template_part( 'templates/blog/blog', $style); ?>
      </div>
    <?php endwhile; ?>

  <?php elseif($style=='grid' && $layout=='grid'):?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="<?php echo esc_attr($class_column); ?>">
          <?php get_template_part( 'templates/blog/blog', $layout); ?>
        </div>
      <?php if($_count%$columns_count==$columns_count-1 || $_count==$wp_query->post_count-1): ?>
      <div class="clearfix"></div>
      <?php endif; ?>
      <?php $_count++; ?>
    <?php endwhile; ?>

  <?php else: ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="col-xs-12">
          <?php get_template_part( 'templates/blog/blog', $layout); ?>
      </div>
    <?php endwhile; ?>   
  <?php endif; ?>
  
</div>
<?php notiz_wpo_pagination_nav( $post_per_page,$wp_query->found_posts,$wp_query->max_num_pages ); ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php get_template_part( 'templates/elements/none' ); ?>
<?php endif; ?>