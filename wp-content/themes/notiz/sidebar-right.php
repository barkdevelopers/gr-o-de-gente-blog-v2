<?php  
	global $notizconfig;
?>
<?php if($notizconfig['right-sidebar']['show']){ 
	$pos = empty($notizconfig['right-sidebar']) ?notiz_wpo_theme_options('right-sidebar'): $notizconfig['right-sidebar']['widget'];
?>
	<div class="<?php echo esc_attr($notizconfig['right-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-right">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
		</div>
	</div>
<?php } ?>