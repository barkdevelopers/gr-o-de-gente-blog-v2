<?php
global $notizconfig;
$notizconfig = $wpoEngine->getPostConfig();
?>
<?php get_header( notiz_wpo_theme_options('headerlayout', '') );  ?>

	<?php do_action( 'notiz_wpo_layout_breadcrumbs_render' ); ?>

	<?php do_action( 'notiz_wpo_layout_template_before' ) ; ?>

			<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
			?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">

						<div class="entry-attachment">
							<?php
								$image_size = apply_filters( 'twentyfifteen_attachment_size', 'large' );

								echo wp_get_attachment_image( get_the_ID(), $image_size );
							?>

							<?php if ( has_excerpt() ) : ?>
								<div class="entry-caption">
									<?php the_excerpt(); ?>
								</div><!-- .entry-caption -->
							<?php endif; ?>

						</div><!-- .entry-attachment -->

						<div class="entry-content">
                        <?php
                            the_content( esc_html__( 'Continue lendo', 'notiz').' <span class="meta-nav">&rarr;</span>' );
                            wp_link_pages( array(
                                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Páginas:', 'notiz' ) . '</span>',
                                'after'       => '</div>',
                                'link_before' => '<span>',
                                'link_after'  => '</span>',
                            ) );
                        ?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php the_tags( '<footer class="entry-meta"><span class="tag-links"><span>'.esc_html__('Tags:', 'notiz').' </span>', ', ', '</span></footer>' ); ?>
						<?php edit_post_link( __( 'Editar', 'notiz' ), '<span class="edit-link">', '</span>' ); ?>
						<div class="wpo-post-next">
		                    <?php 
		                        previous_image_link('<p class="pull-left">%link</p>', esc_html__('Imagem anterior', 'notiz'), FALSE); 
		                        next_image_link('<p class="pull-right">%link</p>', esc_html__('Próxima imagem', 'notiz'), FALSE); 
		                    ?>
		                </div>
					</footer><!-- .entry-footer -->

				</article><!-- #post-## -->

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				// End the loop.
				endwhile;
			?>

		<?php do_action( 'notiz_wpo_layout_template_after' ) ; ?>

<?php get_footer(); ?>
