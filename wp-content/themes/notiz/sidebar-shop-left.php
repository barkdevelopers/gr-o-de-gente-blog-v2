<?php 
global $notizconfig;  
$pos = esc_attr( notiz_wpo_theme_options('woocommerce-archive-left-sidebar') );
?> 
<?php if($notizconfig['left-sidebar']['show']){ ?>
	<div class="<?php echo esc_attr($notizconfig['left-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-left">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
 
		</div>
	</div>
<?php } ?>
 