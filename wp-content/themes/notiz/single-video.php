<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
global $notizconfig;
$notizconfig = $wpoEngine->getPortfolioConfig();
 
get_header( notiz_wpo_theme_options('headerlayout', '') ); ?>

<?php do_action( 'notiz_wpo_layout_breadcrumbs_render' ); ?>	

<?php do_action( 'notiz_wpo_layout_template_before' ) ; ?>

	<?php while(have_posts()):the_post(); ?>
		<div class="video-single-wrapper">
			<?php get_template_part('templates/video/single'); ?>

			 <?php the_tags( '<footer class="entry-meta"><span class="tag-links"><span>'.esc_html__('Tags:', 'notiz').' </span>', ', ', '</span></footer>' ); ?>
                <?php if( notiz_wpo_theme_options('show-share-post', true) ){ ?>
                    <div class="post-share">
                        <div class="row">
                            <div class="col-sm-4">
                                <h6><?php esc_html_e( 'Compartilhe este post!','notiz' ); ?></h6>
                            </div>
                            <div class="col-sm-8">
                                <?php notiz_wpo_share_box(); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    <hr>
                    <div class="author-about clearfix">
                        <?php get_template_part('templates/elements/author-bio'); ?>
                    </div>
                    <hr>
                <?php comments_template(); ?>
                <div> <?php if( notiz_wpo_theme_options('show-related-post', true) ){
                    $relate_count = notiz_wpo_theme_options('blog-items-show', 4);

                    notiz_wpo_related_post($relate_count, 'post', 'category');
                } ?>
                </div>
		</div>
		<?php endwhile; ?>
 <?php do_action( 'notiz_wpo_layout_template_after' ) ; ?>

<?php get_footer(); ?>