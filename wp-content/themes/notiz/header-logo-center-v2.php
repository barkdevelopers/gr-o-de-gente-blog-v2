<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
     <?php locate_template('templates/preloader.php', true); ?>
   <section class="wpo-page row-offcanvas row-offcanvas-left"> <?php locate_template('templates/mobile/topbar.php', true);?>
    <?php
        $meta_template = get_post_meta(get_the_ID(),'wpo_template',true);
    ?>

    <!-- START Wrapper -->
    <section class="wpo-wrapper <?php echo isset($meta_template['el_class']) ? $meta_template['el_class'] : '' ; ?>">
        <!-- Top bar -->
        <section id="wpo-topbar" class="wpo-topbar topbar-light">
    
            <div class="container">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                        <?php 
                            if(is_active_sidebar( 'top-bar-left' )){ 
                                dynamic_sidebar( 'top-bar-left' );
                            }
                        ?>
                        
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">

                        <?php 
                            if(is_active_sidebar( 'top-bar-right' )){ 
                                dynamic_sidebar( 'top-bar-right' );
                            }
                        ?>
                        
                    </div>
                    
                </div>
                <!-- row -->
                
            </div>
            <!-- container -->

        </section>
        <!-- // Topbar -->
        <!-- HEADER -->
        <header id="wpo-header" class="wpo-header wpo-header-v5">

            <div class="container-inner header-wrap">
                
                <div class="header-top space-padding-bottom-45 space-padding-top-45">
                    <div class="container header-wrapper-inner">
                        <div class="row">
                            <!-- LOGO -->
                            <div class="logo-in-theme col-xs-12 text-center">
                                <?php if( notiz_wpo_theme_options('logo') ) { ?>
                                <div class="logo">
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                        <img src="<?php echo esc_url_raw( notiz_wpo_theme_options('logo') ); ?>" alt="<?php bloginfo( 'name' ); ?>">
                                    </a>
                                </div>
                                <?php } else { ?>
                                    <div class="logo logo-theme">
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                             <img src="<?php echo get_template_directory_uri() . '/images/logo3.png'; ?>" alt="<?php bloginfo( 'name' ); ?>" />
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- end:row -->                        
                    </div>
                    <!-- end:header-wrapper-inner -->
                </div>
                <!-- header-top -->
                
                
                <div class="header-middle">
                    <div class="container header-wrapper-inner header-quick-action">
                        <div class="row">
                            <div class="wpo-mainmenu-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12 position-static">
                                <div class="mainmenu-content-wapper">
                                    <div class="mainmenu-content text-right">
                                        <nav id="wpo-mainnav" data-style='light' data-duration="<?php echo notiz_wpo_theme_options('megamenu-duration',400); ?>" class="padding-large position-static  wpo-megamenu <?php echo notiz_wpo_theme_options('magemenu-animation','slide'); ?> animate navbar navbar-mega" role="navigation">

                                             <?php
                                                $args = array(  'theme_location' => 'mainmenu',
                                                                'container_class' => 'collapse navbar-collapse navbar-ex1-collapse space-padding-0',
                                                                'menu_class' => 'nav navbar-nav megamenu',
                                                                'fallback_cb' => '',
                                                                'menu_id' => 'main-menu',
                                                                'walker' => class_exists("Wpo_Megamenu") ? new Wpo_Megamenu() : new Wpo_bootstrap_navwalker );
                                                wp_nav_menu($args);
                                            ?>
                                        </nav>
                                    </div>
                                </div>

                            </div>
                            <!-- end:MENU -->
                        </div>
                        <!-- end:row -->
                        
                        <div class="box-quick-action hidden-xs hidden-sm">
                            <div class="search_form hidden-input">
                                <?php get_search_form(); ?>
                            </div>

                        </div>
                    </div>
                    <!-- end:header-wrapper-inner -->
                </div>
                <!-- header-middle -->

                <div class="header-bottom">
                    <div class="container header-wrapper-inner">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php
                                if(is_active_sidebar( 'short-news' )){ 
                                    dynamic_sidebar( 'short-news' );
                                }
                            ?>                                
                            </div>
                            <!-- end:col-lg-12 -->
                        </div>
                        <!-- end:row -->
                    </div>
                    <!-- end:container -->
                </div>
                <!-- end:header-bottom -->
                
            </div>

        </header>
<!-- //HEADER -->