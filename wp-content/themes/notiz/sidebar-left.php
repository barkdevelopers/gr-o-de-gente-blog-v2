<?php 
global $notizconfig;  
?> 
<?php 
if($notizconfig['left-sidebar']['show']){ 
	$pos = empty($notizconfig['left-sidebar']) ?notiz_wpo_theme_options('left-sidebar'): $notizconfig['left-sidebar']['widget'];
?>
	<div class="<?php echo esc_attr($notizconfig['left-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-left">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
 
		</div>
	</div>
<?php } ?>
 