<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

global $notizconfig;
$notizconfig = $wpoEngine->getPostConfig();
 
?>

<?php get_header( notiz_wpo_theme_options('headerlayout', '') );  ?>
      
<?php do_action( 'notiz_wpo_layout_breadcrumbs_render' ); ?>  

<?php do_action( 'notiz_wpo_layout_template_before' ) ; ?>
        <?php if( get_post_type() && get_post_type() != 'post' ) :   ?>
        <div class="post-<?php echo get_post_type(); ?>">
             <?php get_template_part( 'templates/'.get_post_type().'/single' ); ?>  
        </div>
        <?php else : ?>
        <div class="post-area single-blog">
            <?php  while(have_posts()): the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="post-container">
                        <div class="entry-thumb entry-preview">
                            <?php get_template_part( 'templates/preview/content', get_post_format() ); ?>                            
                        </div>    
                        <!-- entry-thumb -->

                        <div class="meta-box clearfix">
                            <div class="pull-left space-top-10">
                                <div class="entry-meta">
                                    <!-- <span class="entry-date"><?php //echo get_the_date(); ?></span> -->
                                    <!-- <span class="meta-sep"> / </span> -->
                                    <span class="comment-count">
                                        <i class="fa fa-comments-o"></i>
                                        <?php comments_popup_link(esc_html__(' nenhum comentário', 'notiz'), esc_html__(' 1 comentário', 'notiz'), esc_html__(' % Comentários', 'notiz')); ?>
                                    </span>

                                    <?php edit_post_link( esc_html__( 'Editar', 'notiz' ), '<span class="edit-link">', '</span>' ); ?>
                                </div><!-- .entry-meta -->
                            </div>
                            <div class="pull-right">
                                <div class="author-avatar">
                                    <?php echo get_avatar( get_the_author_meta( 'email' ), 64 ) ?>
                                </div>
                                <span class="author-link"><?php the_author_posts_link(); ?></span>
                            </div>
                        </div>
                        <!-- meta-box -->
         
                        <?php if( notiz_wpo_theme_options('blog_show-title', true) ){ ?>   
                            <div class="entry-name">
                                <h1 class="entry-title"> <?php the_title(); ?> </h1>
                            </div>    
                        <?php } ?>
                    <div class="entry-content">
                        <?php
                            the_content( esc_html__( 'Continue lendo', 'notiz').' <span class="meta-nav">&rarr;</span>' );
                            wp_link_pages( array(
                                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Páginas:', 'notiz' ) . '</span>',
                                'after'       => '</div>',
                                'link_before' => '<span>',
                                'link_after'  => '</span>',
                            ) );
                        ?>
                    </div><!-- .entry-content -->
                    
                 </div>
     

                <?php the_tags( '<footer class="entry-meta"><span class="tag-links"><span>'.esc_html__('Tags:', 'notiz').' </span>', ', ', '</span></footer>' ); ?>
                <div class="wpo-post-next">
                    <?php 
                        previous_post_link('<p class="pull-left">%link</p>', esc_html__('Matéria anterior', 'notiz'), FALSE); 
                        next_post_link('<p class="pull-right">%link</p>', esc_html__('Próxima matéria', 'notiz'), FALSE); 
                    ?>
                </div>
                <?php if( notiz_wpo_theme_options('show-share-post', true) ){ ?>
                    <div class="post-share">
                        <div class="row">
                            <div class="col-sm-4">
                                <h6><?php esc_html_e( 'Compartilhe este post!','notiz' ); ?></h6>
                            </div>
                            <div class="col-sm-8">
                                <?php notiz_wpo_share_box(); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    <div class="author-about clearfix">
                        <?php get_template_part('templates/elements/author-bio'); ?>
                    </div>
                    <div id="related-post"> <?php if( notiz_wpo_theme_options('show-related-post', true) ){
                        $relate_count = notiz_wpo_theme_options('blog-items-show', 4);

                        notiz_wpo_related_post($relate_count, 'post', 'category');
                    } ?>
                    </div>
                <?php comments_template(); ?>
                
            </article>    
           <?php endwhile; ?>
        </div>
        <?php endif; ?>
             
<?php do_action( 'notiz_wpo_layout_template_after' ) ; ?>

<?php get_footer(); ?>