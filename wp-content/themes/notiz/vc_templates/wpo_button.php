<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$btnClass = $btn_size.' '.$btn_color.' '.$btn_border_size.' '.$btn_radius;

// Enqueue needed icon font.
$icon_type = 'fontawesome';
vc_icon_element_fonts_enqueue( $icon_type );
$iconClass = isset( ${"icon_" . $icon_type} ) ? esc_attr( ${"icon_" . $icon_type} ) : 'fa fa-adjust';

$href = vc_build_link( $btn_link );

$color_icons =""; 
if($icons_color){
    $color_icons = 'style="color:'.$icons_color.';"';  
}

// Pick up icons
$iconClass = isset( ${"icon_" . $icon_type} ) ? ${"icon_" . $icon_type} : $defaultIconClass;
// Enqueue needed icon font.
vc_icon_element_fonts_enqueue( $icon_type );

?>

<div class="wpo-button <?php echo esc_attr($el_class ).' '.esc_attr( $alignment ); ?>" style="<?php echo ($margin ? ('margin: '. $margin) : '') ?>">
	<?php if( $btn_title || $btn_link ) {  ?>
		<a class="btn <?php echo esc_attr( $btnClass ); ?>" style="<?php echo ($width ? ('width: '. $width) : '') ?>"
			href="<?php echo (!empty($href['url']))? esc_url($href['url']): '#'; ?>" 
            <?php if( !empty($href['title'])){ ?>title="<?php echo esc_attr($href['title']);?>"<?php } ?> 
            <?php echo (!empty( $href['target']) )? 'target="_blank"': ''; ?>>

			<?php if( $is_icons ){  ?>
                <i class="<?php echo esc_attr( $iconClass ); ?> <?php echo esc_attr( $icons_size ); ?> <?php echo esc_attr( $icons_position ); ?>" <?php echo trim($color_icons); ?>></i>
            <?php } ?> 			

			<?php echo esc_attr($btn_title); ?>
		</a>
	<?php } ?>
</div>

 
