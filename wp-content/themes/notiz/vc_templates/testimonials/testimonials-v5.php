<div class="testimonials-body">
    <div class="testimonials-description"><?php the_content() ?></div>                            
    <h5 class="testimonials-name">
         <?php the_title(); ?>
    </h5>
</div>