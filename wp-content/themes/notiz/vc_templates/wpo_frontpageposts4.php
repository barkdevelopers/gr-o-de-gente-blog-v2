<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if(empty($loop)) return;
$this->getLoop($loop);
$args = $this->loop_args;
 
$loop = new WP_Query($args);
?>

<section class="widget frontpage-posts section-blog  <?php echo (($el_class!='')?' '.$el_class:''); ?>">
    <?php
        if($title!=''){ ?>
            <h3 class="widget-title visual-title <?php echo esc_attr( $alignment ); ?> widget-title-<?php echo esc_attr($widget_title_style); ?> <?php echo esc_attr($el_class); ?>">
                <span><?php echo trim($title); ?></span>
            </h3>
        <?php }
    ?>
    <div class="widget-content"> 
             <?php
/**
 * $loop
 * $class_column
 *
 */

$_count =1;

$colums = '3';
$bscol = floor( 12/$colums );
 $end = $loop->post_count;
// $num_mainpost = 2; 
?>

<div class="frontpage frontpage-4">
<div class="row">
<?php

    $i = 0;
    $main = $num_mainpost;

    while($loop->have_posts()){
        $loop->the_post();
 ?>
        <?php if( $i<=$main-1) { ?>
            <?php if( $i == 0 ) {  ?>
                <div  class="main-posts clearfix">
            <?php } ?>
            <?php $thumbsize = isset($thumbsize)? $thumbsize : 'thumbnail';?>
                        <div class="post col-sm-4 space-20">
                                 <?php get_template_part( 'templates/post/_single-v4' ) ?>
                        </div>



            <?php if( $i == $main-1 || $i == $end -1 ) { ?>
                </div>
            <?php } ?>
        <?php } else { ?>
                <?php if( $i == $main  ) { ?>
                     <div class="secondary-posts space-20">
                                <?php }  ?>
                                    <div class="col-sm-6">
                                        <?php get_template_part( 'templates/post/_single-v3' ) ?>
                                    </div>
                                <?php if( $i == $end-1 ) {   ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                <?php  $i++; } ?>
                <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
</section>