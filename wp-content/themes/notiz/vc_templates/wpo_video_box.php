<?php
	$atts = ( vc_map_get_attributes(  str_replace('.php','',basename(__FILE__)) , $atts ) );
	extract( $atts );
	if(empty($url)) return;
?>
<div class="widget wpo-video-box">
	<?php if(!empty($title)): ?>
		<h3 class="widget-title widget-title-<?php echo esc_attr($widget_title_style); ?> <?php echo esc_attr($el_class); ?>">
			<span><?php echo trim( $title ); ?></span>
		</h3>
	<?php endif; ?>

	<div class="entry-thumb post-type-video">
		<div class="video-thumb video-responsive">
			<?php echo wp_oembed_get( $url); ?>
		</div>
	</div>
	<div class="wpo-video-content">
		<div class="title-video">
			<h4 class="entry-title"><?php echo trim($title_video);?></h4>
		</div>
		<div class="desciption-video">
			<span><?php echo trim($content);?></span>
		</div>
	</div>
</div>