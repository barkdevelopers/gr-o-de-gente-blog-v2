<?php
    $atts = vc_map_get_attributes( $this->getShortcode(), $atts );
    extract( $atts );

    if(empty($loop)) return;
    $this->getLoop($loop);
    $args = $this->loop_args;
    $stringquery = json_encode($args) ;

    $posts = new WP_Query($args);
    if($posts->have_posts()):

?>

<section class="widget wpo-grid-list section-blog <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?> <?php echo esc_attr($style); ?>">
    <div class="header-gridpost">
        <div class="title-widget">
            <?php if( $title ): ?>
                <h3 class="widget-title visual-title widget-title-<?php echo esc_attr($widget_title_style); ?>">
                   <span><?php echo trim($title); ?></span>
                </h3>
            <?php endif; ?>
        </div>
        <?php if($listgrid): ?>
            <div class="button-gridlist" id="wpo-filter-posts">
                <div class="display-post input-group">
                    <span><?php esc_html_e('Veja', 'notiz');?></span>
                    <div class="class-button">
                        <a style="position:relative;" class="btn-list" href="javascript:void(0)">
                            <i class="fa fa-list"></i>
                        </a>
                        <a style="position:relative;" class="btn-large active" href="javascript:void(0)">
                            <i class="fa fa-th"></i>
                        </a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="widget-content blogs">
        <div class="content-inner display-grid">
        <?php
            $_count = 0;
            $grid_columns = 2;
            $layout = 'grid';
            while($posts->have_posts()):  $posts->the_post();
                get_template_part( 'templates/post/single', $style );
                if($_count%2==1){
                    echo '<div class="clearfix"></div>';
                }
                $_count++;
            endwhile;
        ?> 
        </div>
    </div>
    <!-- Button Load more -->
    <?php if($loadmore): ?>
        <div class="button-loadmore" id="wpo-loadmore-posts">
            <a href="javascript:void(0);" data-query='<?php echo esc_attr($stringquery); ?>' data-loading-text="<?php esc_html_e('Carregando...', 'notiz'); ?>" data-loadmore="true"
                data-paged="1" role="button" data-style='<?php echo esc_attr($style);?>' class="btn btn-md btn-normal border-2 radius-6x wpo_loadmore" data-type="<?php echo esc_attr($layout); ?>" data-column="<?php echo esc_attr($grid_columns);?>">
                <i class="fa fa-refresh"></i><?php esc_html_e(' Carregar mais', 'notiz'); ?>
            </a>
        </div>
    <?php endif; ?>
    
</section>
<?php endif;