<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$args = array(
    'who' => 'authors',
    'number' => $number,
    'orderby'    => 'meta_value_num',
    'order'      => 'DESC',
);
 
$users = get_users( $args );
$_id = notiz_wpo_makeid();
if( !empty( $users ) ):
    $_count = 0;

?>

<section class="widget frontpage-blog section-blog top-author-widget <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?> <?php echo esc_attr($author_style); ?>">
    <?php
        if(!empty($title)){ ?>
            <h3 class="widget-title visual-title <?php echo esc_attr($alignment).' '.esc_attr( $size ); ?> widget-title-<?php echo esc_attr($widget_title_style); ?>">
                <span><?php echo trim($title); ?></span>
            </h3>
        <?php }
    ?>
    <div class="widget-content">
        <div id="carousel-<?php echo esc_attr($_id); ?>" class="widget-content text-center owl-carousel-play" data-ride="owlcarousel"> 
            <div class="owl-carousel " data-slide="<?php echo esc_attr($columns); ?>" data-pagination="false" data-navigation="true">
                <?php foreach( $users as $user ): ?>
                    <?php $roles = $user->roles;?>
                    <?php if($_count%$rows==0): ?>
                        <div class="item">
                    <?php endif; ?>
                    <div class="content-user">
                        <div class="avatar-user">
                            <a href="<?php echo esc_url( $user->user_url ) ?>">
                                <?php echo get_avatar( $user->ID, 64 ) ?>
                            </a>
                        </div>
                        <div class="info-user">
                            <div class="name-user">
                                <a href="<?php echo esc_url( $user->user_url ); ?>">
                                    <?php echo trim( $user->display_name ); ?>
                                </a>
                            </div>
                            <div class="roles-user">
                                <span class="role-user"><?php echo trim($roles[0]); ?></span>&#32;&#47;&#32;
                                <span class="count-user"><?php echo count_user_posts($user->ID); ?><?php esc_html_e( ' posts', 'notiz' ); ?></span>
                            </div>
                        </div>
                    </div>
                    <?php if($_count%$rows==$rows-1 || $_count==count($users)-1): ?>
                        </div>
                    <?php endif; ?>
                    <?php $_count++; ?>
                <?php endforeach; ?>
            </div>
            <div class="control-button">
                <?php if( $rows*$columns  < count($users)) { ?>
                <a class="left carousel-control carousel-xs radius-x" href="#post-slide" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control carousel-xs radius-x" href="#post-slide" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php endif;