<?php
$grid_link = $grid_layout_mode = $title = $filter= '';
$posts = array();
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if(empty($loop)) return;
$this->getLoop($loop);
$args = $this->loop_args;
 
$loop = new WP_Query($args);
?>

<section class="widget frontpage-posts frontpage-6 <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
    <?php
        if($title!=''){ ?>
            <h3 class="widget-title visual-title">
                <span><?php echo trim($title); ?></span>
            </h3>
        <?php }
    ?>
    <div class="widget-content"> 
      <?php
        /**
         * $loop
         * $class_column
         *
         */
        $_count =0;
        $bscol = floor( 12/$grid_columns );
        ?>
        <div class="posts-grid">
            <?php
                $i =0; while($loop->have_posts()){  $loop->the_post(); ?>
                 <?php $thumbsize = isset($thumbsize)? $thumbsize : 'thumbnail';?>

                 <?php if(  $i++%$grid_columns==0 ) {  ?>
                <div class="posts-grid"><div class="row">
                <?php } ?>
                <div class="col-sm-<?php echo esc_attr($bscol); ?>">
                    <article class="post">
                        <div class="entry-content">
                           
                            <div class="entry-meta-2">
                                <span class="category">
                                    <?php $cats = get_the_category(); ?>
                                    <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                        <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                            <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                        <?php } // endif ?>
                                    <?php } // endif ?>
                                </span>
                            </div>

                            <?php if (get_the_title()) { ?>
                                <h4 class="entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h4>
                            <?php  } ?>
                        </div>
                    </article>
                </div>
                <?php if(  ($i%$grid_columns==0) || $i == $loop->post_count) {  ?>
                </div></div>
                <?php } ?>
            <?php   }  ?>
            <?php wp_reset_postdata(); ?>
        </div>
         


    </div>
</section>