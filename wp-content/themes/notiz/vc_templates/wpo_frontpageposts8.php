<?php
    $atts = vc_map_get_attributes( $this->getShortcode(), $atts );
    extract( $atts );

    if(empty($loop)) return;
    $this->getLoop($loop);
    $args = $this->loop_args;
    $stringquery = json_encode($args) ;

    $posts = new WP_Query($args);
    if($posts->have_posts()):

$my_query = new WP_Query($args);
$columgrid = floor(12/$grid_columns);
if(  empty($layout) ){
    $layout = 'blog';
}

$countposts = $args ['posts_per_page'];

?>

<section class="widget post blog-type frontpage-8 <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
    <div class="header-gridpost">
        <div class="title-widget">
            <?php if( $title ): ?>
                <h3 class="widget-title visual-title widget-title-<?php echo esc_attr($widget_title_style); ?>">
                   <span><?php echo trim($title); ?></span>
                </h3>
            <?php endif; ?>
        </div>
        <?php if($listgrid): ?>
            <div class="button-gridlist" id="wpo-filter-posts">
                <div class="display-post input-group">
                    <div class="class-button">
                        <a style="position:relative;" class="btn-list" href="javascript:void(0)">
                            <i class="fa fa-list"></i>
                        </a>
                        <a style="position:relative;" class="btn-large active" href="javascript:void(0)">
                            <i class="fa fa-th"></i>
                        </a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="widget-content blogs">
        <div class="content-inner display-grid">
            <?php $i=0; while ( $my_query->have_posts() ): $my_query->the_post(); ?>
                <?php if( $i++%$grid_columns == 0 ) { ?>
                    <div class="row">
                        <?php } ?>
                        <div class="col-sm-<?php echo esc_html($columgrid); ?> col-md-<?php echo esc_html($columgrid); ?> col-lg-<?php echo esc_html($columgrid); ?>">
                            <?php get_template_part( 'templates/blog/'.$layout ); ?>
                        </div>
                        <?php if( $i%$grid_columns==0 || $i==$my_query->post_count ) { ?>
                     </div>
                <?php } ?>
                <?php if( $i== $countposts){ break; } ?>
                <?php endwhile; ?> 
                <?php wp_reset_postdata(); ?>
        </div>
    </div>
    <!-- Button Load more -->
    <?php if($loadmore): ?>
        <div class="button-loadmore" id="wpo-loadmore-posts">
            <a href="javascript:void(0);" data-query='<?php echo esc_attr($stringquery); ?>' data-loading-text="<?php esc_html_e('Carregando...', 'notiz'); ?>" data-loadmore="true"
                data-paged="1" role="button" class="btn btn-md btn-normal border-2 radius-6x wpo_loadmore" data-type="<?php echo esc_attr($layout); ?>" data-column="<?php echo esc_attr($grid_columns);?>">
                <i class="fa fa-refresh"></i><?php esc_html_e(' Carregar mais', 'notiz'); ?>
            </a>
        </div>
    <?php endif; ?>
    
</section>
<?php endif;