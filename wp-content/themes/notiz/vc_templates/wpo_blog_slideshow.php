<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if(empty($loop)) return;
$this->getLoop($loop);
$args = $this->loop_args;

//register iosslider js
wp_enqueue_script( 'iosslider_js', WPO_THEME_URI.'/js/jquery.iosslider.min.js', array( 'jquery' ) );
?>

<section class="widget section-blog <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?> responsiveHeight">
    <?php if( $title ) { ?>
        <h3 class="widget-title visual-title">
           <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
    <div class='inner'>
        <div class="iosSlider">
            <div class ='slider'>
            <?php
                $loop = new WP_Query($args);
                if($loop->have_posts()):
                    while($loop->have_posts()): $loop->the_post();
            ?>
                    <div class = 'item post-<?php the_ID(); ?>'>
                        <?php get_template_part( 'templates/post/_single_slider' ) ?>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="loading">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
        </div>
        <?php if( 1 < $loop->post_count ): ?>
            <a class="left carousel-control fa fa-angle-left" href="#post-slide" data-slide="prev"></a>
            <a class="right carousel-control fa fa-angle-right unselectable" href="#post-slide" data-slide="next"></a>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </div>
</section>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.iosSlider').iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            infiniteSlider: true,
            snapSlideCenter: true,
            keyboardControls: true,
            onSlideChange: slideChange,
            onSlideComplete: slideComplete,
            onSliderLoaded: slideBegin,
            navNextSelector: jQuery('.left'),
            navPrevSelector: jQuery('.right'),
        });
        
    });

    function slideBegin(args) {
        jQuery('.iosSlider .slider .item:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
        jQuery(".loading").hide();
    
    }

    function slideChange(args) {
                        
        jQuery('.iosSlider .slider .item').removeClass('selected');
        jQuery('.iosSlider .slider .item:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
    
    }
    
    function slideComplete(args) {
                
        jQuery('.left, .right').removeClass('unselectable');

        if(args.currentSlideNumber == 1) {
    
            jQuery('.right').addClass('unselectable');
    
        } else if(args.currentSliderOffset == args.data.sliderMax) {
    
            jQuery('.left').addClass('unselectable');
    
        }
    
    }
</script>
