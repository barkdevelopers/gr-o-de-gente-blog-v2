<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$posts = array();

if(empty($loop)) return;
$this->getLoop($loop);
$args = $this->loop_args;
 
$loop = new WP_Query($args);
?>

<section class="widget frontpage-posts frontpage-5 <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
    <?php
        if($title!=''){ ?>
            <h3 class="widget-title visual-title <?php echo esc_attr($size).' '.$alignment; ?> widget-title-<?php echo esc_attr($widget_title_style); ?> <?php echo esc_attr($el_class); ?>">
                <span><?php echo trim($title); ?></span>
            </h3>
        <?php }
    ?>
    <div class="widget-content"> 
      <?php
        /**
         * $loop
         * $class_column
         *
         */

        $_count =0;

        $colums = $grid_columns;
        $bscol = floor( 12/$colums );

        ?>
         
        <div class="posts-grid">
            <?php
                $i =0; while($loop->have_posts()){  $loop->the_post(); ?>
                 
                <?php if(  $i++%$colums==0 ) {  ?>
                <div class="posts-grid"><div class="row">
                <?php } ?>
                <div class="col-sm-<?php echo esc_attr($bscol); ?>">
                    <article class="post">
                        <div class="entry-content text-center">
                           
                            <div class="entry-meta-2">
                                <span class="category">
                                    <?php $cats = get_the_category(); ?>
                                    <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                        <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                            <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                        <?php } // endif ?>
                                    <?php } // endif ?>
                                </span>
                            </div>

                            <?php if (get_the_title()) { ?>
                                <h4 class="entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h4>
                            <?php  } ?>
                            <!-- <p class="entry-date"><?php // the_time( 'd M, Y' ); ?></p> -->
                        </div>
                    </article>
                </div>
                <?php if(  ($i%$colums==0) || $i == $loop->post_count) {  ?>
                </div></div>
                <?php } ?>
            <?php   }  ?>
            <?php wp_reset_postdata(); ?>
        </div>
         


    </div>
</section>