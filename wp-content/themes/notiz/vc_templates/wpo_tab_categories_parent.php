<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

wp_enqueue_script( 'mheight_js', WPO_THEME_URI.'/js/jquery.matchHeight-min.js', array( 'jquery' ) );

$_id = notiz_wpo_makeid();
if(empty( $category_id) ) return;

$ocategory = get_category_by_slug( $category_id );

if ( !empty($ocategory) && !is_wp_error($ocategory) ): ?>

<section class="widget tab-categories-posts frontpage-posts section-blog layout-<?php echo esc_attr($post_style); ?>  <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
    <div class="tab-title clearfix">
        <ul role="tablist" class="nav nav-tabs pull-left">
            <li role="tab" class="active">
                <h3 class="widget-title visual-title">
                    <a data-toggle="tab" href="#<?php echo esc_attr($ocategory->slug).'-'.esc_attr($_id);?>" title="<?php echo esc_attr( $ocategory->name); ?>" role="tab">
                    <?php if(!empty( $title) ): ?>
                        <span><?php echo trim($title); ?></span>
                    <?php else: ?>
                        <span><?php echo trim($ocategory->name); ?></span>
                    <?php endif; ?>
                    </a>
                </h3>
            </li>
        <?php
            $categories = get_categories('parent='.$ocategory->term_id);
            if( $categories && !is_wp_error($categories)):
        ?> 
                <?php foreach ( $categories as $key=>$term ): ?>
                <li role="tab">
                    <a data-toggle="tab" href="#<?php echo esc_attr($term->slug).'-'.esc_attr($_id);?>" title="<?php echo esc_attr( $term->name); ?>" role="tab">
                        <?php echo trim( $term->name ); ?>
                    </a>
                </li>
                <?php endforeach; ?>
            <?php endif;?>
            
        </ul>
        <a class="pull-right view-all" href="<?php echo esc_url(get_term_link( $ocategory->term_id, 'category'));?>"><?php esc_html_e('Veja todos', 'notiz');?></a>
    </div> 
        
    <div class="widget-content">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="<?php echo esc_attr($ocategory->slug).'-'.esc_attr($_id);;?>">
                <div class="<?php echo esc_attr($post_style);?>">
                <?php
                    $args = array(
                        'post_type'         =>  'post',
                        'posts_per_page'    =>  notiz_get_numberpost_categories($post_style),
                        'category__in'      =>  array($ocategory->term_id),
                        'post_status'       =>  'publish'
                    );
                    $posts = new WP_Query($args);
                    if($posts->have_posts()):
                        notiz_wpo_get_template('post/post-'.$post_style.'.php',array('loop'=> $posts, 'photo' => $photo, 'ocategory' => $ocategory) );
                    endif;
                ?>
                </div>
            </div>
            <?php foreach ( $categories as $term ): ?>
                <div role="tabpanel" class="tab-pane fade" id="<?php echo esc_attr($term->slug).'-'.esc_attr($_id);;?>">
                    <div class="<?php echo esc_attr($post_style);?>">
                        <?php
                            $args = array(
                                'post_type'         =>  'post',
                                'posts_per_page'    =>  notiz_get_numberpost_categories($post_style),
                                'category__in'      =>  array($term->term_id),
                                'post_status'       =>  'publish'
                            );
                            $posts = new WP_Query($args);
                            if($posts->have_posts()):
                                notiz_wpo_get_template('post/post-'.$post_style.'.php',array('loop'=> $posts, 'photo' => $photo, 'ocategory' => $ocategory) );
                            endif;
                        ?>
                    </div>
                </div>
            <?php endforeach; ?>    
        </div>
    </div>
</section>
<?php endif; ?>