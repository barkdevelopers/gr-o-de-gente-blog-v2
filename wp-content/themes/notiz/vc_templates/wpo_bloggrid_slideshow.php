<?php
  $slider = 1;
  $atts = vc_map_get_attributes( $this->getShortcode(), $atts );
  extract( $atts );

  $posts = array();

  if(empty($loop)) return;
  $this->getLoop($loop);
  $args = $this->loop_args;

  $args['posts_per_page'] = $slider*4;
  $_id = notiz_wpo_makeid();

?>

<section class="widget wpo-grid-posts slide-show section-blog <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
    <?php if( $title ) { ?>
        <h3 class="widget-title visual-title">
           <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>

    <div class="widget-content owl-carousel-play" id="bloggrid-<?php echo esc_attr($_id); ?>" data-ride="carousel">
        
        <?php
            $loop = new WP_Query($args);
            $_count = 0;
        ?>
       <?php if( $loop->post_count > 4 ) { ?>
          <div class="carousel-controls">
            <a href="#bloggrid-<?php echo esc_attr($_id); ?>" data-slide="prev" class="left carousel-control carousel-md radius-x">
              <span class="zmdi zmdi-arrow-back zmd-fw"></span>
            </a>
            <a href="#bloggrid-<?php echo esc_attr($_id); ?>" data-slide="next" class="right carousel-control carousel-md radius-x">
              <span class="zmdi zmdi-arrow-forward zmd-fw"></span>
            </a>
          </div>
        <?php } ?>
        <div class="owl-carousel" data-slide="1"  data-singleItem="true" data-navigation="true" data-pagination="false">
          <?php if($loop->have_posts()):
              while($loop->have_posts()):
                $loop->the_post(); 
          ?>
                <?php if(0==$_count%4): ?>
                  <div class="item">
                    <div class="blog-grid-1">
                      <?php get_template_part('templates/post/_single', 'fp-v1'); ?>
                    </div>
                <?php else: ?>
                    <?php if(1==$_count%4): ?>
                      <div class="blog-grid-2">
                        <div class="row">
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <?php get_template_part('templates/post/_single', 'fp-v2'); ?>
                          </div>
                        <?php if($_count==$loop->post_count-1): ?>
                        </div>
                        </div>
                        <?php endif; ?>
                    <?php else: ?>
                      <?php if(2==$_count%4): ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                      <?php endif; ?>
                        <?php get_template_part('templates/post/_single', 'fp-v2'); ?>
                      <?php if(3==$_count%4 || $_count == $loop->post_count-1): ?>
                          </div>
                      </div>
                      </div>
                      <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if(3==$_count%4 ||$_count==$loop->post_count-1): ?>
                    </div>
                  <?php endif; ?>
                <?php $_count++; ?>
              <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>
        <?php endif; ?>
        </div>
    </div>
</section>
