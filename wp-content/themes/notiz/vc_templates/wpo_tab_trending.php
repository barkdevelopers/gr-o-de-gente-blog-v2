<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$_id = notiz_wpo_makeid();
wp_enqueue_script( 'counter_js', WPO_THEME_URI.'/js/jquery.counterup.min.js', array( 'jquery' ) );
wp_enqueue_script( 'waypoints_js', WPO_THEME_URI.'/js/waypoints.min.js', array( 'jquery' ) );


?>

<section class="widget frontpage-blog section-blog trending-widget <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
    <div class="heading-title clearfix">
        <?php if(!empty($title)): ?>
            <h3 class="widget-title visual-title">
                <span><?php echo trim($title); ?></span>
            </h3>
        <?php endif; ?>
        <div class="heading-tab">
            <ul role="tablist" class="nav nav-tabs">
                <li role="presentation" class="active">
                    <a role="tab" href="#tab_eye-<?php echo esc_attr( $_id ); ?>" data-toggle="tab">
                        <i class="fa fa-eye"></i>
                    </a>
                </li>
                <li role="tab">
                    <a href="#tab_comment-<?php echo esc_attr( $_id ); ?>" data-toggle="tab">
                        <i class="fa fa-comments-o"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="widget-content"> 
        <div class="tab-content">
            <!-- Tabs views-->
            <div role="tabpanel" class="tab-pane fade active in tab-views trending" id="tab_eye-<?php echo esc_attr( $_id ); ?>">
            <?php
                $args = array(
                    'posts_per_page' => $number,
                    'meta_key'   => 'wpo_post_views_count',
                    'orderby' => 'meta_value_num',
                    'order' => 'DESC'
                );
                $most_views = new WP_Query($args);
                if($most_views->have_posts()):
                    while($most_views->have_posts()): $most_views->the_post(); 
                    $categories = get_the_category();
            ?>
                <div class="content-post">
                    <div class="count-views" data-value="<?php echo notiz_wpo_get_post_views(get_the_ID());?>">
                        <?php echo notiz_wpo_get_post_views(get_the_ID());?>
                    </div>
                    <div class="post-content">
                        <div class="category-post">
                            <a href="<?php echo esc_url( get_category_link($categories[0]->term_id) );?>">
                                <?php echo trim($categories[0]->name); ?>
                            </a>
                        </div>
                        <h3 class="entry-title">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            </div>
            <!-- Tabs views-->
            <div role="tabpanel" class="tab-pane fade tab-comments trending" id="tab_comment-<?php echo esc_attr( $_id ); ?>">
            <?php
                $args = array(
                    'posts_per_page' => $number,
                    'orderby' => 'comment_count',
                    'order' => 'DESC'
                );
                $most_comments = new WP_Query($args);

                if($most_comments->have_posts()):
                    while($most_comments->have_posts()): $most_comments->the_post(); 
                        $categories = get_the_category();
                        $comments_count = wp_count_comments( get_the_ID());
            ?>
                <div class="content-post">
                    <div class="count-views" data-value="<?php echo esc_attr( $comments_count->total_comments );?>">
                        <?php echo esc_attr( $comments_count->total_comments );?>
                    </div>
                    <div class="post-content">
                        <div class="category-post">
                            <a href="<?php echo esc_url( get_category_link($categories[0]->term_id) );?>">
                                <?php echo trim($categories[0]->name); ?>
                            </a>
                        </div>
                        <h3 class="entry-title">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</section>