<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$grid_link = $grid_layout_mode = $title = $filter= '';
$posts = array();

if(empty($loop)) return;
$this->getLoop($loop);
$args = $this->loop_args;
 
$loop = new WP_Query($args);
?>

<section class="widget wpo-grid-posts section-blog <?php echo (($el_class!='')?' '.$el_class:''); ?>">
    <?php if( $title ) { ?>
        <h3 class="widget-title visual-title <?php echo esc_attr($size).' '.$alignment; ?> widget-title-<?php echo esc_attr($widget_title_style); ?> <?php echo esc_attr($el_class); ?>">
           <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>

    <div class="widget-content">

        <?php
            $loop = new WP_Query($args);
            if($loop->have_posts()){  ?>
                <div class="frontpage frontpage-7">
                <?php $i = 0;
                    while($loop->have_posts()){
                    $loop->the_post();
                ?>
                    <article class="post">
                        <?php
                        if ( has_post_thumbnail() ) {
                            ?>
                                <figure class="entry-thumb">
                                    <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                                        <?php the_post_thumbnail( $thumbsize );?>
                                    </a>
                                    <!-- vote    -->
                                    <?php do_action('wpo_rating') ?>
                                </figure>
                            <?php
                        }
                        ?>
                        <div class="entry-content">
                            <div class="entry-meta-2">
                                <span class="category">
                                    <?php $cats = get_the_category(); ?>
                                    <?php if ( isset( $cats[0] ) ) { $first_cat = $cats[0]; ?> 
                                        <?php if ( isset( $first_cat->term_id ) && isset( $first_cat->name ) ) { ?>
                                            <a class="entry-categories" href="<?php echo esc_url( get_category_link( $first_cat->term_id ) ); ?>"><?php echo esc_html( $first_cat->name ); ?></a>
                                        <?php } // endif ?>
                                    <?php } // endif ?>
                                </span>
                            </div>

                            <?php if (get_the_title()) { ?>
                                <h4 class="entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h4>
                            <?php  } ?>
                            <?php
                                if (! has_excerpt()) {
                                    echo "";
                                } else {
                                    ?>
                                        <p class="entry-description"><?php echo notiz_wpo_excerpt(20,'...'); ?></p>
                                    <?php
                                }
                            ?>
                        </div>
                        <!-- end:entry-content -->
                    </article>
                <?php  } ?>
                <?php wp_reset_postdata(); ?>
                </div>
            <?php  } ?>
    </div>
</section>