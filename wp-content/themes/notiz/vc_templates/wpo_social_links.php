<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>
<?php if($show_facebook || $show_google_plus || $show_youtube || $show_pinterest || $show_linkedIn || $show_twitter ): ?>
<div class="widget-social-links <?php echo esc_attr($el_class); echo esc_attr( $style ); ?>">
	
		<?php if( !empty( $title)): ?>
			<h3 class="widget-title">
				<?php echo esc_attr($title);?>
			</h3>
		<?php endif; ?>
		
		<div class="widget-content">
			<ul class="social list-unstyled ">
			<?php if($show_facebook): ?>
				<li>
					<a class="facebook-social" href="<?php echo esc_url($link_facebook);?>"  target="_blank" >
						<i class="fa fa-facebook"></i>
					</a>
				</li>
			<?php endif; ?>	
			<?php if($show_twitter): ?>
				<li>
					<a class="twitter-social" href="<?php echo esc_url($link_twitter);?>"  target="_blank">
						<i class="fa fa-twitter"></i>
					</a>
				</li>
			<?php endif; ?>		
			<?php if($show_google_plus): ?>
				<li>
					<a href="<?php echo esc_url($link_google_plus);?>"  target="_blank">
						<i class="fa fa-google-plus"></i>
					</a>
				</li>
			<?php endif; ?>	
			<?php if($show_youtube): ?>
				<li>
					<a href="<?php echo esc_url($link_youtube);?>"  target="_blank">
						<i class="fa fa-youtube"></i>
					</a>
				</li>
			<?php endif; ?>	
			<?php if($show_pinterest): ?>
				<li>
					<a href="<?php echo esc_url($link_pinterest);?>"  target="_blank" >
						<i class="fa fa-pinterest"></i>
					</a>
				</li>
			<?php endif; ?>	
				<?php if($show_linkedIn): ?>
					<li><a href="<?php echo esc_url($link_linkedIn);?>"  target="_blank">
							<i class="fa fa-linkedin"></i>
						</a>
					</li>
				<?php endif; ?>	
			</ul>
		</div>
</div>
<?php endif; ?>