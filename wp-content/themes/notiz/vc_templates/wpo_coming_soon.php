<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


$timestamp = strtotime($date_comingsoon);
if( $timestamp < time())
    return;

?>

<?php
    //register countdown js
    wp_enqueue_script( 'countdown_js', WPO_THEME_URI.'/js/countdown.js', array( 'jquery' ) );
?>

<div class="widget countdown wpo-coming-soon <?php echo esc_attr($el_class.' '.$style); ?>">
	<?php if( $title ) { ?>
        <h3 class="widget-title visual-title">
           <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
        <div class="description-countdown">
            <?php echo trim( $description ); ?>
        </div>
       <div class="coming-soon-time">
            <div class="pts-countdown clearfix" data-countdown="countdown"
                 data-date="<?php echo date('m',$timestamp).'-'.date('d',$timestamp).'-'.date('Y',$timestamp).'-'. date('H',$timestamp) . '-' . date('i',$timestamp) . '-' .  date('s',$timestamp) ; ?>">
            </div>
        </div>
</div>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('[data-countdown="countdown"]').each(function(index, el) {
            var $this = $(this);
            var $date = $this.data('date').split("-");
            $this.lofCountDown({
                TargetDate:$date[0]+"/"+$date[1]+"/"+$date[2]+" "+$date[3]+":"+$date[4]+":"+$date[5],
                DisplayFormat:"<div class=\"countdown-times\"><div class=\"day\">%%D%% <?php esc_html__(' DAYS ', 'notiz'); ?></div><div class=\"hours\">%%H%% <?php esc_html__(' HOURS ', 'notiz'); ?></div><div class=\"minutes\">%%M%% <?php esc_html__(' MINUTES ', 'notiz'); ?></div><div class=\"seconds\">%%S%% <?php esc_html__('SECONDS', 'notiz'); ?></div></div>",
                FinishMessage: "<?php esc_html__('Expired', 'notiz'); ?>"
            });
        });
    });

</script>