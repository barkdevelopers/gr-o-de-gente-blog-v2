<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$_id = notiz_wpo_makeid();
$_count = 0;
$args = array(
	'post_type' => 'testimonial',
	'posts_per_page' => '1',
	'post_status' => 'publish',
);

$query = new WP_Query($args);
?>
<?php $img = wp_get_attachment_image_src($imagebg,'full'); ?>
<?php
	$style = array();
	if( isset($img[0]) )  {
		 $style[] = "background-image:url('".$img[0]."')";
	}
	if( $colorbg ){
		$style[] = "background-color:".$colorbg;
	}
?>

<div class="widget wpo-testimonial <?php echo esc_attr($el_class); ?>">
	<?php if($query->have_posts()){ ?>
		<?php if($title!=''){ ?>
			<h3 class="widget-title visual-title widget-title-<?php echo esc_attr($widget_title_style); ?> <?php echo esc_attr($el_class); ?>">
				<span><?php echo trim($title); ?></span>
			</h3>
		<?php } ?>
 
			<!-- Skin 1 -->
			<div class="widget-content">
					
				<?php  $_count=0; while($query->have_posts()):$query->the_post(); ?>
					<!-- Wrapper for slides -->
					<div class="item">
						<?php  get_template_part( 'vc_templates/testimonials/testimonials-v5' ); ?>
					</div>
					<?php $_count++; ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
	<?php } ?>
	<div class="wpo-testimonial-bg" style="<?php echo implode(';', $style); ?>">
			
	</div>
</div>