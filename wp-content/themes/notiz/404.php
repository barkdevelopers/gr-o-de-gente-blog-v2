<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     Opal  Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http:/wpopal.com
 * @support  http://wpopal.com
 */

/*
*Template Name: 404 Page
*/

?>

<?php get_header( $wpoEngine->getHeaderLayout() ); ?>

<section class="wpo-mainbody clearfix notfound-page">
	<section class="container">
		<div class="page_not_found text-center clearfix space-padding-tb-100">
			<div class="col-sm-12 space-padding-tb-50">
				<div class="clearfix"></div>
				<div class="title">
					<span>404</span>
					<span class="sub"><?php esc_html_e('Página não encontrada', 'notiz') ?></span>
				</div>
				<div class="error-description"><?php esc_html_e('Nos pedimos desculpas porém a página que você esta procurando não foi encontrada.', 'notiz') ?> </div>
				<div class="page-action">
					<a class="btn btn-lg btn-outline btn-success border-2 radius-0x" href="javascript: history.go(-1)"><?php esc_html_e('Voltar para página anterior', 'notiz'); ?></a>
					<a class="btn btn-lg btn-outline btn-success border-2 radius-0x" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Voltar para home', 'notiz'); ?></a>
				</div>
			</div>
		</div>
	</section>
</section>

<?php get_footer(); ?>