<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( post_password_required() ){
    return;
}
?>
<div id="comments" class="comments">
    <header class="header-title">
        <h5 class="comments-title title"><?php comments_number( esc_html__('nenhum comentário', 'notiz'), esc_html__('1 Comentário', 'notiz'), esc_html__('% Comentários', 'notiz') ); ?></h5>
    </header><!-- /header -->

    <?php if ( have_comments() ) { ?>
        <div class="wpo-commentlists">
    	    <ol class="commentlists">
    	        <?php wp_list_comments('callback=notiz_wpo_theme_comment'); ?>
    	    </ol>
    	    <?php
    	    	// Are there comments to navigate through?
    	    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
    	    ?>
    	    <footer class="navigation comment-navigation" role="navigation">
    	        <div class="previous"><?php previous_comments_link( esc_html__( '&larr; Comentários antigos', 'notiz' ) ); ?></div>
    	        <div class="next right"><?php next_comments_link( esc_html__( 'Comentários novos &rarr;', 'notiz' ) ); ?></div>
    	    </footer><!-- .comment-navigation -->
    	    <?php endif; // Check for comment navigation ?>

    	    <?php if ( ! comments_open() && get_comments_number() ) : ?>
    	        <p class="no-comments"><?php esc_html_e( 'Comentario excluido.' , 'notiz' ); ?></p>
    	    <?php endif; ?>
        </div>
    <?php } ?> 

	<?php
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $comment_args = array(
                        'title_reply'=> '<h4 class="title">'.esc_html__('Deixe um comentário','notiz').'</h4>',
                        'comment_field' => '<div class="form-group">
                                                <label class="required" for="comment">'.esc_html__('Comentário','notiz').'</label>
                                                <textarea placeholder="" rows="8" id="comment" class="form-control"  name="comment"'.$aria_req.'></textarea>
                                            </div>',
                        'fields' => apply_filters(
                        	'comment_form_default_fields',
                    		array(
                                'author' => '<div class="row"><div class="form-group col-sm-6 col-xs-12">
                                            <label class="required" for="author">'.esc_html__('Nome','notiz').'</label>
                                            <span><input type="text" name="author" placeholder="" class="form-control" id="author" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' /></span>
                                            </div>',
                                'email' => ' <div class="form-group col-sm-6 col-xs-12">
                                            <label class="required" for="email">'.esc_html__('Email','notiz').'</label>
                                            <span><input id="email" name="email" placeholder="" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' /></span>
                                            </div></div>'
                            )),
                        'label_submit' => esc_html__('Comentário do Post', 'notiz'),
						'comment_notes_before' => '<div class="form-group h-info">'.esc_html__('Seu email não será publicado.','notiz').'</div>',
						'comment_notes_after' => '',
                        );
    ?>
	<?php global $post; ?>
	<?php if('open' == $post->comment_status){ ?>
	<div class="commentform row reset-button-default">
    	<div class="col-sm-12">
			<?php notiz_wpo_comment_form($comment_args); ?>
    	</div>
    </div><!-- end commentform -->
	<?php } ?>
</div><!-- end comments -->