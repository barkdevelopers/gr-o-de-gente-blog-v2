<?php  
global $notizconfig;  
$pos = notiz_wpo_theme_options('woocommerce-archive-right-sidebar');
?>

<?php if($notizconfig['right-sidebar']['show']){ ?>
	<div class="<?php echo esc_attr($notizconfig['right-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-right">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
		</div>
	</div>
<?php } ?>
