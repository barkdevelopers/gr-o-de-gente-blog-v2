<div id="wpo-videopost">
	 <p class="wpo_section ">
        <?php $mb->the_field('job'); ?>
        <label for="embed_post"><?php echo __('Job:','wpothemer');?></label>
        <input type="text" name="<?php $mb->the_name(); ?>" id="embed_videopost" value="<?php $mb->the_value(); ?>" />
    </p>
    <p class="wpo_section ">
        <?php $mb->the_field('phone'); ?>
        <label for="embed_post"><?php echo __('Phone:','wpothemer');?></label>
        <input type="text" name="<?php $mb->the_name(); ?>" id="embed_videopost" value="<?php $mb->the_value(); ?>" />
    </p>
    <p class="wpo_section ">
        <?php $mb->the_field('email'); ?>
        <label for="link_post"><?php _e( 'Email', 'wpothemer' );?></label>
        <input type="text" name="<?php $mb->the_name(); ?>" id="embed_videopost" value="<?php $mb->the_value(); ?>" />
    </p>

     <p class="wpo_section ">
        <?php $mb->the_field('facebook'); ?>
        <label for="link_post"><?php _e( 'Facebook', 'wpothemer' );?></label>
        <input type="text" name="<?php $mb->the_name(); ?>" id="embed_videopost" value="<?php $mb->the_value(); ?>" />
    </p>

     <p class="wpo_section ">
        <?php $mb->the_field('twitter'); ?>
        <label for="link_post"><?php _e( 'Twitter', 'wpothemer' );?></label>
        <input type="text" name="<?php $mb->the_name(); ?>" id="embed_videopost" value="<?php $mb->the_value(); ?>" />
    </p>

     <p class="wpo_section ">
        <?php $mb->the_field('linkedin'); ?>
        <label for="link_post"><?php _e( 'Linked In', 'wpothemer' );?></label>
        <input type="text" name="<?php $mb->the_name(); ?>" id="embed_videopost" value="<?php $mb->the_value(); ?>" />
    </p>
</div>

<script>
	WPO_Admin.params_Embed('#embed_post','#wpo-videopost');
</script>