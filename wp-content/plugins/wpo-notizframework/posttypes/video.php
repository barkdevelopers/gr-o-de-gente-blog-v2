<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

if(!function_exists('wpo_create_type_video')   ){
  function wpo_create_type_video(){
    $labels = array(
      'name' => __( 'Video', "wpothemer" ),
      'singular_name' => __( 'Video', "wpothemer" ),
      'add_new' => __( 'Add New Video', "wpothemer" ),
      'add_new_item' => __( 'Add New Video', "wpothemer" ),
      'edit_item' => __( 'Edit Video', "wpothemer" ),
      'new_item' => __( 'New Video', "wpothemer" ),
      'view_item' => __( 'View Video', "wpothemer" ),
      'search_items' => __( 'Search Videos', "wpothemer" ),
      'not_found' => __( 'No Videos found', "wpothemer" ),
      'not_found_in_trash' => __( 'No Videos found in Trash', "wpothemer" ),
      'parent_item_colon' => __( 'Parent Video:', "wpothemer" ),
      'menu_name' => __( 'Opal Videos', "wpothemer" ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Video',
        'supports' => array( 'title', 'editor', 'thumbnail','comments', 'excerpt' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
    register_post_type( 'video', $args );

    $labels = array(
        'name'              => __( 'Categories Video', "wpothemer" ),
        'singular_name'     => __( 'Category', "wpothemer" ),
        'search_items'      => __( 'Search Category',"wpothemer" ),
        'all_items'         => __( 'All Categories',"wpothemer" ),
        'parent_item'       => __( 'Parent Category',"wpothemer" ),
        'parent_item_colon' => __( 'Parent Category:',"wpothemer" ),
        'edit_item'         => __( 'Edit Category',"wpothemer" ),
        'update_item'       => __( 'Update Category',"wpothemer" ),
        'add_new_item'      => __( 'Add New Category',"wpothemer" ),
        'new_item_name'     => __( 'New Category Name',"wpothemer" ),
        'menu_name'         => __( 'Categories Video',"wpothemer" ),
      );
      // Now register the taxonomy
      register_taxonomy('category_video',array('video'),
          array(
              'hierarchical'      => true,
              'labels'            => $labels,
              'show_ui'           => true,
              'show_admin_column' => true,
              'query_var'         => true,
              'rewrite'           => array( 'slug' => 'video'
          ),
      ));

      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/video.php') ){
          new WPO_MetaBox(array(
            'id'       => 'wpo_formatvideo',
            'title'    => __('Embed Options', 'wpothemer' ),
            'types'    => array('video'),
            'priority' => 'high',
            'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'video.php',
          ));

      }  
  }
  add_action( 'init', 'wpo_create_type_video' );
}


