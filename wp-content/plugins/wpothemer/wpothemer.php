<?php 
/*
  Plugin Name: WPO Themer
  Plugin URI: http://www.wpopal.com/
  Description: implement rick functions for themes base on wpo framework, this is required.
  Version: 0.1
  Author: WP_Opal
  Author URI: http://www.wpopal.com
  License: GPLv2 or later
 */

 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

  define( 'WPO_PLUGIN_THEMER_URL', plugin_dir_url( __FILE__ ) );
  define( 'WPO_PLUGIN_THEMER_DIR', plugin_dir_path( __FILE__ ) );
  define( 'WPO_PLUGIN_THEMER_TEMPLATE_DIR', WPO_PLUGIN_THEMER_DIR.'metabox_templates/' );

  include_once( dirname( __FILE__ ) . '/import/import.php' );
  include_once( dirname( __FILE__ ) . '/export/export.php' );
 	define( "WPO_PLUGIN_THEMER", true );
  ?>