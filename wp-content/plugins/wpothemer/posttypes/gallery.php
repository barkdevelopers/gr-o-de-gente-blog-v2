<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */
if(!function_exists('wpo_create_type_gallery') && defined('WPO_THEME_INC_DIR')  ){
    function wpo_create_type_gallery(){
      $labels = array(
          'name'               => __( 'Gallerys', "wpothemer" ),
          'singular_name'      => __( 'Gallery', "wpothemer" ),
          'add_new'            => __( 'Add New Gallery', "wpothemer" ),
          'add_new_item'       => __( 'Add New Gallery', "wpothemer" ),
          'edit_item'          => __( 'Edit Gallery', "wpothemer" ),
          'new_item'           => __( 'New Gallery', "wpothemer" ),
          'view_item'          => __( 'View Gallery', "wpothemer" ),
          'search_items'       => __( 'Search Gallerys', "wpothemer" ),
          'not_found'          => __( 'No Gallerys found', "wpothemer" ),
          'not_found_in_trash' => __( 'No Gallerys found in Trash', "wpothemer" ),
          'parent_item_colon'  => __( 'Parent Gallery:', "wpothemer" ),
          'menu_name'          => __( 'Opal Gallerys', "wpothemer" ),
      );

      $args = array(
          'labels'              => $labels,
          'hierarchical'        => false,
          'description'         => 'List Gallery',
          'supports'            => array( 'title', 'editor', 'author', 'thumbnail','excerpt','custom-fields' ), //page-attributes, post-formats
          'taxonomies'          => array( 'gallery_category','skills','post_tag' ),
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'menu_position'       => 5,
          'menu_icon'           => WPO_FRAMEWORK_ADMIN_IMAGE_URI.'icon/admin_ico_gallery.png',
          'show_in_nav_menus'   => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => false,
          'has_archive'         => true,
          'query_var'           => true,
          'can_export'          => true,
          'rewrite'             => array('slug'=>'gallery'),
          'capability_type'     => 'post',
      );
      register_post_type( 'gallery', $args );

      //Add Port folio Skill
      // Add new taxonomy, make it hierarchical like categories
      //first do the translations part for GUI
      $labels = array(
        'name'              => __( 'Categories', "wpothemer" ),
        'singular_name'     => __( 'Category', "wpothemer" ),
        'search_items'      => __( 'Search Category',"wpothemer" ),
        'all_items'         => __( 'All Categories',"wpothemer" ),
        'parent_item'       => __( 'Parent Category',"wpothemer" ),
        'parent_item_colon' => __( 'Parent Category:',"wpothemer" ),
        'edit_item'         => __( 'Edit Category',"wpothemer" ),
        'update_item'       => __( 'Update Category',"wpothemer" ),
        'add_new_item'      => __( 'Add New Category',"wpothemer" ),
        'new_item_name'     => __( 'New Category Name',"wpothemer" ),
        'menu_name'         => __( 'Categories',"wpothemer" ),
      );
      // Now register the taxonomy
      register_taxonomy('gallery_category',array('gallery'),
          array(
              'hierarchical'      => true,
              'labels'            => $labels,
              'show_ui'           => true,
              'show_admin_column' => true,
              'query_var'         => true,
              'show_in_nav_menus' => false,
              'rewrite'           => array( 'slug' => 'gallery-category'
          ),
      ));



      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/gallery.php')  ){   //Gallery Setting.
        $aa = new WPO_MetaBox(array(
          'id'       => 'wpo_pageconfig',
          'title'    => __('Gallery Configuration', TEXTDOMAIN),
          'types'    => array('gallery'),
          'priority' => 'high',
          'template' => WPO_PLUGIN_THEMER_TEMPLATE_DIR . 'gallery.php',
        ));

      } 

  }
  add_action( 'init','wpo_create_type_gallery' );
}