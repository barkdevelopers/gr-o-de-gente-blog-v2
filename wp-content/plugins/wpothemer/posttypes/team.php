<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

if(!function_exists('wpo_create_type_team') && defined('WPO_THEME_INC_DIR')  ){
    function wpo_create_type_team(){
      $labels = array(
        'name' => __( 'Opal Team', "wpothemer" ),
        'singular_name' => __( 'Team', "wpothemer" ),
        'add_new' => __( 'Add New Team', "wpothemer" ),
        'add_new_item' => __( 'Add New Team', "wpothemer" ),
        'edit_item' => __( 'Edit Team', "wpothemer" ),
        'new_item' => __( 'New Team', "wpothemer" ),
        'view_item' => __( 'View Team', "wpothemer" ),
        'search_items' => __( 'Search Teams', "wpothemer" ),
        'not_found' => __( 'No Teams found', "wpothemer" ),
        'not_found_in_trash' => __( 'No Teams found in Trash', "wpothemer" ),
        'parent_item_colon' => __( 'Parent Team:', "wpothemer" ),
        'menu_name' => __( 'Opal Teams', "wpothemer" ),
      );

      $args = array(
          'labels' => $labels,
          'hierarchical' => false,
          'description' => 'List Team',
          'supports' => array( 'title', 'editor', 'thumbnail','excerpt'),
          'public' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'menu_position' => 5,
          'show_in_nav_menus' => false,
          'publicly_queryable' => true,
          'exclude_from_search' => true,
          'has_archive' => true,
          'query_var' => true,
          'can_export' => true,
          'rewrite' => false,
          'capability_type' => 'post'
      );
      register_post_type( 'team', $args );

        if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/team.php')  ){
          new WPO_MetaBox(array(
            'id'       => 'wpo_team',
            'title'    => __('Team Options', "wpothemer"),
            'types'    => array('team'),
            'priority' => 'high',
            'template' => WPO_PLUGIN_THEMER_TEMPLATE_DIR . 'team.php',
          ));
        }  
    }

   add_action( 'init','wpo_create_type_team' );
}


